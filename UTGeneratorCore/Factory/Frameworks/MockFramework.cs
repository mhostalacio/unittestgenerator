﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using UTGeneratorCore.Models;

namespace UTGeneratorCore.Factory.Frameworks
{
    public abstract class MockFramework
    {
        public abstract IList<SyntaxNode> GenerateTestClassFields(ClassMapper classUnderTest);

        public abstract IList<UsingDirectiveSyntax> GenerateUsingDirectives();

        public abstract IList<SyntaxNode> GenerateTestClassSetup(ClassMapper classUnderTest);

        public abstract IList<SyntaxNode> GenerateTestClassCleanup(ClassMapper classUnderTest);

        public abstract IList<SyntaxNode> CreateMethodMock(SyntaxNode expression, MockMethodMapper methodMock, SemanticModel model, ITestGenerator generator, bool avoidShouldBeNullParameterAssertion);

        public abstract IList<SyntaxNode> CreateMethodMockThatThrowsException(SyntaxNode expression, MockMethodMapper methodMock, SemanticModel model, ITestGenerator generator, string exceptionType);
    }
}
