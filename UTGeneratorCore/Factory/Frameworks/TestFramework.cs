﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace UTGeneratorCore.Factory.Frameworks
{
    public abstract class TestFramework
    {
        public abstract IList<UsingDirectiveSyntax> GenerateUsingDirectives();

        public abstract AttributeListSyntax GetClassAttributeList();

        public abstract AttributeListSyntax GetTestClassSetupAttributeList();

        public abstract AttributeListSyntax GetTestClassOneTimeSetupAttributeList();

        public abstract AttributeListSyntax GetTestClassCleanupAttributeList();

        public abstract AttributeListSyntax GetTestClassOneTimeCleanupAttributeList();

        public abstract AttributeListSyntax GetTestMethodAttributeList();

        public abstract AttributeListSyntax GetTestMethodDataSourceAttributeList(string variableName);
    }
}
