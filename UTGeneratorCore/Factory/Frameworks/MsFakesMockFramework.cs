﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using UTGeneratorCore.Factory.CSharp;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace UTGeneratorCore.Factory.Frameworks
{
    public class MsFakesMockFramework : MockFramework
    {
        public const string ShimContextVariableName = "_shimContext";

        protected MsFakesMockFramework() { }

        private static MsFakesMockFramework _instance;

        public static MsFakesMockFramework Instance => _instance ?? (_instance = new MsFakesMockFramework());

        public override IList<SyntaxNode> GenerateTestClassCleanup(ClassMapper classUnderTest)
        {
            var list = new List<SyntaxNode>
            {
                ParseStatement($"{ShimContextVariableName}?.Dispose();")
            };

            return list;
        }

        public override IList<SyntaxNode> GenerateTestClassFields(ClassMapper classUnderTest)
        {
            var list = new List<SyntaxNode>
            {
                CSharpHelper.CreateField(
                    ShimContextVariableName,
                    "IDisposable",
                    null,
                    false)
            };
            return list;
        }

        public override IList<SyntaxNode> GenerateTestClassSetup(ClassMapper classUnderTest)
        {
            var list = new List<SyntaxNode>
            {
                ParseStatement($"{ShimContextVariableName} = ShimsContext.Create();")
            };
            //handle special cases
            if (classUnderTest.ContainsHttpContext)
            {
                list.Add(ParseStatement("ShimHttpContext.CurrentGet = () => new ShimHttpContext().Instance;"));
                list.Add(ParseStatement("ShimHttpContext.AllInstances.ResponseGet = _ => new ShimHttpResponse().Instance;"));
                list.Add(ParseStatement("ShimHttpContext.AllInstances.RequestGet = _ => new ShimHttpRequest().Instance;"));
                list.Add(ParseStatement("ShimHttpRequest.AllInstances.BrowserGet = _ => new ShimHttpBrowserCapabilities();"));
                list.Add(ParseStatement("ShimHttpContext.AllInstances.SessionGet = _ => new ShimHttpSessionState { ItemGetString = key => new object() }.Instance;"));
            }
            return list;
        }

        public override IList<UsingDirectiveSyntax> GenerateUsingDirectives()
        {
            var usingDirectives = new List<UsingDirectiveSyntax>
            {
                UsingDirective(ParseName("Microsoft.QualityTools.Testing.Fakes"))
            };

            return usingDirectives;
        }

        private bool ClassContainsShim(ClassMapper clazz, string fieldName)
        {
            return clazz.AlreadyAddedMocks.Contains(fieldName);
        }

        private bool ClassContainsFieldWithSameNameAndType(ITestGenerator generator, string fieldName, string typeName)
        {
            return generator.FieldNodes.Any(o => o is FieldDeclarationSyntax fieldDeclarationSyntax
                                                 && fieldDeclarationSyntax.Declaration.Variables.Count > 0
                                                 && fieldDeclarationSyntax.Declaration.Variables[0].Identifier.ToString().Equals(fieldName)
                                                 && fieldDeclarationSyntax.Declaration.Type is IdentifierNameSyntax typeIdentifier
                                                 && typeIdentifier.Identifier.ToString().Equals(typeName));
        }

        private bool ClassContainsFieldWithSameName(ITestGenerator generator, string fieldName)
        {
            return generator.FieldNodes.Any(o => o is FieldDeclarationSyntax fieldDeclarationSyntax
                                                 && fieldDeclarationSyntax.Declaration.Variables.Count > 0
                                                 && fieldDeclarationSyntax.Declaration.Variables[0].Identifier.ToString().Equals(fieldName));
        }

        public override IList<SyntaxNode> CreateMethodMock(SyntaxNode expression, MockMethodMapper methodMock, SemanticModel model, ITestGenerator generator, bool avoidShouldBeNullParameterAssertion)
        {
            var unitTestSyntaxList = new List<SyntaxNode>();
            var setOrReturnValue = CSharpHelper.GetArgumentValue
            (
                new KeyValuePair<string, ParameterSymbol>
                (
                    methodMock.Symbol.Name,
                    new ParameterSymbol(methodMock.MockType)
                ),
                true,
                false,
                generator
            ).NormalizeWhitespace().ToString();
            if (!string.IsNullOrWhiteSpace(methodMock.ReturnExpressionString))
            {
                if (methodMock.AddIsNullFlag)
                {
                    setOrReturnValue = $"{methodMock.ReturnExpressionString} ? null : {setOrReturnValue}";
                }
                else
                {
                    setOrReturnValue = methodMock.ReturnExpressionString;
                }
            }
            else if (string.IsNullOrWhiteSpace(setOrReturnValue))
            {
                setOrReturnValue = "null";
            }

            if (methodMock.Symbol is IFieldSymbol fieldSymbol)
            {
               
                if (fieldSymbol.IsStatic)
                {
                    unitTestSyntaxList.Add(ParseStatement($"{CSharpHelper.PrivateTypeMemberName}.SetStaticFieldOrProperty(\"{methodMock.MethodName}\", {setOrReturnValue});"));
                }
                else
                {
                    unitTestSyntaxList.Add(ParseStatement($"{CSharpHelper.PrivateObjectMemberName}.SetFieldOrProperty(\"{methodMock.MethodName}\", {setOrReturnValue});"));
                }
            }
            else
            {
                var useStub = (methodMock.Symbol.ContainingType.IsAbstract && methodMock.Symbol.ContainingType.InstanceConstructors.Length == 0)
                    || methodMock.Symbol.IsAbstract;
                var classSuffix = useStub
                    ? "Stub"
                    : "Shim";
                var fakesClassName = $"{classSuffix}{methodMock.Symbol.ContainingType.Name}";
                // Add using nodes
                if (methodMock.Symbol.ContainingType.ContainingNamespace != null)
                {
                    fakesClassName = generator.AddUsingNodeIfNew(UsingDirective(ParseName($"{methodMock.Symbol.ContainingType.ContainingNamespace}.Fakes")), fakesClassName);
                }
                var addInstanceParam = !methodMock.Symbol.IsStatic && !useStub &&
                                            !(methodMock.Symbol is IMethodSymbol methodSymbol && methodSymbol.IsExtensionMethod);
                var parameterList = CreateParametersListForMethodSymbols(methodMock, methodMock.Parameters, addInstanceParam && !useStub);
                var shimMethodSyntaxList = new List<StatementSyntax>();
                var alreadyContainsShim = ClassContainsShim(methodMock.Class, methodMock.MethodInvokeCountVariableName);
                if (!alreadyContainsShim)
                {
                    methodMock.Class.AlreadyAddedMocks.Add(methodMock.MethodInvokeCountVariableName);
                }

                // Create parameter assertions
                var paramsAssertionString = "";
                if (methodMock.Parameters.Count() > 0)
                {
                    var parametersToAssert = methodMock.Parameters
                        .Where(p => p.RefKind != RefKind.Out && p.RefKind != RefKind.Ref && CSharpHelper.CreateAssertParameter(methodMock, ModelsHelper.ConvertToCamelCase(p.Name, true), true, avoidShouldBeNullParameterAssertion) != null);
                    var count = 0;
                    foreach (var parameterToAssert in parametersToAssert)
                    {
                        // Create a list of used values to be asserted
                        var addAssertionList = parameterToAssert.Type.TypeKind == TypeKind.Enum
                                               || parameterToAssert.Type.TypeKind == TypeKind.Struct
                                               || parameterToAssert.Type.Name.Equals("string", StringComparison.InvariantCultureIgnoreCase);
                        if (addAssertionList)
                        {
                            var suffix = parameterToAssert.Type.Name.Equals("Nullable", StringComparison.InvariantCultureIgnoreCase)
                                ? "?.ToString()"
                                : "";
                            paramsAssertionString += $", {ModelsHelper.ConvertToCamelCase(parameterToAssert.Name, true)}{suffix}";
                            count++;
                            if (count == 7)
                                break;
                        }
                    }

                    //assign some value to out parameters
                    var outParams = methodMock.Parameters.Where(p => p.RefKind == RefKind.Out);
                    if (outParams.Count() > 0)
                    {
                        foreach (var outParam in outParams)
                        {
                            var outParamValue = CSharpHelper.GetArgumentValue
                            (
                                new KeyValuePair<string, ParameterSymbol>
                                (
                                    outParam.Name,
                                    new ParameterSymbol(outParam.Type)
                                ),
                                true,
                                false,
                                generator
                            ).NormalizeWhitespace().ToString();

                            shimMethodSyntaxList.Add
                            (
                                ParseStatement
                                (
                                    $"{outParam.Name} = {outParamValue};"
                                )
                            );
                        }
                    }

                }

                var mockMethodName = methodMock.MockMethodName;
                if (mockMethodName.IndexOf("<") > -1)
                {
                    mockMethodName = mockMethodName.Substring(0, mockMethodName.IndexOf("<"));
                }
                shimMethodSyntaxList.Add
                (
                    ParseStatement
                    (
                        $"{CSharpHelper.MethodInvocationVariableName}.Add(Tuple.Create(nameof({fakesClassName}.{mockMethodName}){paramsAssertionString}));"
                    )
                );

                if (!methodMock.ReturnsVoid)
                {
                    ReturnStatementSyntax returnStatement;

                    // Add using nodes if needed
                    // TODO: use alias instead of classname
                    var classAlias = CSharpHelper.AddUsingNodesForType(methodMock.MockType, generator);
                    returnStatement = ReturnStatement
                    (
                        ParseExpression(setOrReturnValue)
                    );
                    shimMethodSyntaxList.Add(returnStatement);
                }

                SyntaxNode shimFinalExpression = BuildShimExpression(methodMock, parameterList, shimMethodSyntaxList, fakesClassName);

                if (generator.PlaceMocksInSetupMethod && !methodMock.IsInternalCall)
                {
                    if (!alreadyContainsShim)
                    {
                        generator.SetupNodes.Add(shimFinalExpression);
                    }
                }
                else
                {
                    unitTestSyntaxList.Add(shimFinalExpression);
                }
            }

            //if it's an enumerator we need to fake the list
            if (methodMock.EnumeratorTypeSymbol != null)
            {
                var typeSymbolName = "dynamic";
                var argumentValue = "new object()";
                var fakesClassName = $"Shim{methodMock.EnumeratorTypeSymbol.Name}";
                if (methodMock.EnumeratorItemTypeSymbol != null)
                {
                    typeSymbolName = methodMock.EnumeratorItemTypeSymbol.Name;
                    argumentValue = CSharpHelper.GetArgumentValue(new KeyValuePair<string, ParameterSymbol>("item", new ParameterSymbol(methodMock.EnumeratorItemTypeSymbol)), true, false, generator).NormalizeWhitespace().ToString();
                    fakesClassName = generator.AddUsingNodeIfNew(UsingDirective(ParseName(methodMock.EnumeratorItemTypeSymbol.ContainingNamespace.ToString())), fakesClassName);
                }
                unitTestSyntaxList.Add(ParseStatement($"{fakesClassName}.AllInstances.GetEnumerator = _ => new List<{typeSymbolName}>{{ {argumentValue} }}.GetEnumerator();"));
            }

            return unitTestSyntaxList;
        }

        public override IList<SyntaxNode> CreateMethodMockThatThrowsException(SyntaxNode expression, MockMethodMapper methodMock, SemanticModel model, ITestGenerator generator, string exceptionType)
        {
            var useStub = (methodMock.Symbol.ContainingType.IsAbstract && methodMock.Symbol.ContainingType.InstanceConstructors.Length == 0)
                          || methodMock.Symbol.IsAbstract;
            var classSuffix = useStub
                ? "Stub"
                : "Shim";
            var fakesClassName = $"{classSuffix}{methodMock.Symbol.ContainingType.Name}";
            // Add using nodes
            if (methodMock.Symbol.ContainingType.ContainingNamespace != null)
            {
                fakesClassName = generator.AddUsingNodeIfNew(UsingDirective(ParseName($"{methodMock.Symbol.ContainingType.ContainingNamespace}.Fakes")), fakesClassName);
            }
            var addInstanceParam = !methodMock.Symbol.IsStatic && !useStub &&
                                   !(methodMock.Symbol is IMethodSymbol methodSymbol && methodSymbol.IsExtensionMethod);
            var parameterList = CreateParametersListForMethodSymbols(methodMock, methodMock.Parameters, addInstanceParam);
            var unitTestSyntaxList = new List<SyntaxNode>();
            StatementSyntax shimMethodSyntax = null;

            if (exceptionType.Equals("Exception"))
            {
                shimMethodSyntax = ParseStatement($"throw new {exceptionType}(\"Dummy Exception\");");
            }
            else
            {
                shimMethodSyntax = ParseStatement($"throw new Shim{exceptionType}().Instance;");
            }


            SyntaxNode shimFinalExpression = BuildShimExpression(methodMock, parameterList, new List<StatementSyntax>{ shimMethodSyntax }, fakesClassName);

            unitTestSyntaxList.Add(shimFinalExpression);

            return unitTestSyntaxList;
        }

        private static SyntaxNode BuildShimExpression(MockMethodMapper methodMock, ParameterListSyntax parameterList, List<StatementSyntax> shimMethodSyntaxList, string fakesClassName)
        {
            var shimExpression = (parameterList.Parameters.Count == 1
                                  && !parameterList.Parameters[0].ToString().Contains("ref ")
                                  && !parameterList.Parameters[0].ToString().Contains("out "))
                ? SimpleLambdaExpression
                (
                    parameterList.Parameters.First(),
                    Block(shimMethodSyntaxList)
                )
                : (ExpressionSyntax) ParenthesizedLambdaExpression
                (
                    parameterList,
                    Block(shimMethodSyntaxList)
                );

            SyntaxNode shimFinalExpression = null;
            var useStub = (methodMock.Symbol.ContainingType.IsAbstract && methodMock.Symbol.ContainingType.InstanceConstructors.Length == 0)
                          || methodMock.Symbol.IsAbstract;
            if (useStub)
            {
                // Use Stubs instead of Shims for interfaces
                shimFinalExpression = ObjectCreationExpression(IdentifierName(fakesClassName))
                    .WithNewKeyword(Token
                    (
                        TriviaList(),
                        SyntaxKind.NewKeyword,
                        TriviaList()
                    ))
                    .WithArgumentList
                    (
                        ArgumentList()
                    )
                    .WithInitializer
                    (
                        InitializerExpression
                        (
                            SyntaxKind.ObjectInitializerExpression,
                            SingletonSeparatedList<ExpressionSyntax>
                            (
                                AssignmentExpression
                                (
                                    SyntaxKind.SimpleAssignmentExpression,
                                    IdentifierName(methodMock.MockMethodName),
                                    shimExpression
                                )
                            )
                        )
                    );
            }
            else
            {
                if (methodMock.IsGeneric)
                {
                    var shimArgs = SingletonSeparatedList
                    (
                        Argument(shimExpression)
                    );

                    var shimCode = ArgumentList
                    (
                        Token(SyntaxKind.OpenParenToken),
                        shimArgs,
                        Token(SyntaxKind.CloseParenToken)
                    );

                    shimFinalExpression = ExpressionStatement
                    (
                        InvocationExpression
                        (
                            BuildShimAccessExpression(methodMock, fakesClassName)
                        ).WithArgumentList(shimCode)
                    );
                }
                else
                {
                    shimFinalExpression = ExpressionStatement
                    (
                        AssignmentExpression
                        (
                            SyntaxKind.SimpleAssignmentExpression,
                            BuildShimAccessExpression(methodMock, fakesClassName),
                            shimExpression
                        )
                    );
                }
            }
            

            return shimFinalExpression;
        }

        private static MemberAccessExpressionSyntax BuildShimAccessExpression(MockMethodMapper methodMock, string fakesClassName)
        {
            var isStaticOrConstructor = methodMock.Symbol.IsStatic ||
                                        methodMock.IsConstructor ||
                                        (methodMock.Symbol is IMethodSymbol methodSymbol && (methodSymbol.IsExtensionMethod || methodSymbol.IsAbstract));

            var shimMethodName = methodMock.MockMethodName;

            return isStaticOrConstructor
                ? MemberAccessExpression
                (
                    SyntaxKind.SimpleMemberAccessExpression,
                    IdentifierName(fakesClassName),
                    IdentifierName(shimMethodName)
                )
                : MemberAccessExpression
                (
                    SyntaxKind.SimpleMemberAccessExpression,
                    MemberAccessExpression
                    (
                        SyntaxKind.SimpleMemberAccessExpression,
                        IdentifierName(fakesClassName),
                        IdentifierName("AllInstances")
                    ),
                    IdentifierName
                    (
                        shimMethodName
                    )
                );
        }

        private static ParameterListSyntax CreateParametersListForMethodSymbols(MockMethodMapper methodMock, IEnumerable<IParameterSymbol> parameters, bool addInstanceParam)
        {
            var list = new List<ParameterSyntax>();

            var parameterList = parameters.ToList();

            if (addInstanceParam)
            {
                if (parameterList.Any(o => o.RefKind == RefKind.Out) || parameterList.Any(o => o.RefKind == RefKind.Ref))
                {
                    // in this case we need to add types to parameters
                    list.Add(Parameter(Identifier($"{methodMock.Symbol.ContainingSymbol.Name} instance")));
                }
                else
                {
                    list.Add(Parameter(Identifier("_")));
                }
                
            }

            if (parameterList.Any(o => o.RefKind == RefKind.Out) || parameterList.Any(o => o.RefKind == RefKind.Ref))
            {
                // in this case we need to add types to parameters
                list.AddRange(ParseParameterList(string.Join(", ", parameterList.Select(o => (o.RefKind == RefKind.Out ? "out " : (o.RefKind == RefKind.Ref ?  "ref " : "")) + $"{o.Type.ToDisplayString()} {ModelsHelper.ConvertToCamelCase(o.Name, true)}"))).Parameters);
            }
            else
            {
                list.AddRange(parameterList.Select(p => Parameter(Identifier(ModelsHelper.ConvertToCamelCase(p.Name, true)))));
            }

            return ParameterList(SeparatedList(list));
        }
    }
}