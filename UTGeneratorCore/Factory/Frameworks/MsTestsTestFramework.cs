﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace UTGeneratorCore.Factory.Frameworks
{
    public class MsTestsTestFramework : TestFramework
    {
        protected MsTestsTestFramework() { }

        private static MsTestsTestFramework _instance;

        public static MsTestsTestFramework Instance => _instance ?? (_instance = new MsTestsTestFramework());

        public override AttributeListSyntax GetClassAttributeList()
        {
            return AttributeList(SeparatedList(new List<AttributeSyntax>
            {
                Attribute(IdentifierName("TestClass")),
                Attribute(IdentifierName("ExcludeFromCodeCoverage")),
                Attribute(IdentifierName($"TestCategory(\"{ModelsHelper.ToolVersionCategory}\")"))
            }));
        }

        public override IList<UsingDirectiveSyntax> GenerateUsingDirectives()
        {
            return new List<UsingDirectiveSyntax>();
        }

        public override AttributeListSyntax GetTestClassSetupAttributeList()
        {
            return AttributeList(SingletonSeparatedList(Attribute(IdentifierName("TestInitialize"))));
        }

        public override AttributeListSyntax GetTestClassOneTimeSetupAttributeList()
        {
            return AttributeList(SingletonSeparatedList(Attribute(IdentifierName("ClassInitialize"))));
        }

        public override AttributeListSyntax GetTestClassCleanupAttributeList()
        {
            return AttributeList(SingletonSeparatedList(Attribute(IdentifierName("TestCleanup"))));
        }

        public override AttributeListSyntax GetTestClassOneTimeCleanupAttributeList()
        {
            return AttributeList(SingletonSeparatedList(Attribute(IdentifierName("ClassCleanup"))));
        }

        public override AttributeListSyntax GetTestMethodAttributeList()
        {
            return AttributeList(SingletonSeparatedList(Attribute(IdentifierName("TestMethod"))));
        }

        public override AttributeListSyntax GetTestMethodDataSourceAttributeList(string variableName)
        {
            return AttributeList(SingletonSeparatedList(Attribute(IdentifierName($"DataSource(nameof({variableName}))"))));
        }
    }
}