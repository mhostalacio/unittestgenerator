﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace UTGeneratorCore.Factory.Frameworks
{
    public class NUnitTestFramework : TestFramework
    {
        protected NUnitTestFramework() { }

        private static NUnitTestFramework _instance;

        public static NUnitTestFramework Instance => _instance ?? (_instance = new NUnitTestFramework());

        public override AttributeListSyntax GetClassAttributeList()
        {
            return AttributeList(SeparatedList(new List<AttributeSyntax>
            {
                Attribute(IdentifierName("TestFixture")),
                Attribute(IdentifierName("ExcludeFromCodeCoverage")),
                Attribute(IdentifierName($"Category(\"{ModelsHelper.ToolVersionCategory}\")"))
            }));
        }

        public override IList<UsingDirectiveSyntax> GenerateUsingDirectives()
        {
            var usingStatements = new List<UsingDirectiveSyntax>
            {
                UsingDirective(ParseName("NUnit.Framework"))
            };
            return usingStatements;
        }

        public override AttributeListSyntax GetTestClassSetupAttributeList()
        {
            return AttributeList(SingletonSeparatedList(Attribute(IdentifierName("SetUp"))));
        }

        public override AttributeListSyntax GetTestClassOneTimeSetupAttributeList()
        {
            return AttributeList(SingletonSeparatedList(Attribute(IdentifierName("OneTimeSetUp"))));
        }

        public override AttributeListSyntax GetTestClassCleanupAttributeList()
        {
            return AttributeList(SingletonSeparatedList(Attribute(IdentifierName("TearDown"))));
        }

        public override AttributeListSyntax GetTestClassOneTimeCleanupAttributeList()
        {
            return AttributeList(SingletonSeparatedList(Attribute(IdentifierName("OneTimeTearDown"))));
        }

        public override AttributeListSyntax GetTestMethodAttributeList()
        {
            return AttributeList(SingletonSeparatedList(Attribute(IdentifierName("Test"))));
        }

        public override AttributeListSyntax GetTestMethodDataSourceAttributeList(string variableName)
        {
            return AttributeList(SingletonSeparatedList(Attribute(IdentifierName($"TestCaseSource(nameof({variableName}))"))));
        }
    }
}