﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace UTGeneratorCore.Factory
{
    public abstract class TestGenerator<T, M> : ITestGenerator
        where T : TestFramework
        where M : MockFramework
    {
        protected string SourceCode { private set; get; }
        public List<SyntaxNode> FieldNodes { private set; get; }
        public List<SyntaxNode> PropertyNodes { private set; get; }
        public List<SyntaxNode> SetupNodes { private set; get; }
        public List<SyntaxNode> OneTimeSetupNodes { private set; get; }
        public List<SyntaxNode> CleanupNodes { private set; get; }
        public List<SyntaxNode> OneTimeCleanupNodes { private set; get; }
        public List<SyntaxNode> MethodNodes { private set; get; }
        public List<UsingDirectiveSyntax> UsingsNodes { private set; get; }
        public bool PlaceMocksInSetupMethod { set; get; }
        protected IDictionary<ClassMapper, SemanticModel> MappedModel { set; get; }

        public T TestFramework { protected set; get; }

        public M MockFramework { private set; get; }
        public IDictionary<string, UsingMapper> UsingNodesMappings { private set; get; }

        public TestGenerator(string filePath, T testFramework, M mockFramework, IList<string> trustedAssembliesPaths)
        {
            SourceCode = File.ReadAllText(filePath);
            TestFramework = testFramework;
            MockFramework = mockFramework;
            ResetNodeLists();
        }

        public TestGenerator(T testFramework)
        {
            TestFramework = testFramework;
            ResetNodeLists();
        }

        public void ResetNodeLists()
        {
            FieldNodes = new List<SyntaxNode>();
            PropertyNodes = new List<SyntaxNode>();
            SetupNodes = new List<SyntaxNode>();
            OneTimeSetupNodes = new List<SyntaxNode>();
            CleanupNodes = new List<SyntaxNode>();
            OneTimeCleanupNodes = new List<SyntaxNode>();
            MethodNodes = new List<SyntaxNode>();
            if (UsingsNodes == null)
                UsingsNodes = new List<UsingDirectiveSyntax>();
            UsingNodesMappings = new Dictionary<string, UsingMapper>();
        }

        public abstract IList<SyntaxNode> MockSelectedCode(TextViewSelection selection);

        public CompilationUnitSyntax GenerateFullTestClass()
        {
            var memberList = new List<MemberDeclarationSyntax>();

            var tokens = new[]
            {
                Token(SyntaxKind.PublicKeyword),
                Token(SyntaxKind.PartialKeyword)
            };

            foreach (var classUnderTest in MappedModel.Keys)
            {
                ResetNodeLists();
                var classSyntaxList = new List<SyntaxNode>();

                // Generate each part and keep in each list
                GenerateUsingDirectives(classUnderTest);
                GenerateTestClassFields(classUnderTest);
                GenerateTestClassProperties(classUnderTest);
                GenerateTestClassOneTimeSetup(classUnderTest);
                GenerateTestClassSetup(classUnderTest);
                GenerateTestClassOneTimeCleanup(classUnderTest);
                GenerateTestClassCleanup(classUnderTest);
                GenerateTestCases(classUnderTest);

                // Add Fields to the list
                classSyntaxList.AddRange(FieldNodes);

                // Add Properties to the list
                classSyntaxList.AddRange(PropertyNodes);

                BlockSyntax body;
                MethodDeclarationSyntax method;

                // Add One Time Setup Method to the list
                if (OneTimeSetupNodes.Count > 0)
                {
                    body = Block
                    (
                        List(OneTimeSetupNodes)
                    );

                    method = MethodDeclaration
                        (
                            PredefinedType
                            (
                                Token(SyntaxKind.VoidKeyword)
                            ),
                            "OneTimeInitialize"
                        )
                        .WithModifiers
                        (
                            TokenList
                            (
                                Token(SyntaxKind.PublicKeyword)
                            )
                        )
                        .WithBody(body)
                        .WithAttributeLists
                        (
                            SingletonList
                            (
                                TestFramework.GetTestClassOneTimeSetupAttributeList()
                            )
                        )
                        .NormalizeWhitespace();

                    classSyntaxList.AddRange(new List<SyntaxNode> { method });
                }

                // Add Setup Method to the list
                if (SetupNodes.Count > 0)
                {
                    body = Block
                    (
                        List(SetupNodes)
                    );

                    method = MethodDeclaration
                        (
                            PredefinedType
                            (
                                Token(SyntaxKind.VoidKeyword)
                            ),
                            "Initialize"
                        )
                        .WithModifiers
                        (
                            TokenList
                            (
                                Token(SyntaxKind.PublicKeyword)
                            )
                        )
                        .WithBody(body)
                        .WithAttributeLists
                        (
                            SingletonList
                            (
                                TestFramework.GetTestClassSetupAttributeList()
                            )
                        )
                        .NormalizeWhitespace();

                    classSyntaxList.AddRange(new List<SyntaxNode> { method });
                }
                

                // Add Cleanup Method to the list
                if (CleanupNodes.Count > 0)
                {
                    body = Block(List(CleanupNodes));

                    method = MethodDeclaration
                        (
                            PredefinedType
                            (
                                Token(SyntaxKind.VoidKeyword)
                            ),
                            Identifier("CleanUp")
                        )
                        .WithAttributeLists
                        (
                            SingletonList
                            (
                                TestFramework.GetTestClassCleanupAttributeList()
                            )
                        )
                        .WithModifiers
                        (
                            TokenList
                            (
                                Token(SyntaxKind.PublicKeyword)
                            )
                        )
                        .WithBody(body)
                        .NormalizeWhitespace();

                    classSyntaxList.AddRange(new List<SyntaxNode> { method });
                }

                // Add One Time Cleanup Method to the list
                if (OneTimeCleanupNodes.Count > 0)
                {
                    body = Block(List(OneTimeCleanupNodes));

                    method = MethodDeclaration
                        (
                            PredefinedType
                            (
                                Token(SyntaxKind.VoidKeyword)
                            ),
                            Identifier("OneTimeCleanUp")
                        )
                        .WithAttributeLists
                        (
                            SingletonList
                            (
                                TestFramework.GetTestClassOneTimeCleanupAttributeList()
                            )
                        )
                        .WithModifiers
                        (
                            TokenList
                            (
                                Token(SyntaxKind.PublicKeyword)
                            )
                        )
                        .WithBody(body)
                        .NormalizeWhitespace();

                    classSyntaxList.AddRange(new List<SyntaxNode> { method });
                }

                classSyntaxList.AddRange(MethodNodes);

                var classGen = ClassDeclaration
                (
                    $"{classUnderTest.OriginalClassName}Tests"
                )
                .WithMembers
                (
                    new SyntaxList<MemberDeclarationSyntax>(classSyntaxList.Select(o => (MemberDeclarationSyntax)o))
                )
                .WithModifiers
                (
                    TokenList(tokens)
                )
                .WithAttributeLists
                (
                    SingletonList
                    (
                        TestFramework.GetClassAttributeList()
                    )
                );

                var nameSpace = NamespaceDeclaration
                (
                    ParseName("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"),
                    new SyntaxList<ExternAliasDirectiveSyntax>(),
                    new SyntaxList<UsingDirectiveSyntax>(),
                    new SyntaxList<MemberDeclarationSyntax>(classGen)
                );
                memberList.Add(nameSpace);
            }

            return CompilationUnit()
                .AddMembers(memberList.ToArray()).AddUsings
                (
                    UsingsNodes.ToArray()
                )
                .NormalizeWhitespace();
        }

        public abstract void GenerateTestProject(string folderPathAndProjectName);

        protected abstract void GenerateTestClassFields(ClassMapper classUnderTest);

        protected abstract void GenerateTestClassProperties(ClassMapper classUnderTest);

        protected abstract void GenerateTestClassSetup(ClassMapper classUnderTest);

        protected abstract void GenerateTestClassOneTimeSetup(ClassMapper classUnderTest);

        protected abstract void GenerateTestClassCleanup(ClassMapper classUnderTest);

        protected abstract void GenerateTestClassOneTimeCleanup(ClassMapper classUnderTest);

        protected abstract void GenerateTestCases(ClassMapper classUnderTest);

        protected abstract void GenerateUsingDirectives(ClassMapper classUnderTest);

        // TODO: get the code properly formatted with Roslyn
        public string FixFormatting(string code)
        {
            var result = new Regex("[}](\n|\r|\r\n)*(\\s)*[;]").Replace(code, "};");
            result = new Regex("[}](\n|\r|\r\n)*(\\s)*[)][;]").Replace(result, "});");
            result = new Regex("[;](\n|\r|\r\n)+(\\s)+(//)").Replace(result, match => match.ToString().Replace("//", "") + match.ToString().Replace(";", ""));
            result = new Regex("[}](\n|\r|\r\n)*(\\s)*[,](\n|\r|\r\n)*(\\s)*").Replace(result, "}, ");
            result = result.Replace("<String", "<string");
            result = result.Replace("<Boolean", "<bool");
            result = result.Replace("<Int32", "<int");
            result = result.Replace("<Int16", "<int");
            result = result.Replace(", String", ", string");
            result = result.Replace(", Boolean", ", bool");
            result = result.Replace(" Int32 ", " int ");
            result = result.Replace(" Int16 ", " int ");
            result = result.Replace(" Byte[", " byte[");
            result = result.Replace(" Byte ", " byte ");
            result = result.Replace(" Char[", " char[");
            result = result.Replace(" Char ", " char ");
            result = result.Replace("(String", "(string");
            result = result.Replace("(Boolean", "(bool");
            result = result.Replace("(Int32", "(int");
            result = result.Replace("(Int16", "(int");
            result = result.Replace("System.Guid", "Guid");
            result = result.Replace("System.Collections.Generic.List", "List");
            result = result.Replace("this.ShouldSatisfyAllConditions();", "");
            result = result.Replace("\"\"", "string.Empty");
            result = result.Replace("System.Collections.Generic.Dictionary", "Dictionary");
            var lines = result.Split('\n');
            result = string.Empty;
            foreach (var line in lines)
            {
                // add one empty line between fields & properties and following methods
                if (line.Contains("[OneTimeSetUp]"))
                {
                    var tab = line.Substring(0, line.IndexOf("[OneTimeSetUp]"));
                    result += line.Replace("[OneTimeSetUp]", "\n" + tab + "[OneTimeSetUp]");
                }
                // break assertions into multiple lines
                else if (line.Contains("() =>") && line.IndexOf("this.") > -1)
                {
                    var tab = line.Substring(0, line.IndexOf("this."));
                    tab = FixTabSpace(tab);
                    result += line.Replace("() =>", "\n" + tab + "() =>");
                }
                // break TestCaseData into multiple lines
                else if (line.Contains("new TestCaseData(") && line.IndexOf("return new[]{") > -1)
                {
                    var tab = line.Substring(0, line.IndexOf("return new[]{"));
                    var temp = line.Replace("new[]{", "new[]" + "\n" + tab + "{");
                    temp = temp.Replace("};", "\n" + tab + "};");
                    tab = FixTabSpace(tab);
                    result += temp.Replace("new TestCaseData(", "\n" + tab + "new TestCaseData(");
                }
                // break method parameters into multiple lines
                else if (line.Contains("(") && line.Contains(")") && line.Contains(", ") 
                         && !line.Contains("Shim") && !line.Contains("[Test") && !line.Contains("var ")
                         && !line.Contains("SetFieldOrProperty(")
                         && !line.Contains("SetStaticFieldOrProperty(")
                         && !line.Contains("return"))
                {
                    int lastWhiteSpace = line.TakeWhile(c => char.IsWhiteSpace(c)).Count();
                    var tab = line.Substring(0, lastWhiteSpace);
                    tab = FixTabSpace(tab);
                    result += line.Replace(", ", ", " + "\n" + tab);
                }
                else
                {
                    result += line;
                }
                result += "\n";
            }
            return result;
        }

        private static string FixTabSpace(string tab)
        {
            if (tab.Last() == 32)
            {
                tab += "    ";
            }
            else
            {
                tab += "    ";
            }

            return tab;
        }

        public virtual string AddUsingNodeIfNew(UsingDirectiveSyntax usingDirectiveSyntax, string className)
        {
            var alias = className.Replace("?", "");
            if (!string.IsNullOrWhiteSpace(alias))
            {
                var nameSpace = usingDirectiveSyntax.Name.ToString();
                if (!UsingNodesMappings.ContainsKey(className))
                {
                    UsingNodesMappings.Add(alias, new UsingMapper(alias, nameSpace, alias));

                    //in case we have class name mismatch we need to give it an alias
                    usingDirectiveSyntax = UsingDirective(ParseName($"{nameSpace}.{alias}"))
                        .WithAlias(NameEquals(IdentifierName(alias)));
                }
                else if (!UsingNodesMappings[className].NickNames.ContainsKey(nameSpace))
                {
                    alias = $"{alias}{UsingNodesMappings[className].NickNames.Count}";
                    UsingNodesMappings[className].NickNames.Add(nameSpace, alias);
                    
                    //in case we have class name mismatch we need to give it an alias
                    usingDirectiveSyntax = UsingDirective(ParseName($"{nameSpace}.{className}"))
                        .WithAlias(NameEquals(IdentifierName(alias)));
                }
                else
                {
                    alias = UsingNodesMappings[className].NickNames[nameSpace];
                    if (alias != className)
                    {
                        //in case we have class name mismatch we need to give it a nick name
                        usingDirectiveSyntax = UsingDirective(ParseName($"{nameSpace}.{className}"))
                            .WithAlias(NameEquals(IdentifierName(alias)));
                    }
                }
            }

            //check if exists and then add if new
            if (UsingsNodes.FirstOrDefault(o => o.ToString().Equals(usingDirectiveSyntax.ToString())) == null)
            {
                UsingsNodes.Add(usingDirectiveSyntax);
            }

            return alias;
        }
    }
}


