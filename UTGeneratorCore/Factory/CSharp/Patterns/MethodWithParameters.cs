﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
using static UTGeneratorCore.Factory.CSharp.CSharpHelper;

namespace UTGeneratorCore.Factory.CSharp.Patterns
{
    public class MethodWithParameters<T, M>
        where T : TestFramework
        where M : MockFramework
    {
        private readonly IMapper _method;
        private readonly CSharpTestGenerator<T, M> _generator;
        private IList<string> _testParametersUsedVariableNames = new List<string>();
        private readonly int? _index;

        public MethodWithParameters(IMapper method, CSharpTestGenerator<T, M> generator, int? index)
        {
            _method = method;
            _generator = generator;
            _index = index;
        }

        private string _methodName => _method.UniqueName ?? _method.Name;
        private string _testMethodName => string.Format(ParametrizedTestMethodName, _methodName, _index.HasValue ? _index.Value.ToString() : "");
        private string _testDataSourceName => $"{_methodName}TestDataSource";
        private readonly List<SyntaxNode> _parameterList = new List<SyntaxNode>();
        private readonly List<SyntaxNode> _methodStatementsList = new List<SyntaxNode>();
        private readonly List<ArgumentSyntax> _assertions = new List<ArgumentSyntax>();
        private readonly List<List<ExpressionSyntax>> _testDataSourceValues = new List<List<ExpressionSyntax>>();

        public void GenerateTestCase()
        {
            var usedVariableNames = new List<string>();

            ProcessTestParameters();

            CSharpHelper.ProcessMethodParametersForUnitTestCases(_method, _methodStatementsList, false, _testParametersUsedVariableNames, _generator);

            //if (!_method.IsVoid)
            //{
            //    //avoid adding complex objects to test parameter
            //    if (_method.MethodDeclaration is MethodDeclarationSyntax methodDeclaration &&
            //        methodDeclaration.ReturnType.Kind() != SyntaxKind.IdentifierName)
            //    {
            //        var symbolInfo = _method.Class.SemanticModel.GetSymbolInfo(methodDeclaration.ReturnType);
            //        if (symbolInfo.Symbol != null)
            //        {
            //            var type = ModelsHelper.GetTypeFromSymbol(symbolInfo.Symbol, _method.Class.SemanticModel);
            //            if (type != null)
            //            {
            //                var argumentValue = GetArgumentValue(new KeyValuePair<string, ParameterSymbol>("expectedResult", new ParameterSymbol(type)), false, false, _generator);
            //                _testDataSourceValues.Add(new List<ExpressionSyntax> { argumentValue });
            //                ITypeSymbol classThatImplements;
            //                if (ModelsHelper.ImplementsInterfaceOrBaseClass(type, typeof(IDisposable), out classThatImplements))
            //                {
            //                    _methodStatementsList.Add(ParseStatement($"{DisposableObjectsVariableName}.Add(expectedResult);"));
            //                }
            //            }
            //            else
            //            {
            //                _testDataSourceValues.Add(new List<ExpressionSyntax> { ParseExpression("null") });
            //            }
            //        }
            //        else
            //        {
            //            _testDataSourceValues.Add(new List<ExpressionSyntax> { ParseExpression("null")});
            //        }
            //    }
            //}

            if (_method.IfStatements.Count > 0)
            {
                foreach (var ifStatement in _method.IfStatements)
                {
                    foreach (var symbol in ifStatement.Value)
                    {
                        var type = ModelsHelper.GetTypeFromSymbol(symbol.Symbol, _method.Class.SemanticModel);
                        if (type != null && !usedVariableNames.Contains(symbol.Name))
                        {
                            if (symbol.AddIsNullFlag)
                            {
                                _testDataSourceValues.Add(new List<ExpressionSyntax>{ ParseExpression("false") });
                            }
                            else
                            {
                                var argumentValue = GetArgumentValueWithVariants(new KeyValuePair<string, ParameterSymbol>(symbol.Name, new ParameterSymbol(type)), _generator);
                                _testDataSourceValues.Add(argumentValue);
                            }
                            
                            usedVariableNames.Add(symbol.Name);
                            if (!symbol.AddIsNullFlag)
                            {
                                ITypeSymbol classThatImplements;
                                if (ModelsHelper.ImplementsInterfaceOrBaseClass(type, typeof(IDisposable), out classThatImplements))
                                {
                                    _methodStatementsList.Add(ParseStatement($"{DisposableObjectsVariableName}.Add({symbol.Name});"));
                                }
                            }
                        }
                        else
                        {
                            // TODO: check this case
                        }
                    } 
                }
            }

            if (_method.SwitchStatements.Count > 0)
            {
                foreach (var switchStatement in _method.SwitchStatements)
                {
                    var type = ModelsHelper.GetTypeFromSymbol(switchStatement.Value.Symbol, _method.Class.SemanticModel);
                    if (type != null && !usedVariableNames.Contains(switchStatement.Value.Name))
                    {
                        var argumentValue = GetArgumentValueWithVariants(new KeyValuePair<string, ParameterSymbol>(switchStatement.Value.Name, new ParameterSymbol(type)), _generator);
                        usedVariableNames.Add(switchStatement.Value.Name);
                        _testDataSourceValues.Add(argumentValue);
                        if (!switchStatement.Value.AddIsNullFlag)
                        {
                            ITypeSymbol classThatImplements;
                            if (ModelsHelper.ImplementsInterfaceOrBaseClass(type, typeof(IDisposable), out classThatImplements))
                            {
                                _methodStatementsList.Add(ParseStatement($"{DisposableObjectsVariableName}.Add({switchStatement.Value.Name});"));
                            }
                        }
                        if (argumentValue.Count == 1)
                        {
                            //get variants from switch
                            foreach (var section in switchStatement.Key.Sections)
                            {
                                if (section.Labels.Count > 0 && section.Labels.First() is CaseSwitchLabelSyntax caseLabel)
                                {
                                    if (caseLabel.Value is IdentifierNameSyntax identifier)
                                    {
                                        var symbolInfo = _method.Class.SemanticModel.GetSymbolInfo(identifier);
                                        var symbolInfoWrapper = ModelsHelper.ResolveSymbolWhenMismatch(identifier, symbolInfo);
                                        if (symbolInfoWrapper.Symbol != null && symbolInfoWrapper.Symbol is IFieldSymbol fieldSymbol && fieldSymbol.ConstantValue != null)
                                        {
                                            argumentValue.Add(LiteralExpression(SyntaxKind.StringLiteralExpression,
                                                Literal(fieldSymbol.ConstantValue.ToString())));
                                        }
                                        else
                                        {
                                            argumentValue.Add(caseLabel.Value);
                                        }
                                    }
                                    else
                                    {
                                        argumentValue.Add(caseLabel.Value);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // TODO: check this case
                    }
                }
            }

            //if (_method.ExternalReferences.Count > 0 && !_generator.AvoidMethodInvocationCounterAsTestParameter)
            //{
            //    _testDataSourceValues.Add(new List<ExpressionSyntax> { ParseExpression("new List<string>()") });
            //}

            ProcessMethodResult();

            ProcessExternalReferences();

            _generator.MethodNodes.Add(DeclareTestCaseSourcePropertyWithVariants(_testDataSourceName, _testDataSourceValues));

            var arranged = _methodStatementsList.Count > 0;

            if (arranged)
            {
                _methodStatementsList[0] = _methodStatementsList[0].WithLeadingTrivia(GetTriviaList(ArrangeComment));
            }

            CSharpHelper.FixStubNodesInTestMethods(_methodStatementsList);

            // Act Statements
            var actComment = arranged ? ActComment : ArrangeAndActComment;
            if (_method is ConstructorMapper)
            {
                _methodStatementsList.AddRange(GenerateClassConstructorInvokeStatement((ConstructorMapper)_method, actComment));
            }
            else
            {
                _methodStatementsList.AddRange(CreateActStatements(_method, actComment));
            }

            // Assert Statements
            var methodStatement = AssertAllStatement
            (
                AssertComment,
                ArgumentList
                (
                    Token(SyntaxKind.OpenParenToken),
                    SeparatedList(_assertions),
                    Token(SyntaxKind.CloseParenToken)
                )
            );

            _methodStatementsList.Add(methodStatement);

            var methodSyntax = GetMethodDeclarationSyntax();

            _generator.MethodNodes.Add(methodSyntax);
        }


        private void ProcessMethodResult()
        {
            //if (!_method.IsVoid)
            //{
            //    //avoid asserting complex objects
            //    if (_method.MethodDeclaration is MethodDeclarationSyntax methodDeclaration &&
            //        methodDeclaration.ReturnType.Kind() != SyntaxKind.IdentifierName)
            //    {
            //        var resultParameterType = IdentifierName(methodDeclaration.ReturnType.ToString());
            //        var resultParameterIdentifier = Identifier("expectedResult");
            //        var resultParameter = Parameter(resultParameterIdentifier).WithType(resultParameterType);

            //        _parameterList.Insert(0, resultParameter);

            //        _assertions.Add
            //        (
            //            Argument
            //            (
            //                ParseExpression($"() => {TestMethodResultVariableName}.{TestAssertShouldBe}(expectedResult)")
            //            )
            //        );
            //    }
            //    else
            //    {
            //        _assertions.Add
            //        (
            //            Argument
            //            (
            //                ParseExpression($"() => {TestMethodResultVariableName}.{TestAssertShouldNotBeNull}()")
            //            )
            //        );
            //    }
            //}

            //if (_method.ExternalReferences.Count > 0 && !_generator.AvoidMethodInvocationCounterAsTestParameter)
            //{
            //    _assertions.Add
            //    (
            //        Argument
            //        (
            //            ParseExpression($"() => {MethodInvocationVariableName}.{TestAssertShouldBe}({ModelsHelper.GetLocalVariabeAssertionName(MethodInvocationVariableName)})")
            //        )
            //    );
            //}
        }

        private void ProcessTestParameters()
        {
            var methodParameters = new List<ParameterSyntax>();
            if (_method.IfStatements.Count > 0)
            {
                foreach (var ifStatement in _method.IfStatements)
                {
                    foreach (var symbol in ifStatement.Value)
                    {
                        var type = ModelsHelper.GetTypeFromSymbol(symbol.Symbol, _method.Class.SemanticModel);
                        if (type != null && !_testParametersUsedVariableNames.Contains(symbol.Name))
                        {
                            _testParametersUsedVariableNames.Add(symbol.Name);
                            methodParameters.Add(Parameter
                                (
                                    Identifier(symbol.Name)
                                )
                                .WithType
                                (
                                    IdentifierName(symbol.AddIsNullFlag ? "bool" : ModelsHelper.ResolveTypeName(type))
                                ));
                        }
                        else
                        {
                            // TODO: check this case
                        }
                    }
                }
            }
            if (_method.SwitchStatements.Count > 0)
            {
                foreach (var switchStatement in _method.SwitchStatements)
                {
                    var type = ModelsHelper.GetTypeFromSymbol(switchStatement.Value.Symbol, _method.Class.SemanticModel);
                    if (type != null && !_testParametersUsedVariableNames.Contains(switchStatement.Value.Name))
                    {
                        _testParametersUsedVariableNames.Add(switchStatement.Value.Name);
                        methodParameters.Add(Parameter
                            (
                                Identifier(switchStatement.Value.Name)
                            )
                            .WithType
                            (
                                IdentifierName(switchStatement.Value.AddIsNullFlag ? "bool" : ModelsHelper.ResolveTypeName(type))
                            ));
                    }
                    else
                    {
                        // TODO: check this case
                    }
                }
            }
            _parameterList.AddRange(methodParameters);
        }

        private void ProcessExternalReferences()
        {
            //if (_method.ExternalReferences.Count > 0 && !_generator.AvoidMethodInvocationCounterAsTestParameter)
            //{
            //    var parameterSyntaxIdentifier = Identifier(ModelsHelper.GetLocalVariabeAssertionName(MethodInvocationVariableName));
            //    var parameterSyntaxType = GenericName(Identifier("IList")).WithTypeArgumentList(TypeArgumentList(SingletonSeparatedList<TypeSyntax>(PredefinedType(Token(SyntaxKind.StringKeyword)))));
            //    var parameterSyntax = Parameter(parameterSyntaxIdentifier).WithType(parameterSyntaxType);
            //    _parameterList.Add(parameterSyntax);
            //}
            foreach (var externalCall in ModelsHelper.GetUniqueExternalReferences(_method.ExternalReferences))
            {
                ProcessExternalReference(externalCall);
            }
        }

        private void ProcessExternalReference(KeyValuePair<SyntaxNode, MockMethodMapper> externalCall)
        {
            var syntaxNodes = _generator.MockFramework.CreateMethodMock(
                externalCall.Key,
                externalCall.Value,
                _method.Class.SemanticModel,
                _generator,
                false);

            _methodStatementsList.AddRange(syntaxNodes);

            //if (!externalCall.Value.AvoidMethodInvokeCount)
            //{
            //    foreach (var assertion in externalCall.Value.AssertionsToBeDone)
            //    {
            //        _assertions.Add(Argument(assertion.Expression));
            //    }
            //}
        }

        private MethodDeclarationSyntax GetMethodDeclarationSyntax()
        {
            var methodDeclaration = _generator.InternalGenerator.MethodDeclaration(
                _testMethodName,
                _parameterList,
                null,
                null,
                Accessibility.Public,
                DeclarationModifiers.None,
                _methodStatementsList) as MethodDeclarationSyntax;

            var attributeList = _generator.TestFramework.GetTestMethodDataSourceAttributeList(_testDataSourceName);
            var attributeSingletonList = SingletonList(attributeList);

            return methodDeclaration?.WithAttributeLists(attributeSingletonList);
        }

        private static ArgumentSyntax GetChangedAssertArgument(
            ExpressionStatementSyntax statementSyntax,
            KeyValuePair<SyntaxNode, MockMethodMapper> externalCall)
        {
            if (statementSyntax.Expression is ParenthesizedLambdaExpressionSyntax parenthesizedLambdaExpressionSyntax
                && parenthesizedLambdaExpressionSyntax.Body is InvocationExpressionSyntax statementSyntaxExpressionBody
                && statementSyntaxExpressionBody.Expression is MemberAccessExpressionSyntax memberAccessExpressionSyntax
                && memberAccessExpressionSyntax.Expression is IdentifierNameSyntax identifier)
            {
                if (identifier.ToString().Equals(externalCall.Value.MethodInvokeCountVariableName))
                {
                    var statementSyntaxAsString =
                        $"() => {externalCall.Value.MethodInvokeCountVariableName}.{TestAssertShouldBe}({ModelsHelper.GetLocalVariabeAssertionName(externalCall.Value.MethodInvokeCountVariableName)})";

                    statementSyntax = ExpressionStatement(ParseExpression(statementSyntaxAsString));
                }
            }

            var assertArgument = Argument(statementSyntax.Expression);

            return assertArgument;
        }
    }
}
