﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;

namespace UTGeneratorCore.Factory.CSharp.Patterns
{
    public class CSharpPatternMatcher<T, M>
        where T : TestFramework
        where M : MockFramework
    {
        private readonly IMapper _method;
        private readonly ClassMapper _class;
        private readonly CSharpTestGenerator<T, M> _generator;
        [ThreadStatic]
        private static int _index = 0;

        public CSharpPatternMatcher(IMapper method, CSharpTestGenerator<T, M> generator)
        {
            _method = method;
            _generator = generator;
        }
        public CSharpPatternMatcher(ClassMapper clazz, CSharpTestGenerator<T, M> generator)
        {
            _class = clazz;
            _generator = generator;
        }

        public void GenerateTestCase()
        {
            if (_method == null && _class == null)
            {
                return;
            }

            if (_method != null && (!(_method is MethodMapper) && !(_method is ConstructorMapper)))
            {
                return;
            }

            var methodMapper = _method as MethodMapper;
            var constructorMapper = _method as ConstructorMapper;

            if (constructorMapper != null)
            {
                if (constructorMapper.SwitchStatements.Count > 0 || (constructorMapper.IfStatements.Count > 0 && constructorMapper.IfStatements.Values.Any(o => o.Count > 0)))
                {
                    _index++;
                    new MethodWithParameters<T, M>(constructorMapper, _generator, _index).GenerateTestCase();
                }
                else
                {
                    _index++;
                    new MethodWithoutParameters<T, M>(constructorMapper, _generator, _index).GenerateTestCase();
                }
            }
            else if (_class != null)
            {
                if (_class.Fields.Any(o => o.HasDefaultValue))
                {
                    new FieldsWithDefaultValueMatch<T, M>(_class, _generator).GenerateTestCase();
                }
                // handle properties
                var propertiesWithoutBusinessLogic = new List<PropertyMapper>();

                foreach (var property in _class.Properties)
                {
                    bool isPropertyWithoutBusinessLogic = true;
                    if (property.HasSetter && property.HasGetter)
                    {
                        foreach (var accessor in property.Declaration.AccessorList.Accessors)
                        {
                            if (accessor.Body != null)
                            {
                                if (accessor.Body.Statements.Count > 1)
                                {
                                    isPropertyWithoutBusinessLogic = false;
                                    break;
                                }

                                if (accessor.Body.Statements.Count > 0
                                    && !(accessor.Body.Statements[0] is ReturnStatementSyntax
                                         || accessor.Body.Statements[0] is ExpressionStatementSyntax expr && expr.Expression is AssignmentExpressionSyntax))
                                {
                                    isPropertyWithoutBusinessLogic = false;
                                    break;
                                }
                            }  
                        }
                    }
                    if (isPropertyWithoutBusinessLogic && property.Declaration.AccessorList.Accessors.Count == 2)
                    {
                        propertiesWithoutBusinessLogic.Add(property);
                    }
                    else
                    {
                        if (property.HasSetter && property.HasGetter)
                        {
                            new PropertyMatch<T, M>(property, _generator).GenerateTestCase();
                        }
                        else if (!property.HasSetter && property.HasGetter)
                        {
                            new PropertyGetterOnlyMatch<T, M>(property, _generator).GenerateTestCase();
                        }
                    }
                }

                if (propertiesWithoutBusinessLogic.Count > 0)
                {
                    //create single test for these cases
                    new MultiplePropertiesInSingleTestMatch<T, M>(propertiesWithoutBusinessLogic, _generator).GenerateTestCase();
                }
            }
            else if (methodMapper != null)
            {
                if (methodMapper.MethodDeclaration.Body != null && !methodMapper.MethodDeclaration.Body.DescendantNodes().Any())
                {
                    new EmptyMethods<T, M>(methodMapper, _generator).GenerateTestCase();
                }
                else if (methodMapper.SwitchStatements.Count > 0 || (methodMapper.IfStatements.Count > 0 && methodMapper.IfStatements.Values.Any(o => o.Count > 0)))
                {
                    new MethodWithParameters<T, M>(methodMapper, _generator, null).GenerateTestCase();
                }
                else
                {
                    new MethodWithoutParameters<T, M>(methodMapper, _generator, null).GenerateTestCase();
                }
                // create extra test cases when methods have catch blocks
                if (methodMapper.CatchClauses.Count > 0)
                {
                    foreach (var catchClause in methodMapper.CatchClauses)
                    {
                        new MethodThatThowsExceptions<T, M>(methodMapper, _generator, catchClause).GenerateTestCase();
                    }
                }
            }


        }

    }
}
