﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
using static UTGeneratorCore.Factory.CSharp.CSharpHelper;

namespace UTGeneratorCore.Factory.CSharp.Patterns
{
    public class MethodWithoutParameters<T, M>
        where T : TestFramework
        where M : MockFramework
    {
        private readonly IMapper _method;
        private readonly CSharpTestGenerator<T, M> _generator;
        private readonly List<SyntaxNode> _methodStatementsList = new List<SyntaxNode>();
        private readonly List<ArgumentSyntax> _assertions = new List<ArgumentSyntax>();
        private readonly int? _index;

        public MethodWithoutParameters(IMapper method, CSharpTestGenerator<T, M> generator, int? index)
        {
            _method = method;
            _generator = generator;
            _index = index;
        }

        public void GenerateTestCase()
        {
            // Arrange Statements
            var arranged = ProcessArranges();
            CSharpHelper.FixStubNodesInTestMethods(_methodStatementsList);

            // Act Statements
            if (_method is ConstructorMapper)
            {
                _methodStatementsList.AddRange(GenerateClassConstructorInvokeStatement((ConstructorMapper)_method, arranged ? ActComment : ArrangeAndActComment));
            }
            else
            {
                _methodStatementsList.AddRange(CreateActStatements(_method, arranged ? ActComment : ArrangeAndActComment));
            }

            // Assert Statements
            ProcessAssertions();
        }

        private bool ProcessArranges()
        {
            CSharpHelper.ProcessMethodParametersForUnitTestCases(_method, _methodStatementsList, true, null, _generator);

            ProcessExternalReferences();

            var arranged = _methodStatementsList.Count > 0;

            if (arranged)
            {
                _methodStatementsList[0] = _methodStatementsList[0].WithLeadingTrivia(GetTriviaList(ArrangeComment));
            }

            return arranged;
        }

        private void ProcessAssertions()
        {
            //if (!_method.IsVoid)
            //{
            //    _assertions.Add(
            //        Argument(ParseExpression
            //        (
            //            $"() => {TestMethodResultVariableName}.{CSharpHelper.TestAssertShouldNotBeNull}()"
            //        )));
            //}
            //if (_method.ExternalReferences.Count > 0)
            //{
            //    _assertions.Add
            //    (
            //        Argument
            //        (
            //            ParseExpression($"() => {MethodInvocationVariableName}.{TestAssertShouldBe}(new List<IStructuralEquatable>())")
            //        )
            //    );
            //}
            _methodStatementsList.Add
            (
                AssertAllStatement
                (
                    AssertComment,
                    ArgumentList
                    (
                        Token(SyntaxKind.OpenParenToken),
                        SeparatedList(_assertions),
                        Token(SyntaxKind.CloseParenToken)
                    )
                )
            );

            var testMethodName = string.Format(TestMethodName, _method.UniqueName ?? _method.Name);
            if (_method is ConstructorMapper)
            {
                var idx = _index.HasValue
                    ? _index.Value.ToString()
                    : "";
                testMethodName = $"Constructor{idx}_OnValidCall_InstantiateObject";
            }

            var methodDeclarationSyntax = _generator.InternalGenerator.MethodDeclaration(
                testMethodName,
                null,
                null,
                null,
                Accessibility.Public,
                DeclarationModifiers.None,
                _methodStatementsList) as MethodDeclarationSyntax;

            var methodSyntax = methodDeclarationSyntax?.WithAttributeLists
            (
                SingletonList
                (
                    _generator.TestFramework.GetTestMethodAttributeList()
                )
            );

            _generator.MethodNodes.Add(methodSyntax);
        }

        private void ProcessExternalReferences()
        {
            if (_method.ExternalReferences.Count == 0)
            {
                return;
            }

            foreach (var externalCall in _method.ExternalReferences)
            {
                _methodStatementsList.AddRange
                (
                    _generator.MockFramework.CreateMethodMock
                    (
                        externalCall.Key,
                        externalCall.Value,
                        _method.Class.SemanticModel,
                        _generator,
                        false
                    )
                );

                if (externalCall.Value.AssertionsToBeDone.Count <= 0)
                {
                    continue;
                }

                foreach (var assertion in externalCall.Value.AssertionsToBeDone)
                {
                    _assertions.Add(Argument(assertion.Expression));
                }
            }
        }
    }
}
