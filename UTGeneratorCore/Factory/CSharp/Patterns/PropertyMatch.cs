﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
using static UTGeneratorCore.Factory.CSharp.CSharpHelper;
using static UTGeneratorCore.Models.ModelsHelper;

namespace UTGeneratorCore.Factory.CSharp.Patterns
{
    public class PropertyMatch<T, M>
        where T : TestFramework
        where M : MockFramework
    {
        private readonly PropertyMapper _property;
        private readonly CSharpTestGenerator<T, M> _generator;
        private string _testMethodName;
        private readonly List<SyntaxNode> _methodStatementsList = new List<SyntaxNode>();
        private readonly List<ArgumentSyntax> _assertions = new List<ArgumentSyntax>();
        private List<string> _usedVariableNames = new List<string>();

        public PropertyMatch(PropertyMapper property, CSharpTestGenerator<T, M> generator)
        {
            _property = property;
            _generator = generator;
        }


        /// <summary>
        /// Generates ready-to-run Test Case for properties that have setter
        /// </summary>
        public void GenerateTestCase()
        {
            if (_property.HasSetter)
            {
                _testMethodName = $"{ConvertToPascalCase(_property.Name)}_SetValue_ReturnsTheSameValue";

                // Arrange Section
                ProcessArrangeSection();
                HandlePropertyGetterAndSetter<T, M>(_property, _usedVariableNames, _generator, _methodStatementsList, _assertions);

                // Act Section
                if (_property.HasPublicSetter)
                {
                    _methodStatementsList.Add(ParseStatement($"{TestObjectVariableName}.{_property.Name} = givenValue;").WithLeadingTrivia(GetTriviaList(ActComment)));
                }
                else
                {
                    _methodStatementsList.Add(ParseStatement($"{PrivateObjectMemberName}.SetFieldOrProperty(\"{_property.Name}\", givenValue);").WithLeadingTrivia(GetTriviaList(ActComment)));
                }


                // Assert Section
                var assert = "";
                if (_property.HasPublicGetter)
                {
                    assert = $"{TestObjectVariableName}.{_property.Name}.ShouldBe(givenValue)";
                }
                else
                {
                    assert = $"{PrivateObjectMemberName}.GetFieldOrProperty(\"{_property.Name}\").ShouldBe(givenValue)";
                }


                if (_assertions.Count > 0)
                {
                    _assertions.Insert
                    (
                        0, Argument
                        (
                            ParseExpression($"() => {assert}")
                        )
                    );

                    _methodStatementsList.Add
                    (
                        AssertAllStatement
                        (
                            AssertComment,
                            ArgumentList
                            (
                                Token(SyntaxKind.OpenParenToken),
                                SeparatedList(_assertions),
                                Token(SyntaxKind.CloseParenToken)
                            )
                        ).WithLeadingTrivia(GetTriviaList(AssertComment))
                    );

                }
                else
                {
                    _methodStatementsList.Add(ParseStatement($"{assert};").WithLeadingTrivia(GetTriviaList(AssertComment)));
                }

                var methodSyntax = GetMethodDeclarationSyntax();

                _generator.MethodNodes.Add(methodSyntax);
            }
        }

        private MethodDeclarationSyntax GetMethodDeclarationSyntax()
        {
            var methodDeclaration = _generator.InternalGenerator.MethodDeclaration(
                _testMethodName,
                new List<SyntaxNode>(),
                null,
                null,
                Accessibility.Public,
                DeclarationModifiers.None,
                _methodStatementsList) as MethodDeclarationSyntax;

            var attributeList = _generator.TestFramework.GetTestMethodAttributeList();
            var attributeSingletonList = SingletonList(attributeList);

            var methodSyntax = methodDeclaration?.WithAttributeLists(attributeSingletonList);

            return methodSyntax;
        }

        private void ProcessArrangeSection()
        {
            var variableType = "var";
            var shouldBeConstant = VariableShouldBeDeclaredAsConstant(_property.PropertySymbol.Type);

            if (shouldBeConstant)
            {
                variableType = _property.PropertySymbol.Type.ToString();
            }

            var variableForParam = LocalDeclarationStatement
            (
                VariableDeclaration
                (
                    IdentifierName
                    (
                        Identifier(variableType)
                    )
                )
                    .WithVariables
                    (
                        SingletonSeparatedList
                        (
                            VariableDeclarator
                            (
                                Identifier("givenValue")
                            )
                                .WithInitializer
                                (
                                    EqualsValueClause
                                    (
                                        GetArgumentValue
                                        (
                                            new KeyValuePair<string, ParameterSymbol>(_property.Name, new ParameterSymbol(_property.PropertySymbol.Type)), 
                                            true, false, _generator
                                        )
                                    )
                                )
                        )
                    )
            );

            if (shouldBeConstant)
            {
                variableForParam =
                    variableForParam.WithModifiers
                    (
                        TokenList
                        (
                            Token(SyntaxKind.ConstKeyword)
                        )
                    );
            }

            _methodStatementsList.Add(variableForParam.WithLeadingTrivia(GetTriviaList(ArrangeComment)));
            ITypeSymbol classThatImplements;
            if (_property.PropertySymbol.Type is ITypeSymbol typeSymbol && ModelsHelper.ImplementsInterfaceOrBaseClass(typeSymbol, typeof(IDisposable), out classThatImplements)
                && ClassesToNotShim.Contains(typeSymbol.Name))
            {
                _methodStatementsList.Add(ParseStatement($"{DisposableObjectsVariableName}.Add({_property.Name});"));
            }
        }
    }
}
