﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
using static UTGeneratorCore.Factory.CSharp.CSharpHelper;

namespace UTGeneratorCore.Factory.CSharp.Patterns
{
    public class MethodThatThowsExceptions<T, M>
        where T : TestFramework
        where M : MockFramework
    {
        private readonly MethodMapper _method;
        private readonly CSharpTestGenerator<T, M> _generator;
        private readonly List<SyntaxNode> _methodStatementsList = new List<SyntaxNode>();
        private readonly List<ArgumentSyntax> _assertions = new List<ArgumentSyntax>();
        private readonly KeyValuePair<CatchClauseSyntax, SyntaxNode> _catchClause;
        private List<string> _usedVariableNames;

        public MethodThatThowsExceptions(MethodMapper method, CSharpTestGenerator<T, M> generator, KeyValuePair<CatchClauseSyntax, SyntaxNode> catchClause)
        {
            _method = method;
            _generator = generator;
            _catchClause = catchClause;
        }

        public void GenerateTestCase()
        {
            _usedVariableNames = new List<string>();

            // Arrange Statements
            var arranged = ProcessArranges();

            // Act Statements
            _methodStatementsList.AddRange(CreateActStatementsWithAction(_method, arranged ? ActComment : ArrangeAndActComment));

            // Assert Statements
            ProcessAssertions();
        }

        private bool ProcessArranges()
        {
            CSharpHelper.ProcessMethodParametersForUnitTestCases(_method, _methodStatementsList, true, null, _generator);
            //mock something to throw exception
            var symbolInfo = ModelsHelper.GetSymbolInfoFromSyntaxNode(_catchClause.Value, _method.Class.SemanticModel, _method.MethodDeclaration, _method.Class.Declaration, _catchClause.Value);
            var temp = new Dictionary<SyntaxNode, MockMethodMapper>();
            ModelsHelper.ExtractExternalReferencesFromSymbol(_catchClause.Value, _method.Class, temp, false, symbolInfo);
            if (temp.Count > 0)
            {
                _methodStatementsList.AddRange
                (
                    _generator.MockFramework.CreateMethodMockThatThrowsException
                    (
                        temp.First().Key,
                        temp.First().Value,
                        _method.Class.SemanticModel,
                        _generator,
                        GetExceptionType()
                    )
                );
            }

            // mock and assert logic inside catch block
            if (_catchClause.Key.Block.DescendantNodes().Any())
            {
                var _externalRefsInCatchBlock = new Dictionary<SyntaxNode, MockMethodMapper>();
                foreach (var node in _catchClause.Key.Block.DescendantNodes())
                {
                    ModelsHelper.ExtractExternalReferences(node, _method.Class, _externalRefsInCatchBlock, true, _method.MethodDeclaration);
                }

                if (_externalRefsInCatchBlock.Count > 0)
                {
                    foreach (var externalRef in _externalRefsInCatchBlock)
                    {
                        if (!_usedVariableNames.Contains(externalRef.Value.MethodInvokeCountVariableName))
                        {
                            _usedVariableNames.Add(externalRef.Value.MethodInvokeCountVariableName);
                            var syntaxNodes = _generator.MockFramework.CreateMethodMock(
                                externalRef.Key,
                                externalRef.Value,
                                _method.Class.SemanticModel,
                                _generator,
                                true);

                            _methodStatementsList.AddRange(syntaxNodes);
                            foreach (var assertion in externalRef.Value.AssertionsToBeDone)
                            {
                                _assertions.Add(Argument(assertion.Expression));
                            }
                        }
                    }
                    //_assertions.Add
                    //(
                    //    Argument
                    //    (
                    //        ParseExpression($"() => {MethodInvocationVariableName}.{TestAssertShouldBe}(new List<IStructuralEquatable>())")
                    //    )
                    //);
                }
            }

            var arranged = _methodStatementsList.Count > 0;

            if (arranged)
            {
                _methodStatementsList[0] = _methodStatementsList[0].WithLeadingTrivia(GetTriviaList(ArrangeComment));
            }

            return arranged;
        }

        private void ProcessAssertions()
        {
            
            var exceptionType = GetExceptionType();
            var testMethodName = string.Format(TestMethodNameThrowsException, _method.UniqueName ?? _method.Name, exceptionType);
            

            if (_catchClause.Key.Block.DescendantNodes().OfType<ThrowExpressionSyntax>().Any()
            || _catchClause.Key.Block.DescendantNodes().OfType<ThrowStatementSyntax>().Any())
            {
                testMethodName = string.Format(TestMethodNameDoesNotThrowException, _method.UniqueName ?? _method.Name, exceptionType);
                if (_assertions.Count > 0)
                {
                    _assertions.Insert(0,
                    
                        Argument(ExpressionStatement
                        (
                            ParseExpression
                            (
                                $"() => {TestMethodActionVariableName}.ShouldThrow<{exceptionType}>()"
                            )
                        ).Expression)
                    );
                }
                else
                {
                    _methodStatementsList.Add
                    (
                        ParseStatement($"{TestMethodActionVariableName}.ShouldThrow<{exceptionType}>();").WithLeadingTrivia(GetTriviaList(AssertComment))
                    );
                }
                
            }
            else
            {
                if (_assertions.Count > 0)
                {
                    _assertions.Insert(0,
                    
                        Argument(ExpressionStatement
                        (
                            ParseExpression
                            (
                                $"() => {TestMethodActionVariableName}.ShouldNotThrow()"
                            )
                        ).Expression)
                    );
                }
                else
                {
                    _methodStatementsList.Add
                    (
                        ParseStatement($"{TestMethodActionVariableName}.ShouldNotThrow();").WithLeadingTrivia(GetTriviaList(AssertComment))
                    );
                }
            }

            if (_assertions.Count > 1)
            {
                _methodStatementsList.Add
                (
                    AssertAllStatement
                    (
                        AssertComment,
                        ArgumentList
                        (
                            Token(SyntaxKind.OpenParenToken),
                            SeparatedList(_assertions),
                            Token(SyntaxKind.CloseParenToken)
                        )
                    ).WithLeadingTrivia(GetTriviaList(AssertComment))
                );
            }
                

            var methodDeclarationSyntax = _generator.InternalGenerator.MethodDeclaration(
                testMethodName,
                null,
                null,
                null,
                Accessibility.Public,
                DeclarationModifiers.None,
                _methodStatementsList) as MethodDeclarationSyntax;

            var methodSyntax = methodDeclarationSyntax?.WithAttributeLists
            (
                SingletonList
                (
                    _generator.TestFramework.GetTestMethodAttributeList()
                )
            );

            _generator.MethodNodes.Add(methodSyntax);
        }

        private string GetExceptionType()
        {
            var exceptionType = "Exception";
            if (_catchClause.Key.Declaration != null && _catchClause.Key.Declaration.Type != null
                                                     && _catchClause.Key.Declaration.Type is IdentifierNameSyntax identifierNameSyntax)
            {
                exceptionType = identifierNameSyntax.ToString();
            }

            return exceptionType;
        }
    }
}
