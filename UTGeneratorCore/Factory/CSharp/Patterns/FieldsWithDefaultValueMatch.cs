﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
using static UTGeneratorCore.Factory.CSharp.CSharpHelper;
using static UTGeneratorCore.Models.ModelsHelper;

namespace UTGeneratorCore.Factory.CSharp.Patterns
{
    public class FieldsWithDefaultValueMatch<T, M>
        where T : TestFramework
        where M : MockFramework
    {
        private readonly ClassMapper _class;
        private readonly CSharpTestGenerator<T, M> _generator;
        private string _testMethodName;
        private readonly List<SyntaxNode> _methodStatementsList = new List<SyntaxNode>();
        private readonly List<ArgumentSyntax> _assertions = new List<ArgumentSyntax>();

        public FieldsWithDefaultValueMatch(ClassMapper clazz, CSharpTestGenerator<T, M> generator)
        {
            _class = clazz;
            _generator = generator;
        }

        /// <summary>
        /// Generates ready-to-run Test Case for fields with default values
        /// </summary>
        public void GenerateTestCase()
        {
            _testMethodName = $"ClassFields_GetValues_ReturnsDefaultValues";

            foreach (var field in _class.Fields)
            {
                if (field.HasDefaultValue)
                {
                    var val = string.Empty;
                    if (field.Declaration.Declaration.Variables[0].Initializer.Value is IdentifierNameSyntax identifierNameSyntax)
                    {
                        var otherField = _class.Fields.FirstOrDefault(o => o.Name.Equals(identifierNameSyntax.Identifier.ToString()));
                        if (otherField != null)
                        {
                            val = GetFieldAccessCode(otherField, _class);
                        }
                    }
                    else
                    {
                        val = field.Declaration.Declaration.Variables[0].Initializer.Value.ToString();

                    }

                    var cast = "";
                    if (field.FieldSymbol.Type.TypeKind == TypeKind.Struct)
                    {
                        cast = $"({field.Declaration.Declaration.Type})";
                    }
                    _assertions.Add
                    (
                        Argument
                        (
                            ParseExpression($"() => {GetFieldAccessCode(field, _class)}.{TestAssertShouldBe}({cast}{val})")
                        )
                    );
                }
            }
            var methodStatement = AssertAllStatement
            (
                AssertComment,
                ArgumentList
                (
                    Token(SyntaxKind.OpenParenToken),
                    SeparatedList(_assertions),
                    Token(SyntaxKind.CloseParenToken)
                )
            );

            _methodStatementsList.Add(methodStatement.WithLeadingTrivia(GetTriviaList(ArrangeActAndAssertComment)));

            var methodSyntax = GetMethodDeclarationSyntax();

            _generator.MethodNodes.Add(methodSyntax);
        }

        private MethodDeclarationSyntax GetMethodDeclarationSyntax()
        {
            var methodDeclaration = _generator.InternalGenerator.MethodDeclaration(
                _testMethodName,
                new List<SyntaxNode>(),
                null,
                null,
                Accessibility.Public,
                DeclarationModifiers.None,
                _methodStatementsList) as MethodDeclarationSyntax;

            var attributeList = _generator.TestFramework.GetTestMethodAttributeList();
            var attributeSingletonList = SingletonList(attributeList);

            var methodSyntax = methodDeclaration?.WithAttributeLists(attributeSingletonList);

            return methodSyntax;
        }

    }
}
