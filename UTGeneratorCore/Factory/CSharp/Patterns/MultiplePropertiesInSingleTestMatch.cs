﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
using static UTGeneratorCore.Factory.CSharp.CSharpHelper;
using static UTGeneratorCore.Models.ModelsHelper;

namespace UTGeneratorCore.Factory.CSharp.Patterns
{
    public class MultiplePropertiesInSingleTestMatch<T, M>
        where T : TestFramework
        where M : MockFramework
    {
        private readonly IList<PropertyMapper> _properties;
        private readonly CSharpTestGenerator<T, M> _generator;
        private readonly List<SyntaxNode> _methodStatementsList = new List<SyntaxNode>();
        private readonly List<SyntaxNode> _parameterList = new List<SyntaxNode>();
        private readonly List<List<ExpressionSyntax>> _testDataSourceValues = new List<List<ExpressionSyntax>>();

        public MultiplePropertiesInSingleTestMatch(IList<PropertyMapper> properties, CSharpTestGenerator<T, M> generator)
        {
            _properties = properties;
            _generator = generator;
        }


        /// <summary>
        /// Generates ready-to-run Test Case for properties that have setter
        /// </summary>
        public void GenerateTestCase()
        {
            var testMethodName = "Properties_SetAndGet_ReturnsTheSameValue";
            var testDataSourceName = "PropertiesSetAndGetTestDataSource";

            // Test Datasource
            _testDataSourceValues.Add(new List<ExpressionSyntax>());
            _testDataSourceValues.Add(new List<ExpressionSyntax>());
            foreach (var property in _properties)
            {
                _testDataSourceValues[0].Add(ParseExpression($"\"{property.Name}\""));
                _testDataSourceValues[1].Add(GetArgumentValue
                (
                    new KeyValuePair<string, ParameterSymbol>(property.Name, new ParameterSymbol(property.PropertySymbol.Type)),
                    true, true, _generator
                ));
            }
            _generator.MethodNodes.Add(DeclareTestCaseSourcePropertyWithoutVariants(testDataSourceName, _testDataSourceValues));

            // Test Parameters
            _parameterList.Add(Parameter(Identifier("propertyName")).WithType(IdentifierName("string")));
            _parameterList.Add(Parameter(Identifier("setValue")).WithType(IdentifierName("object")));

            // Arrange, Act, Assert
            _methodStatementsList.Add(ParseStatement($"{PrivateObjectMemberName}.SetProperty(propertyName, setValue);").WithLeadingTrivia(GetTriviaList(ArrangeComment)));
            _methodStatementsList.Add(ParseStatement($"{TestMethodResultVariableName} = {PrivateObjectMemberName}.GetProperty(propertyName);").WithLeadingTrivia(GetTriviaList(ActComment)));
            _methodStatementsList.Add(ParseStatement($"{TestMethodResultVariableName}.ShouldBe(setValue);").WithLeadingTrivia(GetTriviaList(AssertComment)));

            var methodDeclarationSyntax = _generator.InternalGenerator.MethodDeclaration(
                testMethodName,
                _parameterList,
                null,
                null,
                Accessibility.Public,
                DeclarationModifiers.None,
                _methodStatementsList) as MethodDeclarationSyntax;

            var methodSyntax = methodDeclarationSyntax?.WithAttributeLists
            (
                SingletonList
                (
                    _generator.TestFramework.GetTestMethodDataSourceAttributeList(testDataSourceName)
                )
            );

            _generator.MethodNodes.Add(methodSyntax);
        }
    }

}
