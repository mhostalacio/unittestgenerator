﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
using static UTGeneratorCore.Factory.CSharp.CSharpHelper;
using static UTGeneratorCore.Models.ModelsHelper;

namespace UTGeneratorCore.Factory.CSharp.Patterns
{
    public class PropertyGetterOnlyMatch<T, M>
        where T : TestFramework
        where M : MockFramework
    {
        private readonly PropertyMapper _property;
        private readonly CSharpTestGenerator<T, M> _generator;
        private string _testMethodName;
        private readonly List<SyntaxNode> _methodStatementsList = new List<SyntaxNode>();
        private readonly List<ArgumentSyntax> _assertions = new List<ArgumentSyntax>();
        private List<string> _usedVariableNames = new List<string>();

        public PropertyGetterOnlyMatch(PropertyMapper property, CSharpTestGenerator<T, M> generator)
        {
            _property = property;
            _generator = generator;
        }


        /// <summary>
        /// Generates ready-to-run Test Case for properties that have setter
        /// </summary>
        public void GenerateTestCase()
        {
            if (_property.HasGetter && !_property.HasSetter)
            {
                _testMethodName = $"{ConvertToPascalCase(_property.Name)}_GetValue_ReturnsObject";

                // Arrange Section
                HandlePropertyGetterAndSetter<T, M>(_property, _usedVariableNames, _generator, _methodStatementsList, _assertions);
                CSharpHelper.FixStubNodesInTestMethods(_methodStatementsList);
                var arranged = _methodStatementsList.Count > 0;

                if (arranged)
                {
                    _methodStatementsList[0] = _methodStatementsList[0].WithLeadingTrivia(GetTriviaList(ArrangeComment));
                }


                // Act Section
                if (_property.HasPublicGetter)
                {
                    _methodStatementsList.Add(ParseStatement($"{TestMethodResultVariableName} = {TestObjectVariableName}.{_property.Name};").WithLeadingTrivia(GetTriviaList(arranged ? ActComment : ArrangeAndActComment)));
                }
                else
                {
                    _methodStatementsList.Add(ParseStatement($"{TestMethodResultVariableName} = {PrivateObjectMemberName}.GetFieldOrProperty(\"{_property.Name}\");").WithLeadingTrivia(GetTriviaList(arranged ? ActComment : ArrangeAndActComment)));
                }

                // Assert Section
                if (_assertions.Count > 0)
                {
                    _assertions.Add
                    (
                        Argument
                        (
                            ParseExpression($"() => {TestMethodResultVariableName}.ShouldNotBeNull()")
                        )
                    );

                    _methodStatementsList.Add
                    (
                        AssertAllStatement
                        (
                            AssertComment,
                            ArgumentList
                            (
                                Token(SyntaxKind.OpenParenToken),
                                SeparatedList(_assertions),
                                Token(SyntaxKind.CloseParenToken)
                            )
                        ).WithLeadingTrivia(GetTriviaList(AssertComment))
                    );

                }
                else
                {
                    _methodStatementsList.Add(ParseStatement($"{TestMethodResultVariableName}.ShouldNotBeNull();").WithLeadingTrivia(GetTriviaList(AssertComment)));
                }
                

                var methodSyntax = GetMethodDeclarationSyntax();

                _generator.MethodNodes.Add(methodSyntax);
            }
        }

        private MethodDeclarationSyntax GetMethodDeclarationSyntax()
        {
            var methodDeclaration = _generator.InternalGenerator.MethodDeclaration(
                _testMethodName,
                new List<SyntaxNode>(),
                null,
                null,
                Accessibility.Public,
                DeclarationModifiers.None,
                _methodStatementsList) as MethodDeclarationSyntax;

            var attributeList = _generator.TestFramework.GetTestMethodAttributeList();
            var attributeSingletonList = SingletonList(attributeList);

            var methodSyntax = methodDeclaration?.WithAttributeLists(attributeSingletonList);

            return methodSyntax;
        }

    }
}
