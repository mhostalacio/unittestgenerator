﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
using static UTGeneratorCore.Factory.CSharp.CSharpHelper;

namespace UTGeneratorCore.Factory.CSharp.Patterns
{
    public class EmptyMethods<T, M>
        where T : TestFramework
        where M : MockFramework
    {
        private readonly MethodMapper _method;
        private readonly CSharpTestGenerator<T, M> _generator;
        private readonly List<SyntaxNode> _methodStatementsList = new List<SyntaxNode>();

        public EmptyMethods(MethodMapper method, CSharpTestGenerator<T, M> generator)
        {
            _method = method;
            _generator = generator;
        }

        public void GenerateTestCase()
        {
            // Arrange Statements
            var arranged = ProcessArranges();

            // Act Statements
            _methodStatementsList.AddRange(CreateActStatementsWithAction(_method, arranged ? ActComment : ArrangeAndActComment));

            // Assert Statements
            ProcessAssertions();
        }

        private bool ProcessArranges()
        {
            CSharpHelper.ProcessMethodParametersForUnitTestCases(_method, _methodStatementsList, true, null, _generator);

            var arranged = _methodStatementsList.Count > 0;

            if (arranged)
            {
                _methodStatementsList[0] = _methodStatementsList[0].WithLeadingTrivia(GetTriviaList(ArrangeComment));
            }

            return arranged;
        }

        private void ProcessAssertions()
        {
            var testMethodName = string.Format(TestMethodNameEmptyMethod, _method.UniqueName ?? _method.Name);
            _methodStatementsList.Add
            (
                ParseStatement($"{TestMethodActionVariableName}.ShouldNotThrow();").WithLeadingTrivia(GetTriviaList(AssertComment))
            );

            var methodDeclarationSyntax = _generator.InternalGenerator.MethodDeclaration(
                testMethodName,
                null,
                null,
                null,
                Accessibility.Public,
                DeclarationModifiers.None,
                _methodStatementsList) as MethodDeclarationSyntax;

            var methodSyntax = methodDeclarationSyntax?.WithAttributeLists
            (
                SingletonList
                (
                    _generator.TestFramework.GetTestMethodAttributeList()
                )
            );

            _generator.MethodNodes.Add(methodSyntax);
        }

    }
}
