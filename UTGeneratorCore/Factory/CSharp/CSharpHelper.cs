﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Web.UI;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
using UserControl = System.Web.UI.UserControl;

namespace UTGeneratorCore.Factory.CSharp
{
    public static class CSharpHelper
    {
        public const string ActComment = "// Act";
        public const string ArrangeAndActComment = "// Arrange, Act";
        public const string ArrangeActAndAssertComment = "// Arrange, Act, Assert";
        public const string ArrangeComment = "// Arrange";
        public const string AssertComment = "// Assert";
        public const string PrivateObjectMemberName = "_privateObject";
        public const string PrivateTypeMemberName = "_privateType";
        public const string TestAssertAllMethod = "ShouldSatisfyAllConditions";
        public const string TestAssertShouldBe = "ShouldBe";
        public const string TestAssertShouldNotBeNull = "ShouldNotBeNull";
        public const string TestMethodName = "{0}_Condition_Result";
        public const string TestMethodResultVariableName = "_result";
        public const string TestMethodActionVariableName = "_action";
        public const string ParametrizedTestMethodName = "{0}{1}_GivenData_InvokeMethods";
        public const string TestMethodNameThrowsException = "{0}_ForceError_Throws{1}";
        public const string TestMethodNameEmptyMethod = "{0}_OnValidCall_ShouldNotThrowException";
        public const string TestMethodNameDoesNotThrowException = "{0}_ForceError_Catch{1}";
        public const string TestObjectVariableName = "_testObject";
        public const string XmlNodeVariableName = "_dummyXmlNode";
        public const string XmlDocumentVariableName = "_dummyXmlDocument";
        public const string XElementVariableName = "_dummyXElement";
        public const string CurrentCultureVariableName = "_currentCulture";
        public const string DisposableObjectsVariableName = "_disposableObjects";
        public const string MethodInvocationVariableName = "_methodInvocationsList";

        private static readonly Random random = new Random();

        public static IList<MetadataReference> CommonReferences = new List<MetadataReference>
        {
            MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(AppSettingsReader).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(DataColumn).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(XmlAttribute).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Page).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(XNode).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(AuditLevel).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Enumerable).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(ScriptManager).Assembly.Location),
            MetadataReference.CreateFromFile(typeof(Label).Assembly.Location)
        };

        public static int RandomInt() => random.Next(0, 999);

        public static ExpressionStatementSyntax AssertAllStatement(string comment, ArgumentListSyntax asserts)
        {
            return ExpressionStatement
            (
                InvocationExpression
                (
                    MemberAccessExpression
                    (
                        SyntaxKind.SimpleMemberAccessExpression,
                        IdentifierName(Identifier(GetTriviaList(comment), "this", TriviaList())),
                        IdentifierName(TestAssertAllMethod)
                    )
                ).WithArgumentList(asserts)
            );
        }

        private static ArgumentListSyntax BuildArgumentsList(IMapper method)
        {
            return ArgumentList
            (
                SeparatedList
                (
                    method
                        .ParametersTypes
                        .Select
                        (
                            p => Argument
                            (
                                IdentifierName(p.Value.UsedAsTestCaseParameter
                                    ? p.Value.RenamedTo
                                    : p.Key)
                            )
                        )
                )
            );
        }

        private static SeparatedSyntaxList<ExpressionSyntax> BuildParameterList(IMapper methodUnderTest)
        {
            return SeparatedList<ExpressionSyntax>
            (
                methodUnderTest
                    .ParametersTypes
                    .Select
                    (
                        p => IdentifierName(p.Value.UsedAsTestCaseParameter
                            ? p.Value.RenamedTo
                            : p.Key)
                    )
            );
        }

        private static ExpressionStatementSyntax CreateInvokeStatement(
            SyntaxTriviaList leading,
            string testEntity,
            string methodName,
            ArgumentListSyntax methodArguments)
        {
            return ExpressionStatement
            (
                InvocationExpression
                    (
                        MemberAccessExpression
                        (
                            SyntaxKind.SimpleMemberAccessExpression,
                            IdentifierName
                            (
                                Identifier
                                (
                                    leading,
                                    testEntity,
                                    TriviaList()
                                )
                            ),
                            IdentifierName(methodName)
                        )
                    )
                    .WithArgumentList(methodArguments)
            );
        }

        public static IList<SyntaxNode> CreateActStatements(IMapper methodUnderTest, string comment)
        {
            var nodes = new List<SyntaxNode>();
            ExpressionStatementSyntax methodInvoke = CreateMethodInvoke(methodUnderTest, comment);

            if (!methodUnderTest.IsVoid)
            {
                nodes.Add(ProcessMethodResult(comment, methodInvoke));
            }
            else
            {
                nodes.Add(methodInvoke);
            }

            return nodes;
        }

        private static ExpressionStatementSyntax CreateMethodInvoke(IMapper methodUnderTest, string comment)
        {
            ExpressionStatementSyntax methodInvoke;
            var methodArguments = ArgumentList();
            if (methodUnderTest.IsNonPublic || methodUnderTest.Class.IsNestedClass)
            {
                var arguments = new List<ArgumentSyntax>
                {
                    Argument(IdentifierName($"MethodName{methodUnderTest.UniqueName}"))
                };

                if (methodUnderTest.ParametersTypes.Count > 0)
                {
                    var argument = ProcessMethodParameters(methodUnderTest);

                    arguments.Add(argument);
                }

                methodArguments = ArgumentList(SeparatedList(arguments));

                methodInvoke = CreateInvokeStatement(
                    methodUnderTest.IsVoid
                        ? GetTriviaList(comment)
                        : TriviaList(),
                    methodUnderTest.IsStatic
                        ? PrivateTypeMemberName
                        : PrivateObjectMemberName,
                    methodUnderTest.IsStatic
                        ? "InvokeStatic"
                        : "Invoke",
                    methodArguments);
            }
            else
            {
                if (methodUnderTest.ParametersTypes.Count > 0)
                {
                    methodArguments = BuildArgumentsList(methodUnderTest);
                }

                methodInvoke = CreateInvokeStatement(
                    methodUnderTest.IsVoid
                        ? GetTriviaList(comment)
                        : TriviaList(),
                    methodUnderTest.IsStatic
                        ? methodUnderTest.Class.ClassName
                        : TestObjectVariableName,
                    methodUnderTest.Name,
                    methodArguments);
            }

            return methodInvoke;
        }

        public static IList<SyntaxNode> CreateActStatementsWithAction(MethodMapper methodUnderTest, string comment)
        {
            var nodes = new List<SyntaxNode>();
            ExpressionStatementSyntax methodInvoke = CreateMethodInvoke(methodUnderTest, null);
            nodes.Add(ProcessMethodResultWithAction(comment, methodInvoke));
            return nodes;
        }

        private static StatementSyntax ProcessMethodResultWithAction(
            string comment,
            ExpressionStatementSyntax methodInvoke)
        {
            return ExpressionStatement
            (
                AssignmentExpression
                (
                    SyntaxKind.SimpleAssignmentExpression,
                    IdentifierName(TestMethodActionVariableName),
                    ParenthesizedLambdaExpression(methodInvoke.Expression)
                )
            ).WithLeadingTrivia(TriviaList(Comment(comment)));
        }

        private static StatementSyntax ProcessMethodResult(
            string comment,
            ExpressionStatementSyntax methodInvoke)
        {
            return ExpressionStatement
            (
                AssignmentExpression
                (
                    SyntaxKind.SimpleAssignmentExpression,
                    IdentifierName(TestMethodResultVariableName),
                    methodInvoke.Expression
                )
            ).WithLeadingTrivia(TriviaList(Comment(comment)));
        }

        private static ArgumentSyntax ProcessMethodParameters(IMapper methodUnderTest)
        {
            return Argument
            (
                ArrayCreationExpression
                    (
                        ArrayType
                            (
                                PredefinedType
                                (
                                    Token(SyntaxKind.ObjectKeyword)
                                )
                            )
                            .WithRankSpecifiers
                            (
                                SingletonList
                                (
                                    ArrayRankSpecifier
                                        (
                                            SingletonSeparatedList<ExpressionSyntax>
                                            (
                                                OmittedArraySizeExpression()
                                            )
                                        )
                                        .WithCloseBracketToken
                                        (
                                            Token
                                            (
                                                new SyntaxTriviaList(Space),
                                                SyntaxKind.CloseBracketToken,
                                                new SyntaxTriviaList(Space)
                                            )
                                        )
                                )
                            )
                    )
                    .WithNewKeyword
                    (
                        Token(SyntaxKind.NewKeyword)
                    )
                    .WithInitializer
                    (
                        InitializerExpression
                        (
                            SyntaxKind.ArrayInitializerExpression,
                            BuildParameterList(methodUnderTest)
                        )
                    )
            );
        }

        public static void ArrangeParameters(IMapper mappedMethod, List<SyntaxNode> nodesToAdd, ITestGenerator generator)
        {
            if (mappedMethod.ParametersTypes.Count > 0)
            {
                nodesToAdd.AddRange(mappedMethod.ParametersTypes.Select(param => GetArrangeParameter(generator, param)));
            }
        }

        private static LocalDeclarationStatementSyntax GetArrangeParameter(ITestGenerator generator, KeyValuePair<string, ParameterSymbol> param)
        {
            var variableType = "var";
            var shouldBeConstant = VariableShouldBeDeclaredAsConstant(param.Value.Symbol)
                                   && !param.Value.IsOutParameter
                                   && !param.Value.IsRefParameter;

            if (shouldBeConstant)
            {
                variableType = param.Value.Name;
            }

            var variableForParam = LocalDeclarationStatement
            (
                VariableDeclaration
                    (
                        IdentifierName
                        (
                            Identifier
                            (
                                TriviaList(),
                                variableType,
                                TriviaList()
                            )
                        )
                    )
                    .WithVariables
                    (
                        SingletonSeparatedList
                        (
                            VariableDeclarator
                                (
                                    Identifier(param.Key)
                                )
                                .WithInitializer
                                (
                                    EqualsValueClause
                                    (
                                        GetArgumentValue
                                        (
                                            param,
                                            true,
                                            false,
                                            generator
                                        )
                                    )
                                )
                        )
                    )
            );

            if (shouldBeConstant)
            {
                variableForParam = variableForParam
                    .WithModifiers
                    (
                        TokenList
                        (
                            Token
                            (
                                TriviaList(),
                                SyntaxKind.ConstKeyword,
                                TriviaList(Space)
                            )
                        )
                    );
            }

            return variableForParam;
        }

        public static ExpressionStatementSyntax CreateAssertParameter(MockMethodMapper methodMock, string variableName, bool addTrivia, bool avoidShouldBeNullParameterAssertion)
        {
            var triviaList = addTrivia
                ? GetTriviaListForAssert()
                : TriviaList();

            //Check if there are hard coded values to assert
            var valuesUsed = methodMock.UsedParameterValues.Where(p => p.ContainsKey(variableName)).Select(o => o[variableName].ToString()).Distinct().ToList();

            //TODO: it's being done for string type only, need to be implemented for all struct types

            if (valuesUsed.Count != 1 || !valuesUsed[0].StartsWith("\"") || !valuesUsed[0].EndsWith("\""))
            {
                if (avoidShouldBeNullParameterAssertion)
                {
                    return null;
                }

                return ExpressionStatement
                (
                    InvocationExpression
                    (
                        MemberAccessExpression
                        (
                            SyntaxKind.SimpleMemberAccessExpression,
                            IdentifierName(Identifier(triviaList, variableName, TriviaList())),
                            IdentifierName(TestAssertShouldNotBeNull)
                        )
                    )
                );
            }

            var arguments = new List<ArgumentSyntax>
            {
                methodMock.UsedParameterValues[0][variableName]
            };

            var argumentList = ArgumentList(SeparatedList(arguments));

            return ExpressionStatement
            (
                InvocationExpression
                    (
                        MemberAccessExpression
                        (
                            SyntaxKind.SimpleMemberAccessExpression,
                            IdentifierName(Identifier(triviaList, variableName, TriviaList())),
                            IdentifierName(TestAssertShouldBe)
                        )
                    )
                    .WithArgumentList(argumentList)
            );

        }

        //Not able to use ParseStatement for declaring fields -> it returns ExpressionStatement and not able to convert to MemberDeclarationSyntax
        public static FieldDeclarationSyntax CreateField(
            string fieldName,
            string typeName,
            string fieldValue,
            bool isConstant)
        {
            if (isConstant)
            {
                return FieldDeclaration
                    (
                        VariableDeclaration
                            (
                                IdentifierName(typeName)
                            )
                            .WithVariables
                            (
                                SingletonSeparatedList
                                (
                                    VariableDeclarator
                                        (
                                            Identifier(fieldName)
                                        )
                                        .WithInitializer
                                        (
                                            EqualsValueClause
                                            (
                                                LiteralExpression
                                                (
                                                    SyntaxKind.StringLiteralExpression,
                                                    Literal(fieldValue)
                                                )
                                            )
                                        )
                                )
                            )
                    )
                    .WithModifiers
                    (
                        TokenList
                        (
                            Token(SyntaxKind.PrivateKeyword),
                            Token(SyntaxKind.ConstKeyword)
                        )
                    );
            }

            if (fieldValue != null)
            {
                return FieldDeclaration
                    (
                        VariableDeclaration
                            (
                                IdentifierName(typeName)
                            )
                            .WithVariables
                            (
                                SingletonSeparatedList
                                (
                                    VariableDeclarator
                                        (
                                            Identifier(fieldName)
                                        )
                                        .WithInitializer
                                        (
                                            EqualsValueClause
                                            (
                                                LiteralExpression
                                                (
                                                    SyntaxKind.StringLiteralExpression,
                                                    Literal(fieldValue)
                                                )
                                            )
                                        )
                                )
                            )
                    )
                    .WithModifiers
                    (
                        TokenList
                        (
                            Token(SyntaxKind.PrivateKeyword)
                        )
                    );
            }

            return FieldDeclaration
                (
                    VariableDeclaration
                        (
                            IdentifierName(typeName)
                        )
                        .WithVariables
                        (
                            SingletonSeparatedList
                            (
                                VariableDeclarator
                                (
                                    Identifier(fieldName)
                                )
                            )
                        )
                )
                .WithModifiers
                (
                    TokenList
                    (
                        Token(SyntaxKind.PrivateKeyword)
                    )
                );
        }

        public static IList<SyntaxNode> GenerateClassConstructorInvokeStatement(ConstructorMapper constructor, string comment)
        {
            var nodes = new List<SyntaxNode>();
            var classConstructorArguments = ArgumentList();

            if (constructor.ParametersTypes.Count > 0)
            {
                classConstructorArguments = BuildArgumentsList(constructor);
            }

            var classConstructorStatement = ExpressionStatement
            (
                AssignmentExpression
                (
                    SyntaxKind.SimpleAssignmentExpression,
                    IdentifierName(TestObjectVariableName),
                    ObjectCreationExpression
                        (
                            IdentifierName(constructor.Class.ClassName)
                        )
                        .WithArgumentList(classConstructorArguments)
                )
            );

            nodes.Add(classConstructorStatement.WithLeadingTrivia(GetTriviaList(comment)));

            return nodes;
        }

        public static IList<SyntaxNode> GenerateClassConstructorInvokeStatementViaReflection(ClassMapper classUnderTest, ConstructorMapper constructor)
        {
            var list = new List<SyntaxNode>();

            list.Add(ParseStatement($"var internalClass = typeof({classUnderTest.ParentClass.Identifier.ToString()}).Assembly.GetType(\"{classUnderTest.Namespace}+{classUnderTest.ClassName}\");"));
            list.Add(ParseStatement("var internalClassConstructor = internalClass.GetConstructors(BindingFlags.Instance | BindingFlags." + (constructor.IsNonPublic
                                        ? "NonPublic"
                                        : "Public") + ")[0];"));
            list.Add(ParseStatement("var testObject = internalClassConstructor.Invoke(new object[] { });"));

            return list;
        }

        /// <summary>
        /// Returns expression to initialize variables with dummy values based on type
        /// </summary>
        public static ExpressionSyntax GetArgumentValue(KeyValuePair<string, ParameterSymbol> parameter, bool useMocks, bool castValue, ITestGenerator generator)
        {
            //TODO: find better approach for these special cases
            if (parameter.Value.Symbol.Name.Equals("XmlNode"))
            {
                return ParseExpression(XmlNodeVariableName);
            }
            else if (parameter.Value.Symbol.Name.Equals("XmlDocument"))
            {
                return ParseExpression(XmlDocumentVariableName);
            }
            else if (parameter.Value.Symbol.Name.Equals("XElement"))
            {
                return ParseExpression(XElementVariableName);
            }
            else if (parameter.Value.Symbol.Name.Equals("Type"))
            {
                return ParseExpression("typeof(string)");
            }
            else if (parameter.Value.Symbol.IsAbstract)
            {
                if (parameter.Value.Symbol.ToString().Contains(">"))
                {
                    var parts = parameter.Value.Symbol.ToDisplayParts();
                    if (parts.Length > 2)
                    {
                        return ParseExpression($"new List<{parts[parts.Length - 2]}>()");
                    }
                    else
                    {
                        return ParseExpression("new List<object>()");
                    }
                }
                else
                {
                    // use object
                    if (parameter.Value.Symbol.Name.Equals("IList")
                        || parameter.Value.Symbol.Name.Equals("ICollection")
                        || parameter.Value.Symbol.Name.Equals("IEnumerable"))
                    {
                        return ParseExpression("new List<object>()");
                    }
                    else if (parameter.Value.Symbol.Name.Equals("IEnumerator"))
                    {
                        return ParseExpression("new List<object>().GetEnumerator()");
                    }
                    else if (parameter.Value.Symbol.Name.Equals("Stream"))
                    {
                        return ParseExpression("new ShimMemoryStream().Instance");
                    }
                }
            }
            else if (parameter.Value.Symbol.Name.Equals("Color"))
            {
                return ParseExpression("Color.Black");
            }
            else if (parameter.Value.Symbol.Name.Equals("Image"))
            {
                return ParseExpression("new Bitmap(1, 1)");
            }
            else if (parameter.Value.Symbol.Name.Equals("CultureInfo"))
            {
                return ParseExpression("CultureInfo.InvariantCulture");
            }
            

            //TODO: complete this method for all the value types

            if (parameter.Value.Symbol is INamedTypeSymbol symbol)
            {
                switch (symbol.TypeKind)
                {
                    case TypeKind.Struct when symbol.ToString().ToLower().Contains("int")
                                              || symbol.ToString().ToLower().Contains("short")
                                              || symbol.ToString().ToLower().Contains("double")
                                              || symbol.ToString().ToLower().Contains("long")
                                              || symbol.ToString().ToLower().Contains("decimal")
                                              || symbol.ToString().ToLower().Contains("single")
                                              || symbol.ToString().ToLower().Contains("float"):

                        if (!castValue || symbol.ToString().ToLower().Contains("int"))
                        {
                            return LiteralExpression
                            (
                                SyntaxKind.NumericLiteralExpression,
                                Literal
                                (
                                    RandomInt()
                                )
                            );
                        }
                        else
                        {
                            return CastExpression(
                                IdentifierName(symbol.ToString()),
                                LiteralExpression
                                (
                                    SyntaxKind.NumericLiteralExpression,
                                    Literal
                                    (
                                        RandomInt()
                                    )
                                ));
                        }


                    case TypeKind.Struct when symbol.ToString().Contains("DateTime"):

                        return MemberAccessExpression
                        (
                            SyntaxKind.SimpleMemberAccessExpression,
                            IdentifierName("DateTime"),
                            IdentifierName("MinValue")
                        );

                    case TypeKind.Struct when symbol.ToString().Equals("bool", StringComparison.InvariantCultureIgnoreCase) ||
                                              symbol.ToString().Equals("boolean", StringComparison.InvariantCultureIgnoreCase):

                        return LiteralExpression(SyntaxKind.TrueLiteralExpression);

                    case TypeKind.Struct when symbol.ToString().Equals("System.Guid", StringComparison.InvariantCultureIgnoreCase) ||
                                              symbol.ToString().Equals("System.Guid?", StringComparison.InvariantCultureIgnoreCase):
                        return ParseExpression("Guid.NewGuid()");

                    case TypeKind.Struct:

                        break;
                    case TypeKind.Enum:
                        return ParseExpression($"{symbol.Name}.{symbol.MemberNames.FirstOrDefault()}");

                    default:
                        if (symbol.Name.ToString().Equals("string", StringComparison.InvariantCultureIgnoreCase))
                        {
                            return LiteralExpression(SyntaxKind.StringLiteralExpression, Literal(parameter.Key));
                        }

                        if (symbol.ToString().Equals("System.EventArgs", StringComparison.InvariantCultureIgnoreCase))
                        {
                            return ParseExpression("EventArgs.Empty");
                        }

                        if (symbol.ContainingNamespace != null && symbol.ContainingNamespace.ToString().Equals("System.IO"))
                        {
                            //use shim instead of real class
                            return MemberAccessExpression
                            (
                                SyntaxKind.SimpleMemberAccessExpression,
                                ObjectCreationExpression
                                    (
                                        IdentifierName($"Shim{symbol.Name}")
                                    )
                                    .WithArgumentList
                                    (
                                        ArgumentList()
                                    ),
                                IdentifierName("Instance")
                            );
                        }

                        if (symbol.TypeKind == TypeKind.Class && symbol.IsGenericType)
                        {
                            //Handle Lists
                            return ObjectCreationExpression
                                (
                                    IdentifierName
                                    (
                                        ModelsHelper.ResolveTypeName(parameter.Value.Symbol)
                                    )
                                )
                                .WithArgumentList
                                (
                                    ArgumentList()
                                );
                        }

                        break;
                }
            }
            else if (parameter.Value.Symbol is ITypeSymbol typeSymbol && typeSymbol.TypeKind == TypeKind.Array)
            {
                var array = typeSymbol as IArrayTypeSymbol;

                var dummyArrayValues = new SeparatedSyntaxList<ExpressionSyntax>();
                return ArrayCreationExpression
                    (
                        ArrayType
                            (
                                IdentifierName(ModelsHelper.ResolveTypeName(array.ElementType))
                            )
                            .WithRankSpecifiers
                            (
                                SingletonList
                                (
                                    ArrayRankSpecifier
                                        (
                                            SingletonSeparatedList<ExpressionSyntax>
                                            (
                                                OmittedArraySizeExpression()
                                            )
                                        )
                                        .WithCloseBracketToken
                                        (
                                            Token
                                            (
                                                new SyntaxTriviaList(Space),
                                                SyntaxKind.CloseBracketToken,
                                                new SyntaxTriviaList(Space)
                                            )
                                        )
                                )
                            )
                    )
                    .WithNewKeyword
                    (
                        Token(SyntaxKind.NewKeyword)
                    )
                    .WithInitializer
                    (
                        InitializerExpression
                        (
                            SyntaxKind.ArrayInitializerExpression,
                            dummyArrayValues
                        )
                    );
            }
            else if (parameter.Value.Symbol is IDynamicTypeSymbol)
            {
                //TODO: improve for dynamic objects
                return ObjectCreationExpression
                    (
                        IdentifierName("object")
                    )
                    .WithArgumentList
                    (
                        ArgumentList()
                    );
            }

            var mockType = useMocks
                           && !ModelsHelper.ClassesToNotShim.Contains(parameter.Value.Symbol.Name)
                           && !string.IsNullOrEmpty(parameter.Value.Symbol.Name);
            var mockClassName = parameter.Value.Symbol.Name;
            var memberAccessEpr = "";
            if (mockType && parameter.Value.Symbol is INamedTypeSymbol namedTypeSymbol)
            {
                var classPrefix = namedTypeSymbol.IsAbstract
                    ? "Stub"
                    : "Shim";
                memberAccessEpr = namedTypeSymbol.IsAbstract
                    ? ""
                    : "Instance";
                mockClassName = $"{classPrefix}{namedTypeSymbol.Name}";
                if (namedTypeSymbol.Name == "Nullable")
                {
                    mockClassName = $"{classPrefix}{namedTypeSymbol.ToDisplayString()}";
                }

                // Add using nodes
                if (namedTypeSymbol.Kind != SymbolKind.ErrorType && namedTypeSymbol.ToString().IndexOf(".") > -1) // it happens when Roslyn cannot resolve a type
                {
                    var nameSpace = namedTypeSymbol.ToString().Substring(0, namedTypeSymbol.ToString().LastIndexOf("."));
                    mockClassName = generator.AddUsingNodeIfNew(UsingDirective(ParseName($"{nameSpace}.Fakes")), mockClassName);
                }
            }

            ExpressionSyntax obj = ObjectCreationExpression
                (
                    IdentifierName
                    (
                        mockType
                            ? mockClassName
                            : parameter.Value.Symbol.ToDisplayString()
                    )
                )
                .WithArgumentList
                (
                    ArgumentList()
                );
            if (mockType && !string.IsNullOrEmpty(memberAccessEpr))
            {
                obj = MemberAccessExpression(SyntaxKind.SimpleMemberAccessExpression, obj, IdentifierName(memberAccessEpr));
            }

            return obj;
        }

        /// <summary>
        /// Returns expression list to initialize variables with dummy values based on type
        /// </summary>
        public static List<ExpressionSyntax> GetArgumentValueWithVariants(KeyValuePair<string, ParameterSymbol> parameter, ITestGenerator generator)
        {
            var list = new List<ExpressionSyntax>();
            list.Add(GetArgumentValue(parameter, true, true, generator));
            if (parameter.Value.Symbol is INamedTypeSymbol symbol)
            {
                switch (symbol.TypeKind)
                {
                    case TypeKind.Struct when symbol.ToString().Equals("bool", StringComparison.InvariantCultureIgnoreCase) ||
                                              symbol.ToString().Equals("boolean", StringComparison.InvariantCultureIgnoreCase):
                        list.Add(LiteralExpression(SyntaxKind.FalseLiteralExpression));
                        break;
                    case TypeKind.Enum:
                        foreach (var member in symbol.MemberNames)
                        {
                            if (!member.ToString().Equals(".ctor"))
                                list.Add(ParseExpression($"{symbol.Name}.{member}"));
                        }

                        break;
                    default:
                        //if (symbol.ToString().Equals("string", StringComparison.InvariantCultureIgnoreCase))
                        //{
                        //    list.Add(ParseExpression("null"));
                        //}
                        break;
                }
            }

            return list;
        }

        public static SyntaxTriviaList GetTriviaList(string comment)
        {
            return string.IsNullOrEmpty(comment)
                ? TriviaList()
                : TriviaList(Comment(comment));
        }

        public static SyntaxTriviaList GetTriviaListForAssert()
        {
            return TriviaList
            (
                Trivia
                (
                    SkippedTokensTrivia()
                        .WithTokens
                        (
                            TokenList
                            (
                                Token(SyntaxKind.OpenParenToken),
                                Token(SyntaxKind.CloseParenToken),
                                Token(SyntaxKind.EqualsGreaterThanToken)
                            )
                        )
                )
            );
        }

        /// <summary>
        /// Returns if the local variable should be constant according to its type
        /// </summary>
        public static bool VariableShouldBeDeclaredAsConstant(ISymbol type)
        {
            if (type.ToString().Equals("System.Guid", StringComparison.InvariantCultureIgnoreCase))
            {
                return false;
            }

            if (type.Name.Equals("Nullable", StringComparison.InvariantCultureIgnoreCase))
            {
                return false;
            }

            if (type.ToString().Equals("System.DateTime", StringComparison.InvariantCultureIgnoreCase))
            {
                return false;
            }

            if (type.ToString().Equals("System.TimeSpan", StringComparison.InvariantCultureIgnoreCase))
            {
                return false;
            }

            if (type.ToString().Equals("string", StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            return type is ITypeSymbol typeSymbol && typeSymbol.TypeKind == TypeKind.Struct;
        }

        public static void ProcessMethodParametersForUnitTestCases(IMapper method,
            List<SyntaxNode> methodStatementsList,
            bool avoidUsingTestCaseParameters,
            IList<string> parametersToAvoid,
            ITestGenerator generator)
        {
            if (method.ParametersTypes.Count <= 0)
            {
                return;
            }

            foreach (var parameter in method.ParametersTypes)
            {
                if (parameter.Value.UsedAsTestCaseParameter && !avoidUsingTestCaseParameters)
                {
                    continue;
                }

                if (parametersToAvoid != null && parametersToAvoid.Contains(parameter.Key))
                {
                    continue;
                }

                var variableType = "var";
                var shouldBeConstant = VariableShouldBeDeclaredAsConstant(parameter.Value.Symbol)
                                       && !parameter.Value.IsOutParameter
                                       && !parameter.Value.IsRefParameter;

                if (shouldBeConstant)
                {
                    variableType = parameter.Value.Symbol.ToString();
                }

                var variableForParam = LocalDeclarationStatement
                (
                    VariableDeclaration
                        (
                            IdentifierName
                            (
                                Identifier(variableType)
                            )
                        )
                        .WithVariables
                        (
                            SingletonSeparatedList
                            (
                                VariableDeclarator
                                    (
                                        Identifier(parameter.Key)
                                    )
                                    .WithInitializer
                                    (
                                        EqualsValueClause
                                        (
                                            GetArgumentValue
                                            (
                                                parameter,
                                                true,
                                                false,
                                                generator
                                            )
                                        )
                                    )
                            )
                        )
                );

                if (shouldBeConstant)
                {
                    variableForParam =
                        variableForParam.WithModifiers
                        (
                            TokenList
                            (
                                Token(SyntaxKind.ConstKeyword)
                            )
                        );
                }

                ITypeSymbol classThatImplements;
                methodStatementsList.Add(variableForParam);
                if (parameter.Value.Symbol is ITypeSymbol typeSymbol && ModelsHelper.ImplementsInterfaceOrBaseClass(typeSymbol, typeof(IDisposable), out classThatImplements)
                                                                     && ModelsHelper.ClassesToNotShim.Contains(typeSymbol.Name))
                {
                    methodStatementsList.Add(ParseStatement($"{DisposableObjectsVariableName}.Add({parameter.Key});"));
                }

            }
        }

        public static string GetFieldAccessCode(FieldMapper field, ClassMapper clazz)
        {
            if (field.IsStatic || field.IsConstant)
            {
                if (field.IsPublic)
                {
                    return $"{clazz.ClassName}.{field.Name}";
                }
                else
                {
                    return $"{PrivateTypeMemberName}.GetStaticField(\"{field.Name}\")";
                }
            }
            else
            {
                if (field.IsPublic)
                {
                    return $"{TestObjectVariableName}.{field.Name}";
                }
                else
                {
                    return $"{PrivateObjectMemberName}.GetField(\"{field.Name}\")";
                }
            }

            return string.Empty;
        }

        internal static void HandlePropertyGetterAndSetter<T, M>(PropertyMapper property,
            List<string> usedVariableNames,
            CSharpTestGenerator<T, M> generator,
            List<SyntaxNode> methodStatementsList,
            List<ArgumentSyntax> assertions)
            where T : TestFramework
            where M : MockFramework
        {
            bool found = false;
            var externalRefs = new Dictionary<SyntaxNode, MockMethodMapper>();
            foreach (var accessor in property.Declaration.AccessorList.Accessors)
            {
                if (accessor.Body != null && accessor.Body.DescendantNodes().Any())
                {
                    foreach (var node in accessor.Body.DescendantNodes())
                    {
                        ModelsHelper.ExtractExternalReferences(node, property.Owner, externalRefs, true, null);
                    }

                    if (externalRefs.Count > 0)
                    {
                        found = true;
                        foreach (var externalRef in externalRefs)
                        {
                            if (!usedVariableNames.Contains(externalRef.Value.MethodInvokeCountVariableName))
                            {
                                usedVariableNames.Add(externalRef.Value.MethodInvokeCountVariableName);
                                var syntaxNodes = generator.MockFramework.CreateMethodMock(
                                    externalRef.Key,
                                    externalRef.Value,
                                    property.Owner.SemanticModel,
                                    generator,
                                    true);

                                methodStatementsList.AddRange(syntaxNodes);
                                foreach (var assertion in externalRef.Value.AssertionsToBeDone)
                                {
                                    assertions.Add(Argument(assertion.Expression));
                                }
                            }
                        }
                    }
                }
            }

            //if (found)
            //{
            //    assertions.Add
            //    (
            //        Argument
            //        (
            //            ParseExpression($"() => {MethodInvocationVariableName}.{TestAssertShouldBe}(new List<IStructuralEquatable>())")
            //        )
            //    );
            //}
        }


        public static string AddUsingNodesForType(ITypeSymbol type, ITestGenerator generator)
        {
            string result = null;
            if (type is INamedTypeSymbol namedType && namedType.IsGenericType)
            {
                result = "";
                foreach (var typeParam in namedType.TypeParameters)
                {
                    if (!string.IsNullOrWhiteSpace(result))
                    {
                        result += ", ";
                    }

                    result += AddUsingNodesForTypeInternal(typeParam, generator);
                }

                result = $"{type.Name}<{result}>";
            }
            else
            {
                result = AddUsingNodesForTypeInternal(type, generator);
            }

            return result;
        }

        private static string AddUsingNodesForTypeInternal(ITypeSymbol type, ITestGenerator generator)
        {
            var typeName = ModelsHelper.ResolveTypeName(type);
            if (type.TypeKind != TypeKind.Struct
                && type.ToString().IndexOf(".") > -1)
            {
                var nameSpace = type.ToString().Substring(0, type.ToString().LastIndexOf("."));
                typeName = type.ToString().Substring(type.ToString().LastIndexOf(".") + 1);
                if (!typeName.Contains("[")) // avoid creating alias for arrays
                {
                    typeName = generator.AddUsingNodeIfNew(UsingDirective(ParseName($"{nameSpace}")), typeName);
                }
            }

            return typeName;
        }

        internal static SyntaxNode DeclareTestCaseSourcePropertyWithVariants(string testDataSourceName, List<List<ExpressionSyntax>> testDataSourceValues)
        {
            var tokens = new List<List<SyntaxNodeOrToken>>();
            //handle test data source variants
            int total = 1;
            foreach (var testDataSourceValue in testDataSourceValues)
            {
                total = total * testDataSourceValue.Count;
                //TODO: verify
                if (total > 10)
                    break;
            }

            for (var index = 0; index < total; index++)
            {
                tokens.Add(new List<SyntaxNodeOrToken>());
            }

            for (var position = 0; position < testDataSourceValues.Count; position++)
            {
                var variantCount = testDataSourceValues[position].Count;
                var variantIdx = 0;
                foreach (var testValueList in tokens)
                {
                    testValueList.Add(Argument(testDataSourceValues[position][variantIdx]));
                    variantIdx++;
                    if (variantIdx >= variantCount)
                    {
                        variantIdx = 0;
                    }

                    if (position != testDataSourceValues.Count - 1)
                    {
                        testValueList.Add(Token(SyntaxKind.CommaToken));
                    }
                }
            }

            return GetTestDatasourceEspression(testDataSourceName, tokens);
        }

        private static SyntaxNode GetTestDatasourceEspression(string testDataSourceName, List<List<SyntaxNodeOrToken>> tokens)
        {
            var expressionList = new List<ExpressionSyntax>();
            foreach (var testValueList in tokens)
            {
                expressionList.Add(ObjectCreationExpression
                    (
                        IdentifierName("TestCaseData")
                    )
                    .WithArgumentList
                    (
                        ArgumentList
                        (
                            SeparatedList<ArgumentSyntax>
                            (
                                testValueList
                            )
                        )
                    ));
            }

            return MethodDeclaration(
                    GenericName(
                            Identifier("IEnumerable"))
                        .WithTypeArgumentList(
                            TypeArgumentList(
                                SingletonSeparatedList<TypeSyntax>(
                                    IdentifierName("TestCaseData")))),
                    Identifier(testDataSourceName))
                .WithModifiers(
                    TokenList(
                        new[]
                        {
                            Token(SyntaxKind.PrivateKeyword),
                            Token(SyntaxKind.StaticKeyword)
                        }))
                .WithBody(
                    Block(
                        SingletonList<StatementSyntax>(
                            UsingStatement(
                                Block(
                                    SingletonList<StatementSyntax>(
                                        ReturnStatement(
                                            ImplicitArrayCreationExpression(
                                                InitializerExpression(
                                                    SyntaxKind.ArrayInitializerExpression,
                                                    SeparatedList<ExpressionSyntax>(expressionList)))))))
                            .WithExpression(
                                InvocationExpression(
                                    MemberAccessExpression(
                                        SyntaxKind.SimpleMemberAccessExpression,
                                        IdentifierName("ShimsContext"),
                                        IdentifierName("Create"))))))
                );
        }

        internal static SyntaxNode DeclareTestCaseSourcePropertyWithoutVariants(string testDataSourceName, List<List<ExpressionSyntax>> testDataSourceValues)
        {
            var tokens = new List<List<SyntaxNodeOrToken>>();
            if (testDataSourceValues.Count > 0)
            {
                var size = testDataSourceValues[0].Count;
                for (var i = 0; i < size; i++)
                {
                    tokens.Add(new List<SyntaxNodeOrToken>());
                    for (var j = 0; j < testDataSourceValues.Count; j++)
                    {
                        if (j > 0)
                            tokens[i].Add(Token(SyntaxKind.CommaToken));
                        tokens[i].Add(Argument(testDataSourceValues[j][i]));
                    }
                }
            }
            return GetTestDatasourceEspression(testDataSourceName, tokens);
        }

        internal static void FixStubNodesInTestMethods(List<SyntaxNode> _methodStatementsList)
        {
            // try to fix stub declaration nodes
            var stubStatements = _methodStatementsList.FindAll(o => o.ToString().Contains("Stub")
                                                                    && o is ObjectCreationExpressionSyntax objExpr
                                                                    && objExpr.Initializer != null && objExpr.Type is IdentifierNameSyntax);
            if (stubStatements.Count > 0)
            {
                var map = new Dictionary<string, SyntaxNode>();
                _methodStatementsList.RemoveAll(o => stubStatements.Contains(o));
                var groups = stubStatements.GroupBy(o => ((IdentifierNameSyntax) ((ObjectCreationExpressionSyntax) o).Type).Identifier.Text);
                foreach (var group in groups)
                {
                    var expressionList = @group.SelectMany(o => ((ObjectCreationExpressionSyntax) o).Initializer.Expressions).ToList();
                    var newNode = (ObjectCreationExpressionSyntax) @group.First();
                    newNode = newNode.WithInitializer(newNode.Initializer.WithExpressions(SeparatedList<ExpressionSyntax>(expressionList)));
                    _methodStatementsList.Add(newNode);
                    map.Add(group.Key, newNode);
                }

                // CASE 1: try to find local variables to assign the expression
                stubStatements = _methodStatementsList.FindAll(o => o.ToString().Contains("Stub")
                                                                    && o is LocalDeclarationStatementSyntax localExp && localExp.Declaration.Variables.Count == 1
                                                                    && localExp.Declaration.Variables[0].Initializer != null
                                                                    && localExp.Declaration.Variables[0].Initializer.Value is ObjectCreationExpressionSyntax objExpr
                                                                    && objExpr.Type is IdentifierNameSyntax);
                if (stubStatements.Count > 0)
                {
                    var localVariableGroups = stubStatements.GroupBy(o =>
                        ((IdentifierNameSyntax) ((ObjectCreationExpressionSyntax) ((LocalDeclarationStatementSyntax) o).Declaration.Variables[0].Initializer.Value).Type).Identifier.Text);
                    foreach (var group in localVariableGroups)
                    {
                        if (map.ContainsKey(group.Key))
                        {

                            var newNode = (LocalDeclarationStatementSyntax) @group.First();
                            //UPDATE NODE WITH STUB EXPRESSION
                            newNode = newNode.WithDeclaration(newNode.Declaration
                                .WithVariables(SeparatedList<VariableDeclaratorSyntax>(new List<VariableDeclaratorSyntax>
                                {
                                    newNode.Declaration.Variables[0].WithInitializer
                                    (
                                        EqualsValueClause
                                        (
                                            (ExpressionSyntax) map[group.Key]
                                        )
                                    )
                                })));
                            _methodStatementsList.Insert(_methodStatementsList.IndexOf(group.First()), newNode);
                            _methodStatementsList.Remove(group.First());
                            if (_methodStatementsList.Contains(map[group.Key]))
                                _methodStatementsList.Remove(map[group.Key]);
                        }
                    }
                }

                // CASE 2: try to find expressions that sets private fields or properties with stubs
                stubStatements = _methodStatementsList.FindAll(o => o.ToString().Contains("Stub")
                                                                    && o is ExpressionStatementSyntax exp && exp.Expression is InvocationExpressionSyntax invExp
                                                                    && invExp.ArgumentList != null && invExp.ArgumentList.Arguments != null  && invExp.ArgumentList.Arguments.Count == 2
                                                                    && invExp.ArgumentList.Arguments[1].Expression is ConditionalExpressionSyntax condExp
                                                                    && condExp.WhenFalse is ObjectCreationExpressionSyntax objExpr
                                                                    && objExpr.Type is IdentifierNameSyntax);
                if (stubStatements.Count > 0)
                {
                    var setFieldGroups = stubStatements.GroupBy(o =>
                        ((IdentifierNameSyntax)
                            ((ObjectCreationExpressionSyntax)
                                ((ConditionalExpressionSyntax)
                                    ((InvocationExpressionSyntax)
                                        ((ExpressionStatementSyntax) o).Expression).ArgumentList.Arguments[1].Expression).WhenFalse).Type).Identifier.Text);
                    foreach (var group in setFieldGroups)
                    {
                        if (map.ContainsKey(group.Key))
                        {
                            //UPDATE NODE WITH STUB EXPRESSION
                            ExpressionStatementSyntax newNode = (ExpressionStatementSyntax) @group.First();
                            var conditional = (ConditionalExpressionSyntax)
                                ((InvocationExpressionSyntax) newNode.Expression).ArgumentList.Arguments[1].Expression;
                            var secondArg = ((InvocationExpressionSyntax) newNode.Expression).ArgumentList.Arguments[1].WithExpression
                            (
                                conditional.WithWhenFalse((ExpressionSyntax) map[group.Key])
                            );
                            var arguments = new List<ArgumentSyntax>
                            {
                                ((InvocationExpressionSyntax) newNode.Expression).ArgumentList.Arguments[0],
                                secondArg
                            };
                            InvocationExpressionSyntax invExp = ((InvocationExpressionSyntax) newNode.Expression).WithArgumentList(ArgumentList(SeparatedList(arguments)));
                            newNode = newNode.WithExpression(invExp);
                            _methodStatementsList.Insert(_methodStatementsList.IndexOf(group.First()), newNode);
                            _methodStatementsList.Remove(group.First());
                            if (_methodStatementsList.Contains(map[group.Key]))
                                _methodStatementsList.Remove(map[group.Key]);

                        }
                    }
                }

                // CASE 3: try to find expressions that returns stubs
                stubStatements = _methodStatementsList.FindAll(o => o.ToString().Contains("Stub")
                                                                    && o is ExpressionStatementSyntax exp && exp.Expression is AssignmentExpressionSyntax assExp
                                                                    && assExp.Right is ParenthesizedLambdaExpressionSyntax parentExp
                                                                    && ((BlockSyntax) parentExp.Body).Statements.FirstOrDefault(p => p is ReturnStatementSyntax retStat
                                                                    && retStat.Expression is ObjectCreationExpressionSyntax objExpr
                                                                    && objExpr.Type is IdentifierNameSyntax) != null);
                if (stubStatements.Count > 0)
                {
                    var setFieldGroups = stubStatements.GroupBy(o =>
                        ((IdentifierNameSyntax)
                            ((ObjectCreationExpressionSyntax)
                              ((ReturnStatementSyntax)
                                ((BlockSyntax)
                                    ((ParenthesizedLambdaExpressionSyntax)
                                        ((AssignmentExpressionSyntax)
                                            ((ExpressionStatementSyntax)o).Expression).Right).Body).Statements.First(p => p is ReturnStatementSyntax)).Expression).Type).Identifier.Text);


                    foreach (var group in setFieldGroups)
                    {
                        if (map.ContainsKey(group.Key))
                        {
                            //UPDATE NODE WITH STUB EXPRESSION
                            ExpressionStatementSyntax newNode = (ExpressionStatementSyntax)@group.First();
                            var originalStatements = ((BlockSyntax)
                                                        ((ParenthesizedLambdaExpressionSyntax)
                                                            ((AssignmentExpressionSyntax)
                                                                newNode.Expression).Right).Body).Statements;
                            var statements = new List<StatementSyntax>();
                            foreach (var stat in originalStatements)
                            {
                                if (stat is ReturnStatementSyntax retStat
                                    && retStat.Expression is ObjectCreationExpressionSyntax objExpr
                                    && objExpr.Type is IdentifierNameSyntax)
                                {
                                    statements.Add(ReturnStatement
                                    (
                                        (ObjectCreationExpressionSyntax)map[group.Key]
                                    ));
                                }
                                else
                                {
                                    statements.Add(stat);
                                }
                            }
                            newNode = newNode.WithExpression(((AssignmentExpressionSyntax) newNode.Expression)
                                .WithRight(((ParenthesizedLambdaExpressionSyntax)((AssignmentExpressionSyntax)newNode.Expression).Right)
                                    .WithBody(((BlockSyntax)((ParenthesizedLambdaExpressionSyntax)((AssignmentExpressionSyntax)newNode.Expression).Right).Body)
                                        .WithStatements(new SyntaxList<StatementSyntax>(statements)))));
                            _methodStatementsList.Insert(_methodStatementsList.IndexOf(group.First()), newNode);
                            _methodStatementsList.Remove(group.First());
                            if (_methodStatementsList.Contains(map[group.Key]))
                                _methodStatementsList.Remove(map[group.Key]);

                        }
                    }
                }
            }
        }
    }
}
