﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using EnvDTE100;
using EnvDTE80;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;
using UTGeneratorCore.Factory.CSharp.Patterns;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
using static UTGeneratorCore.Factory.CSharp.CSharpHelper;
using SyntaxNode = Microsoft.CodeAnalysis.SyntaxNode;

namespace UTGeneratorCore.Factory.CSharp
{
    public class CSharpTestGenerator<T, M> : TestGenerator<T, M>
        where T : TestFramework
        where M : MockFramework
    {
        public SyntaxGenerator InternalGenerator { get; }

        public CSharpTestGenerator(string filePath, T testFramework, M mockFramework, IList<string> trustedAssembliesPaths) : base(filePath, testFramework, mockFramework, trustedAssembliesPaths)
        {
            MappedModel = new Dictionary<ClassMapper, SemanticModel>();

            var tree = CSharpSyntaxTree.ParseText(SourceCode);
            var root = tree.GetCompilationUnitRoot();

            // Getting the semantic model 
            var allTrees = new List<SyntaxTree>
            {
                tree
            };

            var references = new List<MetadataReference>();

            if (trustedAssembliesPaths == null || trustedAssembliesPaths.Count == 0)
            {
                references.AddRange(CommonReferences); // needed when running this project directly. We are able to get these dll's from VS extension
            }

            if (!string.IsNullOrEmpty(filePath))
            {
                var directory = new DirectoryInfo(filePath).Parent;

                while (directory != null)
                {
                    foreach (var searchDir in directory.GetDirectories())
                    {
                        if (!searchDir.Name.Equals("bin", StringComparison.InvariantCultureIgnoreCase))
                        {
                            continue;
                        }

                        foreach (var file in searchDir.GetFiles())
                        {
                            if (file.Extension.Equals(".dll") || file.Extension.Equals(".exe"))
                            {
                                references.Add(MetadataReference.CreateFromFile(file.FullName));
                            }
                        }

                        foreach (var childDir in searchDir.GetDirectories())
                        {
                            if (!childDir.Name.Equals("debug", StringComparison.InvariantCultureIgnoreCase))
                            {
                                continue;
                            }

                            foreach (var file in childDir.GetFiles())
                            {
                                if (file.Extension.Equals(".dll") || file.Extension.Equals(".exe"))
                                {
                                    references.Add(MetadataReference.CreateFromFile(file.FullName));
                                }
                            }

                            break;
                        }

                        // get all .cs files to build trees
                        var cSharpFiles = directory
                            .GetFiles("*.cs*", SearchOption.AllDirectories)
                            .Select(fi => CSharpSyntaxTree.ParseText(File.ReadAllText(fi.FullName)));

                        allTrees.AddRange(cSharpFiles);

                        break;
                    }

                    if (directory.GetFiles("*.csproj", SearchOption.TopDirectoryOnly).Any())
                    {
                        break;
                    }

                    directory = directory.Parent;
                }
            }

            if (trustedAssembliesPaths != null && trustedAssembliesPaths.Count > 0)
            {
                foreach (var trustedAssembly in trustedAssembliesPaths)
                {
                    if (!String.IsNullOrWhiteSpace(trustedAssembly))
                    {
                        var trustedAssemblyRef = MetadataReference.CreateFromFile(trustedAssembly);
                        if (!references.Contains(trustedAssemblyRef))
                        {
                            references.Add(trustedAssemblyRef);
                        }
                    } 
                }
            }

            var compilation = CSharpCompilation.Create("HelloWorld").AddReferences(references).AddSyntaxTrees(allTrees);

            var model = compilation.GetSemanticModel(tree);

            var classNodes = new List<SyntaxNode>(ModelsHelper.GetClassSyntaxNodesFromCompilationUnit(root.SyntaxTree.GetCompilationUnitRoot()));

            if (classNodes.Count > 0)
            {
                foreach (var classNode in classNodes)
                {
                    var classMapper = new ClassMapper(classNode as ClassDeclarationSyntax, model, compilation, filePath);

                    MappedModel.Add(classMapper, model);
                }
            }

            InternalGenerator = SyntaxGenerator.GetGenerator(new AdhocWorkspace(), "C#");
        }

        public CSharpTestGenerator(T testFramework) : base(testFramework)
        {

        }

        protected override void GenerateTestClassFields(ClassMapper classUnderTest)
        {
            var addedFields = new List<string>();
            foreach (var method in classUnderTest.Methods)
            {
                var fieldName = $"MethodName{method.UniqueName}";
                if ((method.IsNonPublic || method.Class.IsNestedClass) && !addedFields.Contains(fieldName))
                {
                    addedFields.Add(fieldName);
                    FieldNodes.Add(CreateField(fieldName, "string", method.Name, true));
                }
            }
            FieldNodes.AddRange(MockFramework.GenerateTestClassFields(classUnderTest));
            FieldNodes.Add(CreateField(PrivateTypeMemberName, "PrivateType", null, false));
            FieldNodes.Add(CreateField(CurrentCultureVariableName, "CultureInfo", null, false));
            if (!classUnderTest.IsStaticClass)
            {
                FieldNodes.Add(CreateField(PrivateObjectMemberName, "PrivateObject", null, false));
                FieldNodes.Add(CreateField(TestObjectVariableName, classUnderTest.ClassName, null, false));
            }
            FieldNodes.Add(CreateField(DisposableObjectsVariableName, "List<IDisposable>", null, false));
            FieldNodes.Add(CreateField(MethodInvocationVariableName, "IList<IStructuralEquatable>", null, false));
            FieldNodes.Add(CreateField(TestMethodResultVariableName, "object", null, false));
            FieldNodes.Add(CreateField(TestMethodActionVariableName, "Action", null, false));
            if (classUnderTest.Fields.Count > 0)
            {
                var controls = classUnderTest.Fields.Where(o => o.ExtendsControl).ToList();
                if (controls.Count > 0)
                {
                    foreach (var control in controls)
                    {
                        FieldNodes.Add(CreateField(ModelsHelper.ConvertToCamelCase(control.Name, false), control.FieldSymbol.Type.Name, null, false));
                    }
                }
            }

            //handle special cases: XmlNode
            if (classUnderTest.ContainsXmlNode)
            {
                FieldNodes.Add(FieldDeclaration
                    (
                        VariableDeclaration
                            (
                                IdentifierName("XmlNode")
                            )
                            .WithVariables
                            (
                                SingletonSeparatedList
                                (
                                    VariableDeclarator
                                        (
                                            Identifier(XmlNodeVariableName)
                                        )
                                        .WithInitializer
                                        (
                                            EqualsValueClause
                                            (
                                                ParseExpression("CreateDummyXmlNode()")
                                            )
                                        )
                                )
                            )
                    )
                    .WithModifiers
                    (
                        TokenList
                        (
                            Token(SyntaxKind.PrivateKeyword),
                            Token(SyntaxKind.StaticKeyword),
                            Token(SyntaxKind.ReadOnlyKeyword)
                        )
                    ));
            }
            //handle special cases: XmlDocument
            if (classUnderTest.ContainsXmlDocument)
            {
                FieldNodes.Add(FieldDeclaration
                    (
                        VariableDeclaration
                            (
                                IdentifierName("XmlDocument")
                            )
                            .WithVariables
                            (
                                SingletonSeparatedList
                                (
                                    VariableDeclarator
                                        (
                                            Identifier(XmlDocumentVariableName)
                                        )
                                        .WithInitializer
                                        (
                                            EqualsValueClause
                                            (
                                                ParseExpression("CreateDummyXmlDocument()")
                                            )
                                        )
                                )
                            )
                    )
                    .WithModifiers
                    (
                        TokenList
                        (
                            Token(SyntaxKind.PrivateKeyword),
                            Token(SyntaxKind.StaticKeyword),
                            Token(SyntaxKind.ReadOnlyKeyword)
                        )
                    ));
            }
            //handle special cases: XElement
            if (classUnderTest.ContainsXElement)
            {
                FieldNodes.Add(FieldDeclaration
                    (
                        VariableDeclaration
                            (
                                IdentifierName("XElement")
                            )
                            .WithVariables
                            (
                                SingletonSeparatedList
                                (
                                    VariableDeclarator
                                        (
                                            Identifier(XElementVariableName)
                                        )
                                        .WithInitializer
                                        (
                                            EqualsValueClause
                                            (
                                                ParseExpression("CreateDummyXElement()")
                                            )
                                        )
                                )
                            )
                    )
                    .WithModifiers
                    (
                        TokenList
                        (
                            Token(SyntaxKind.PrivateKeyword),
                            Token(SyntaxKind.StaticKeyword),
                            Token(SyntaxKind.ReadOnlyKeyword)
                        )
                    ));
            }
        }

        protected override void GenerateTestClassProperties(ClassMapper classUnderTest)
        {
            // create a inherited class for testing purposes only
            if (classUnderTest.IsAbstractClass)
            {
                var classGen = ClassDeclaration
                    (
                        $"{classUnderTest.ClassName}ForTest"
                    )
                    .WithBaseList(
                        BaseList(
                            SingletonSeparatedList<BaseTypeSyntax>(
                                SimpleBaseType(
                                    IdentifierName(classUnderTest.ClassName)))))
                    .WithModifiers
                    (
                        TokenList(new[]
                        {
                            Token(SyntaxKind.PublicKeyword)
                        })
                    );

                PropertyNodes.Add(classGen);
            }
        }

        protected override void GenerateTestClassSetup(ClassMapper classUnderTest)
        {
            SetupNodes.AddRange(MockFramework.GenerateTestClassSetup(classUnderTest));
            SetupNodes.Add(ParseStatement($"{MethodInvocationVariableName} = new List<IStructuralEquatable>();"));
            SetupNodes.Add(ParseStatement($"{TestMethodResultVariableName} = null;"));
            SetupNodes.Add(ParseStatement($"{TestMethodActionVariableName} = null;"));

            if (classUnderTest.IsAbstractClass)
            {
                classUnderTest.ClassName = $"{classUnderTest.ClassName}ForTest";
            }

            if (!classUnderTest.IsNestedClass)
            {
                if (classUnderTest.Constructors.Where(o => !o.IsNonPublic).Count() > 0)
                {
                    var construct = classUnderTest.Constructors.First(o => !o.IsNonPublic);
                    if (construct.ParametersTypes.Count > 0)
                    {
                        ArrangeParameters(construct, SetupNodes, this);
                    }
                    SetupNodes.AddRange(GenerateClassConstructorInvokeStatement(construct, null));
                }
                else if (!classUnderTest.IsStaticClass)
                {
                    SetupNodes.Add(ParseStatement($"{TestObjectVariableName} = new {classUnderTest.ClassName}();"));
                }
            }
            else
            {
                // Use reflection to instantiate the nested type
                var constructor = classUnderTest.Constructors.FirstOrDefault();
                if (constructor != null)
                {
                    SetupNodes.AddRange(GenerateClassConstructorInvokeStatementViaReflection(classUnderTest, constructor));
                }
            }

            SetupNodes.Add(classUnderTest.IsNestedClass
                ? ParseStatement($"{PrivateTypeMemberName} = new PrivateType(internalClass);")
                : ParseStatement($"{PrivateTypeMemberName} = new PrivateType(typeof({classUnderTest.OriginalClassName}));"));

            if (!classUnderTest.IsStaticClass)
            {
                SetupNodes.Add(ParseStatement($"{PrivateObjectMemberName} = new PrivateObject({TestObjectVariableName}, {PrivateTypeMemberName});"));
            }

            

            if (classUnderTest.Fields.Count > 0)
            {
                var controls = classUnderTest.Fields.Where(o => o.ExtendsControl).ToList();

                if (controls.Count > 0)
                {
                    foreach (var control in controls)
                    {
                        SetupNodes.Add(ParseStatement($"{PrivateObjectMemberName}.SetField(\"{control.Name}\", {ModelsHelper.ConvertToCamelCase(control.Name, false)} = new {control.FieldSymbol.Type.Name} {{ ID = \"{ModelsHelper.ConvertToCamelCase(control.Name, false)}\"}});"));
                        SetupNodes.Add(ParseStatement($"{DisposableObjectsVariableName}.Add({ModelsHelper.ConvertToCamelCase(control.Name, false)});"));
                    }
                }
            }
        }

        protected override void GenerateTestClassOneTimeSetup(ClassMapper classUnderTest)
        {
            OneTimeSetupNodes.Add(ParseStatement($"{CurrentCultureVariableName} = Thread.CurrentThread.CurrentCulture;"));
            OneTimeSetupNodes.Add(ParseStatement("Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;"));
            OneTimeSetupNodes.Add(ParseStatement($"{DisposableObjectsVariableName} = new List<IDisposable>();"));
        }

        protected override void GenerateTestClassCleanup(ClassMapper classUnderTest)
        {
            CleanupNodes.AddRange(MockFramework.GenerateTestClassCleanup(classUnderTest));
            var symbol = classUnderTest.SemanticModel.GetDeclaredSymbol(classUnderTest.Declaration);
            ITypeSymbol classThatImplements;
            if (symbol != null && ModelsHelper.ImplementsInterfaceOrBaseClass(symbol, typeof(IDisposable), out classThatImplements))
            {
                CleanupNodes.Add(ParseStatement($"{TestObjectVariableName}?.Dispose();"));
            }
        }

        protected override void GenerateTestClassOneTimeCleanup(ClassMapper classUnderTest)
        {
            OneTimeCleanupNodes.Add(ParseStatement($"Thread.CurrentThread.CurrentCulture = {CurrentCultureVariableName};"));
            OneTimeCleanupNodes.Add(ParseStatement($"{DisposableObjectsVariableName}.ForEach(o => o?.Dispose());"));
        }

        protected override void GenerateTestCases(ClassMapper classUnderTest)
        {
            new CSharpPatternMatcher<T, M>(classUnderTest, this).GenerateTestCase();

            foreach (var constructor in classUnderTest.Constructors.Where((o => !o.IsNonPublic)))
            {
                new CSharpPatternMatcher<T, M>(constructor, this).GenerateTestCase();
            }

            foreach (var method in classUnderTest.Methods)
            {
                new CSharpPatternMatcher<T, M>(method, this).GenerateTestCase();
            }

            //add helper methods for specific type usage
            if (classUnderTest.ContainsXmlNode)
            {
                MethodNodes.Add(InternalGenerator.MethodDeclaration(
                    "CreateDummyXmlNode",
                    null,
                    null,
                    IdentifierName("XmlNode"),
                    Accessibility.Private,
                    DeclarationModifiers.Static,
                    new List<SyntaxNode>
                    {
                        ParseStatement("var xmlDoc = new XmlDocument();"),
                        ParseStatement("xmlDoc.LoadXml(\"<root><child></child></root>\");"),
                        ParseStatement("return xmlDoc.DocumentElement;")
                    }));
            }
            if (classUnderTest.ContainsXmlDocument)
            {
                MethodNodes.Add(InternalGenerator.MethodDeclaration(
                    "CreateDummyXmlDocument",
                    null,
                    null,
                    IdentifierName("XmlDocument"),
                    Accessibility.Private,
                    DeclarationModifiers.Static,
                    new List<SyntaxNode>
                    {
                        ParseStatement("var xmlDoc = new XmlDocument();"),
                        ParseStatement("xmlDoc.LoadXml(\"<root><child></child></root>\");"),
                        ParseStatement("return xmlDoc;")
                    }));
            }
            if (classUnderTest.ContainsXElement)
            {
                MethodNodes.Add(InternalGenerator.MethodDeclaration(
                    "CreateDummyXElement",
                    null,
                    null,
                    IdentifierName("XElement"),
                    Accessibility.Private,
                    DeclarationModifiers.Static,
                    new List<SyntaxNode>
                    {
                        ParseStatement("var document = XDocument.Parse(\"<element>This is a test</element>\");"),
                        ParseStatement("return document.Root;")
                    }));
            }
        }

        protected override void GenerateUsingDirectives(ClassMapper classUnderTest)
        {
            UsingsNodes.Add(UsingDirective(ParseName("System")));
            UsingsNodes.Add(UsingDirective(ParseName("System.Collections")));
            UsingsNodes.Add(UsingDirective(ParseName("System.Collections.Generic")));
            UsingsNodes.Add(UsingDirective(ParseName("System.Diagnostics.CodeAnalysis")));
            UsingsNodes.Add(UsingDirective(ParseName("System.Globalization")));
            UsingsNodes.Add(UsingDirective(ParseName("System.Threading")));
            UsingsNodes.Add(UsingDirective(ParseName("System.Action")).WithAlias(NameEquals(IdentifierName("Action"))));
            UsingsNodes.AddRange(TestFramework.GenerateUsingDirectives());
            UsingsNodes.Add(UsingDirective(ParseName("Microsoft.VisualStudio.TestTools.UnitTesting")));
            UsingsNodes.AddRange(MockFramework.GenerateUsingDirectives());
            UsingsNodes.Add(UsingDirective(ParseName("Shouldly")));

            if (!classUnderTest.IsNestedClass && !String.IsNullOrEmpty(classUnderTest.Namespace))
            {
                UsingsNodes.Add(UsingDirective(ParseName(classUnderTest.Namespace)));
            }
            else if (classUnderTest.IsNestedClass)
            {
                UsingsNodes.Add(UsingDirective(ParseName("System.Reflection")));
            }

            //if (classUnderTest.Declaration.Parent != null)
            //{
            //    foreach (var originalUsing in classUnderTest.Declaration.Parent.DescendantNodes().OfType<UsingDirectiveSyntax>())
            //    {
            //        AddUsingNodeIfNew(originalUsing, null);
            //    }
            //}
            //else
            //{
            //    foreach (var originalUsing in classUnderTest.Declaration.DescendantNodes().OfType<UsingDirectiveSyntax>())
            //    {
            //        AddUsingNodeIfNew(originalUsing, null);
            //    }
            //}
        }

        public override void GenerateTestProject(string folderPathAndProjectName)
        {
            var type = Type.GetTypeFromProgID("VisualStudio.DTE.15.0");
            var obj = Activator.CreateInstance(type, true);
            var dte = (DTE2)obj;
            var sln = (Solution4)dte.Solution;

            var projectName = folderPathAndProjectName.Substring(folderPathAndProjectName.LastIndexOf("\\") + 1);

            if (TestFramework is NUnitTestFramework)
            {
                var projectTemplatePath = Assembly.GetExecutingAssembly().CodeBase + "..\\..\\Factory\\CSharp\\ProjectTemplates\\NUnit.Template.zip";
                sln.AddFromTemplate(projectTemplatePath, folderPathAndProjectName, projectName, false);
            }
            else if (TestFramework is MsTestsTestFramework)
            {
                var projectTemplatePath = Assembly.GetExecutingAssembly().CodeBase + "..\\..\\Factory\\CSharp\\ProjectTemplates\\MsTests.Template.zip";
                sln.AddFromTemplate(projectTemplatePath, folderPathAndProjectName, projectName, false);
            }
        }

        /// <summary>
        /// Returns mocked code for the given code selection
        /// </summary>
        public override IList<SyntaxNode> MockSelectedCode(TextViewSelection selection)
        {
            var result = new List<SyntaxNode>();
            var walker = new CSharpCustomSyntaxWalker();
            var externalReferences = new Dictionary<SyntaxNode, MockMethodMapper>();
            foreach (var clazz in MappedModel.Keys)
            {
                walker.Visit(clazz.Declaration);
                foreach (var line in walker.LineMap.Keys)
                {
                    if (line >= selection.StartPosition.Line && line <= selection.EndPosition.Line)
                    {
                        foreach (var node in walker.LineMap[line])
                        {
                            ModelsHelper.ExtractExternalReferences(node, clazz, externalReferences, false, null);
                        }
                    }
                }
                walker.LineMap.Clear();
            }

            if (externalReferences.Count > 0)
            {
                foreach (var externalCall in externalReferences)
                {
                    result.AddRange(MockFramework.CreateMethodMock(externalCall.Key,
                        externalCall.Value, externalCall.Value.Class.SemanticModel, this, false));
                }
            }
            return result;
        }

        
    }
}
