﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTGeneratorCore.Models;

namespace UTGeneratorCore.Factory.CSharp
{
    public class CSharpCustomSyntaxWalker : CSharpSyntaxWalker
    {
        public IDictionary<int, IList<SyntaxNode>> LineMap { get; }

        public CSharpCustomSyntaxWalker() : base(SyntaxWalkerDepth.StructuredTrivia)
        {
            LineMap = new Dictionary<int, IList<SyntaxNode>>();
        }

        public override void Visit(SyntaxNode node)
        {
            var parent = node.SyntaxTree.GetRoot();
            AddLine(node, node.Span.Start, parent);
            if (node.ChildNodes().Count() > 0)
            {
                foreach (var childNode in node.ChildNodes())
                {
                    Visit(childNode);
                }
            }
        }

        private void AddLine(SyntaxNode node, int position, SyntaxNode parent)
        {
            var text = parent.GetText();
            var line = text.Lines.GetLineFromPosition(position).LineNumber;

            if (!LineMap.ContainsKey(line))
            {
                LineMap.Add(line, new List<SyntaxNode>());
            }

            LineMap[line].Add(node);
        }
    }
}
