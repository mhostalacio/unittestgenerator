﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace UTGeneratorCore.Factory
{
    public interface ITestGenerator
    {
        List<SyntaxNode> FieldNodes { get; }
        List<SyntaxNode> PropertyNodes { get; }
        List<SyntaxNode> SetupNodes { get; }
        List<SyntaxNode> OneTimeSetupNodes { get; }
        List<SyntaxNode> CleanupNodes { get; }
        List<SyntaxNode> OneTimeCleanupNodes { get; }
        List<SyntaxNode> MethodNodes { get; }
        List<UsingDirectiveSyntax> UsingsNodes { get; }

        bool PlaceMocksInSetupMethod { get; }
        string AddUsingNodeIfNew(UsingDirectiveSyntax usingDirectiveSyntax, string className);
    }
}
