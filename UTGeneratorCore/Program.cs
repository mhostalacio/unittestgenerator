﻿using System;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using UTGeneratorCore.Factory.CSharp;
using UTGeneratorCore.Factory.Frameworks;

namespace UTGeneratorCore
{
    class Program
    {
        static void Main(string[] args)
        {
            var testFramework = NUnitTestFramework.Instance;
            var mockFramnework = MsFakesMockFramework.Instance;
            var filePath = ConfigurationManager.AppSettings["filePath"];
            var output = ConfigurationManager.AppSettings["output"];

            if (string.IsNullOrEmpty(filePath) || !File.Exists(filePath))
            {
                Console.WriteLine($"Invalid filePath setting '{filePath}'.");
            }
            else if (!string.IsNullOrEmpty(output) && output.Equals(filePath, StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine($"filePath and output settings cannot be same.");
            }
            else
            {
                var gen = new CSharpTestGenerator<NUnitTestFramework, MsFakesMockFramework>(filePath, testFramework, mockFramnework, null);
                gen.PlaceMocksInSetupMethod = false;
                //gen.AvoidMethodInvocationCounterAsTestParameter = true;
                var code = gen.FixFormatting(gen.GenerateFullTestClass().ToString());
                
                if (!string.IsNullOrEmpty(output))
                {
                    if (File.Exists(output))
                    {
                        File.Delete(output);
                    }

                    File.WriteAllText(output, code);

                    Console.WriteLine($"Test file created at '{output}'");

                }
                else
                {
                    Console.WriteLine(code.ToString());
                }
            }

            Console.ReadLine();
        }

    }
}
