﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using UTGeneratorCore.Factory.CSharp;

namespace UTGeneratorCore.Models
{
    public class ClassMapper
    {
        public string AssemblyName { get; }
        public string OriginalClassName { get; set; }
        public string ClassName { get; internal set; }
        public IList<ConstructorMapper> Constructors { get; }
        public IDictionary<SyntaxNode, MockMethodMapper> ExternalReferences { get; private set; }
        public IList<FieldMapper> Fields { get; }
        public IList<PropertyMapper> Properties { get; }
        public string FilePath { get; }
        public string FullName => $"{Namespace}.{ClassName}";
        public bool IsNestedClass { get; }

        public bool IsStaticClass { get; }
        public bool IsAbstractClass { get; }
        public int LinesCount { get; private set; }
        public IList<MethodMapper> Methods { get; }
        public int MethodsCount { get; private set; }
        public string Namespace { get; }
        public ClassDeclarationSyntax ParentClass { get; set; }
        public ClassDeclarationSyntax Declaration { get; set; }
        public IDictionary<ClassDeclarationSyntax, SemanticModel> PartialClasses { get; }
        public SemanticModel SemanticModel { get; }

        //To deal with some specific object we identify their usage and generate some helper methods and properties
        public bool ContainsXmlNode { get; set; }
        public bool ContainsHttpContext { get; set; }
        public bool ContainsXmlDocument { get; set; }
        public bool ContainsXElement { get; set; }

        public List<string> AlreadyAddedMocks { get; }
        public Dictionary<string, string> UsingNodesNickNames { get; }

        public ClassMapper(
            TypeDeclarationSyntax classDeclaration,
            SemanticModel model,
            CSharpCompilation compilation,
            string filePath,
            string assemblyName = null)
        {
            SemanticModel = model;
            AssemblyName = assemblyName;
            FilePath = filePath;
            OriginalClassName = Convert.ToString(classDeclaration.Identifier);
            ClassName = Convert.ToString(classDeclaration.Identifier);
            Declaration = (ClassDeclarationSyntax)classDeclaration;
            ContainsXmlNode = classDeclaration.ToString().Contains("XmlNode");
            ContainsXmlDocument = classDeclaration.ToString().Contains("XmlDocument");
            ContainsXElement = classDeclaration.ToString().Contains("XElement");
            ContainsHttpContext = classDeclaration.ToString().Contains("HttpContext");
            AlreadyAddedMocks = new List<string>();
            UsingNodesNickNames = new Dictionary<string, string>();
            if (classDeclaration.Modifiers.Any(o => o.Kind() == SyntaxKind.PartialKeyword))
            {
                PartialClasses = new Dictionary<ClassDeclarationSyntax, SemanticModel>();
                var symbol = model.GetDeclaredSymbol(classDeclaration);
                foreach (var partialClassRef in symbol.DeclaringSyntaxReferences)
                {
                    var partialClasses = new List<SyntaxNode>(ModelsHelper.GetClassSyntaxNodesFromCompilationUnit(partialClassRef.SyntaxTree.GetCompilationUnitRoot()));

                    var tree = compilation.GetSemanticModel(partialClassRef.SyntaxTree);

                    compilation.AddReferences(CSharpHelper.CommonReferences); //TODO: change this -> should be language agnostic
                    
                    foreach (var partialClass in partialClasses)
                    {
                        if (partialClass != classDeclaration && ((ClassDeclarationSyntax)partialClass).Identifier.ToString().Equals(classDeclaration.Identifier.ToString()))
                        {
                            PartialClasses.Add((ClassDeclarationSyntax)partialClass, tree);
                        }
                    }
                }
            }

            switch (classDeclaration.Parent)
            {
                case NamespaceDeclarationSyntax parentNamespace:
                    Namespace = Convert.ToString(parentNamespace.Name);
                    break;
                case ClassDeclarationSyntax classDeclarationSyntax:
                {
                    IsNestedClass = true;
                    ParentClass = classDeclarationSyntax;

                    if (ParentClass.Parent is NamespaceDeclarationSyntax parentParentNamespace)
                    {
                        Namespace = $"{parentParentNamespace.Name}.{ParentClass.Identifier.ToString()}";
                    }
                    else
                    {
                        Namespace = $"{ParentClass.Identifier.ToString()}";
                    }

                    break;
                }
                case CompilationUnitSyntax _:
                    Namespace = string.Empty; //TODO :- Does this ever be case? Class's Parent to be CompilationUnit?
                    break;
            }

            IsStaticClass = classDeclaration.Modifiers.Any(o => o.Kind() == SyntaxKind.StaticKeyword);
            IsAbstractClass = classDeclaration.Modifiers.Any(o => o.Kind() == SyntaxKind.AbstractKeyword);
            Constructors = new List<ConstructorMapper>();
            Methods = new List<MethodMapper>();
            Fields = new List<FieldMapper>();
            Properties = new List<PropertyMapper>();
            ExtractMembers(classDeclaration, model);

            if (PartialClasses == null || PartialClasses.Count <= 0)
            {
                return;
            }

            foreach (var partialClass in PartialClasses)
            {
                foreach (var field in partialClass.Key.DescendantNodes().OfType<FieldDeclarationSyntax>())
                {
                    var fieldToAdd = new FieldMapper(field, partialClass.Value, this);
                    if (Fields.FirstOrDefault(o => o.Name == fieldToAdd.Name) == null)
                    {
                        Fields.Add(fieldToAdd);
                    }
                }
            }
        }

        private void ExtractMembers(TypeDeclarationSyntax classDeclaration, SemanticModel model)
        {
            foreach (var member in classDeclaration.Members)
            {
                switch (member.Kind())
                {
                    case SyntaxKind.ConstructorDeclaration:
                        var constructor = new ConstructorMapper((ConstructorDeclarationSyntax)member, this, model);
                        LinesCount += constructor.LinesCount;
                        Constructors.Add(constructor);
                        break;
                    case SyntaxKind.MethodDeclaration:
                        MethodsCount++;
                        var method = new MethodMapper((MethodDeclarationSyntax)member, model, this);
                        LinesCount += method.LinesCount;
                        Methods.Add(method);
                        break;
                    case SyntaxKind.FieldDeclaration:
                        var field = new FieldMapper((FieldDeclarationSyntax)member, model, this);
                        Fields.Add(field);
                        break;
                    case SyntaxKind.PropertyDeclaration:
                        var prop = new PropertyMapper((PropertyDeclarationSyntax)member, model, this);
                        Properties.Add(prop);
                        break;
                }
            }
        }
    }
}
