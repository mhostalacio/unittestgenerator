﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace UTGeneratorCore.Models
{
    public class MockMethodMapper
    {
        public IList<ExpressionStatementSyntax> AssertionsToBeDone { get; }
        public string MethodInvokeCountVariableName
        {
            get {
               
                return $"{ModelsHelper.ConvertToCamelCase(MethodName + MockMethodName, false)}CallCount";
            }
        }

        //public bool AvoidMethodInvokeCount
        //{
        //    get { return Symbol is IFieldSymbol; }
        //}

        public string MethodName { get; }
        public string MockMethodName { get; }
        public string NamespaceOwner { get; }
        public ISymbol EnumeratorTypeSymbol { get; set; }
        public ISymbol EnumeratorItemTypeSymbol { get; set; }
        public ISymbol Symbol { get; }
        public IList<IDictionary<string, ArgumentSyntax>> UsedParameterValues { get; }
        public ClassMapper Class { get; internal set; }

        private IList<IParameterSymbol> _parameters;
        public bool AddIsNullFlag { get; set; }

        public string ReturnExpressionString { get; set; }
        public bool IsInternalCall { get; set; }
        public IList<IParameterSymbol> Parameters
        {
            get
            {
                if (_parameters == null)
                {
                    _parameters = new List<IParameterSymbol>();
                    if (Symbol is IMethodSymbol methodSymbol && !methodSymbol.Parameters.IsEmpty)
                    {
                        foreach (var paramSym in methodSymbol.Parameters)
                        {
                            _parameters.Add(paramSym);
                        }
                    }
                }
                return _parameters;
            }
        }

        private IList<ITypeSymbol> _typeArguments;
        public IList<ITypeSymbol> TypeArguments
        {
            get
            {
                if (_typeArguments == null)
                {
                    if (Symbol is IMethodSymbol methodSymbol && !methodSymbol.TypeArguments.IsEmpty)
                    {
                        _typeArguments = methodSymbol.TypeArguments;
                    }
                    else
                    {
                        _typeArguments = new List<ITypeSymbol>();
                    }
                }
                return _typeArguments;
            }
        }

        private bool? _isGeneric;
        public bool IsGeneric
        {
            get
            {
                if (_isGeneric == null)
                {
                    if (Symbol is IMethodSymbol)
                    {
                        _isGeneric = ((IMethodSymbol)Symbol).IsGenericMethod;
                    }
                    else
                    {
                        _isGeneric = false;
                    }
                }
                return _isGeneric.Value;
            }
        }

        private bool? _isConstructor;
        public bool IsConstructor
        {
            get
            {
                if (_isConstructor == null)
                {
                    if (Symbol is IMethodSymbol)
                    {
                        _isConstructor = ((IMethodSymbol)Symbol).MethodKind == MethodKind.Constructor;
                    }
                    else
                    {
                        _isConstructor = false;
                    }
                }
                return _isConstructor.Value;
            }
        }

        private bool? _returnsVoid;
        public bool ReturnsVoid
        {
            get
            {
                if (_returnsVoid == null)
                {
                    if (Symbol is IMethodSymbol)
                    {
                        _returnsVoid = ((IMethodSymbol)Symbol).ReturnsVoid;
                    }
                    else
                    {
                        _returnsVoid = false;
                    }
                }
                return _returnsVoid.Value;
            }
        }

        private ITypeSymbol _mockType;
        public ITypeSymbol MockType
        {
            get
            {
                if (_mockType == null)
                {
                    if (Symbol is IMethodSymbol methodSymbol)
                    {
                        _mockType = methodSymbol.ReturnType;
                    }
                    else if (Symbol is IPropertySymbol)
                    {
                        _mockType = ((IPropertySymbol)Symbol).Type;
                    }
                    else if (Symbol is IFieldSymbol)
                    {
                        _mockType = ((IFieldSymbol)Symbol).Type;
                    }
                }
                return _mockType;
            }
        }

        public MockMethodMapper(ISymbol symbol, ClassMapper parent)
        {
            Symbol = symbol;
            UsedParameterValues = new List<IDictionary<string, ArgumentSyntax>>();
            AssertionsToBeDone = new List<ExpressionStatementSyntax>();

            MethodName = Symbol.Name.Equals(".ctor") ?
                    Symbol.ContainingSymbol.Name :
                    Symbol.Name;

            MockMethodName = BuildMockMethodName();
            if (symbol.ContainingSymbol != null && !symbol.ContainingNamespace.ToDisplayString().Equals("<global namespace>"))
            {
                NamespaceOwner = $"{symbol.ContainingNamespace.ToDisplayString()}.Fakes";
            }

            Class = parent;
        }

        private string BuildMockMethodName()
        {
            var builder = new StringBuilder();
            var resolvedName = false;
            if (Symbol is IMethodSymbol methodSymbol)
            {
                if (methodSymbol.MethodKind == MethodKind.PropertyGet)
                {
                    if (methodSymbol.Name.Equals("get_Item"))
                    {
                        resolvedName = true;
                        builder.Append("ItemGet");
                    }
                    else if (methodSymbol.Name.Equals("get_ItemOf"))
                    {
                        resolvedName = true;
                        builder.Append("ItemOfGet");
                    }
                }
                else if (methodSymbol.IsExtensionMethod && methodSymbol.ReducedFrom != null && methodSymbol.ReducedFrom.Parameters.Length > 0)
                {
                    Parameters.Insert(0, methodSymbol.ReducedFrom.Parameters.First());
                }
            }

            if (!resolvedName)
            {
                builder.Append(IsConstructor
                    ? "Constructor"
                    : Symbol.Name);
            }

            if (IsGeneric)
            {
                builder.Append("Of1");
            }

            var genericTypeCount = 0;

            foreach (var param in Parameters)
            {
                if (string.IsNullOrEmpty(param.Type.Name))
                {
                    if (param.Type.Kind == SymbolKind.ArrayType)
                    {
                        var array = (IArrayTypeSymbol)param.Type;
                        builder.Append($"{array.ElementType.Name}Array");
                    }
                    else
                    {
                        // TODO
                    }
                }
                else if (param.Type.Name.Equals("dynamic"))
                {
                    builder.Append("Object");
                }
                else
                {
                    if (IsGeneric && param.OriginalDefinition.Type.Kind == SymbolKind.TypeParameter
                                  && param.OriginalDefinition.Type.TypeKind == TypeKind.TypeParameter)
                    {
                        builder.Append("M").Append(genericTypeCount);
                        genericTypeCount++;
                        continue;
                    }

                    if (param.Type.ContainingType != null && param.Type.ContainingType.TypeKind == TypeKind.Class)
                    {
                        builder.Append(param.Type.ContainingType.Name);
                    }
                    builder.Append(param.Type.Name);

                    if (!(param.Type is INamedTypeSymbol namedTypeSymbol) || !namedTypeSymbol.IsGenericType)
                    {
                        if (param.RefKind == RefKind.Out)
                        {
                            builder.Append("Out");
                        }
                        if (param.RefKind == RefKind.Ref)
                        {
                            builder.Append("Ref");
                        }
                        continue;
                    }

                    builder.Append("Of");

                    var parts = param.OriginalDefinition.ToDisplayParts();

                    //TODO: fix the following conditions: horrible!

                    if (namedTypeSymbol.Name.Equals("Nullable") && parts.Any() && parts[parts.Count() - 2].Symbol != null)
                    {
                        builder.Append(parts[parts.Count() - 2].Symbol.Name);
                    }
                    else if (namedTypeSymbol.Name.Equals("Func") && parts.Any())
                    {
                        var start = false;
                        foreach (var part in parts)
                        {
                            if (start)
                            {
                                genericTypeCount = HandlePart(part, builder, genericTypeCount);
                            }
                            if (!start && part.ToString().Equals("<"))
                            {
                                start = true;
                            }
                        }
                    }
                    else if (parts.Count() > 1 &&
                             parts.Last().ToString().Equals(">") &&
                             parts[parts.Length - 2].Symbol != null)
                    {
                        bool start = false;
                        foreach (var part in parts)
                        {
                            if (start)
                            {
                                genericTypeCount = HandlePart(part, builder, genericTypeCount);
                            }
                            if (!start && part.Symbol != null && part.Symbol.Name.Equals(namedTypeSymbol.Name))
                            {
                                start = true;
                            }
                        }
                    }
                    else
                    {
                        builder.Append("M").Append(genericTypeCount);
                        genericTypeCount++;
                    } 
                }

                if (param.RefKind == RefKind.Out)
                {
                    builder.Append("Out");
                }
                if (param.RefKind == RefKind.Ref)
                {
                    builder.Append("Ref");
                }
            }

            if (IsGeneric)
            {
                builder.Append("<");
                var count = 0;

                foreach (var arg in TypeArguments)
                {
                    if (count > 0)
                    {
                        builder.Append(", ");
                    }
                    builder.Append(arg.Name);
                    count++;
                }

                builder.Append(">");
            }

            if (Symbol is IPropertySymbol)
            {
                builder.Append("Get");
            }

            return builder.ToString();
        }

        private static int HandlePart(SymbolDisplayPart part, StringBuilder builder, int genericTypeCount)
        {
            if (part.Symbol != null && part.Kind != SymbolDisplayPartKind.NamespaceName)
            {
                if (part.Symbol.Kind == SymbolKind.TypeParameter)
                {
                    builder.Append("M").Append(genericTypeCount);
                    genericTypeCount++;
                }
                else
                {
                    builder.Append(part.Symbol.Name);
                    if (part.Symbol is INamedTypeSymbol namedSymbol && namedSymbol.IsGenericType)
                    {
                        builder.Append("Of");
                    }
                }
            }

            return genericTypeCount;
        }

        private static bool IsFromSystemNamespace(SymbolDisplayPart part)
        {
            return part.Symbol.ContainingNamespace.ToDisplayString().StartsWith("System.") ||
                   part.Symbol.ContainingNamespace.ToDisplayString().Equals("System");
        }
    }
}
