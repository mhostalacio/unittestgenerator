﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Editing;

namespace UTGeneratorCore.Models
{
    internal static class ModelsHelper
    {
        public const string ToolVersionCategory = "HUT Template Tool version 3.5.10";

        public static string ConvertToCamelCase(string variableName, bool isLocalVariable)
        {
            if (variableName.Length > 100)
            {
                variableName = variableName.Substring(variableName.Length - 100);
            }
            variableName = RemoveSpecialCharacters(variableName);
            variableName = Regex.Replace(variableName, "([A-Z])([A-Z]+)($|[A-Z])",
                m => m.Groups[1].Value + m.Groups[2].Value.ToLower() + m.Groups[3].Value);
            variableName = char.ToLowerInvariant(variableName[0]) + variableName.Substring(1);
            variableName = ReplaceReservedWords(variableName);
            return isLocalVariable ? variableName : "_" + variableName;
        }

        private static string ReplaceReservedWords(string variableName)
        {
            if (variableName.ToLowerInvariant() == "value")
                return "givenValue";
            if (variableName.ToLowerInvariant() == "group")
                return "givenGroup";
            if (variableName.ToLowerInvariant() == "checked")
                return "checkedValue";
            if (variableName.ToLowerInvariant() == "var")
                return "givenVar";
            if (variableName.ToLowerInvariant() == "set")
                return "givenSet";
            if (variableName.ToLowerInvariant() == "from")
                return "givenFrom";
            if (variableName.ToLowerInvariant() == "add")
                return "givenAdd";
            if (variableName.ToLowerInvariant() == "async")
                return "givenAsync";
            if (variableName.ToLowerInvariant() == "await")
                return "givenAwait";
            if (variableName.ToLowerInvariant() == "dynamic")
                return "givenDynamic";
            if (variableName.ToLowerInvariant() == "get")
                return "givenGet";
            if (variableName.ToLowerInvariant() == "global")
                return "givenGlobal";
            if (variableName.ToLowerInvariant() == "partial")
                return "givenPartial";
            if (variableName.ToLowerInvariant() == "remove")
                return "givenRemove";
            if (variableName.ToLowerInvariant() == "when")
                return "givenWhen";
            if (variableName.ToLowerInvariant() == "where")
                return "givenWhere";
            if (variableName.ToLowerInvariant() == "yield")
                return "givenYield";
            if (variableName.ToLowerInvariant() == "select")
                return "givenSelect";
            if (variableName.ToLowerInvariant() == "into")
                return "givenInto";
            if (variableName.ToLowerInvariant() == "orderby")
                return "givenOrderby";
            if (variableName.ToLowerInvariant() == "join")
                return "givenJoin";
            if (variableName.ToLowerInvariant() == "let")
                return "givenLet";
            if (variableName.ToLowerInvariant() == "in")
                return "givenIn";
            if (variableName.ToLowerInvariant() == "on")
                return "givenOn";
            if (variableName.ToLowerInvariant() == "equals")
                return "givenEquals";
            if (variableName.ToLowerInvariant() == "by")
                return "givenBy";
            if (variableName.ToLowerInvariant() == "ascending")
                return "givenAscending";
            if (variableName.ToLowerInvariant() == "descending")
                return "givenDescending";
            return variableName;
        }

        public static string GetLocalVariabeAssertionName(string variableName)
        {
            return $"expected{char.ToUpperInvariant(variableName[1])}{variableName.Substring(2)}";
        }

        public static string ConvertToPascalCase(string variableName)
        {
            variableName = RemoveSpecialCharacters(variableName);
            return char.ToUpperInvariant(variableName[0]) + variableName.Substring(1);
        }

        private static string RemoveSpecialCharacters(string name)
        {
            return name
                .Replace(".", string.Empty)
                .Replace("_", string.Empty)
                .Replace("<", string.Empty)
                .Replace(">", string.Empty)
                .Replace("[", string.Empty)
                .Replace("]", string.Empty)
                .Replace("(", string.Empty)
                .Replace(")", string.Empty)
                .Replace(" ", string.Empty)
                .Replace(",", string.Empty)
                .Replace("\"", string.Empty)
                .Replace("+", string.Empty)
                .Replace("-", string.Empty)
                .Replace("=", string.Empty)
                .Replace(":", string.Empty)
                .Replace("\r", string.Empty)
                .Replace("\n", string.Empty);
        }

        public static IEnumerable<SyntaxNode> GetClassSyntaxNodesFromCompilationUnit(CompilationUnitSyntax root)
        {
            return root
                .DescendantNodes()
                .OfType<ClassDeclarationSyntax>();
        }

        public static string NormalizePropertyName(string variableName)
        {
            //TODO: replace reserved names to dummy names -> it's safe to do here
            variableName = variableName.Replace("_", string.Empty);
            return ConvertToPascalCase(variableName);
        }

        public static bool ImplementsInterfaceOrBaseClass(ITypeSymbol typeSymbol, Type typeToCheck, out ITypeSymbol classThatImplements)
        {
            classThatImplements = null;
            if (typeSymbol == null)
            {
                return false;
            }

            if (typeSymbol.MetadataName == typeToCheck.Name)
            {
                classThatImplements = typeSymbol;
                return true;
            }

            var baseType = typeSymbol;

            while (baseType != null)
            {
                if (baseType.MetadataName == typeToCheck.Name)
                {
                    classThatImplements = baseType;
                    return true;
                }

                foreach (var symbolInterface in baseType.Interfaces)
                {
                    if (symbolInterface.MetadataName == typeToCheck.Name)
                    {
                        classThatImplements = baseType;
                        return true;
                    }
                }

                baseType = baseType.BaseType;
            }

            

            return false;
        }

        public static ITypeSymbol GetTypeFromSymbol(ISymbol symbol, SemanticModel model)
        {
            // TODO: Use reflection to find Type property from ISymbol
            // Change this method when Roslyn compilers provide separate interface for ITypeSymbol property
            var returnType = GetProperty<ITypeSymbol>(symbol, "Type");
            if (symbol is IMethodSymbol methodSymbol && methodSymbol.MethodKind == MethodKind.Constructor)
            {
                returnType = methodSymbol.ContainingType;
            }
            if (returnType == null)
            {
                returnType = GetProperty<ITypeSymbol>(symbol, "ReturnType");
            }
            if (returnType is IErrorTypeSymbol errorTypeSymbol)
            {
                if (errorTypeSymbol.CandidateSymbols.Length > 0 && errorTypeSymbol.CandidateSymbols.First() is ITypeSymbol)
                {
                    returnType = (ITypeSymbol)errorTypeSymbol.CandidateSymbols.First();
                }
                else
                {
                    returnType = model.Compilation.GetTypeByMetadataName("System.Object");
                }
            }
            if (returnType == null && symbol is INamedTypeSymbol namedTypeSymbol && namedTypeSymbol.IsType)
            {
                returnType = namedTypeSymbol;
            }
            return returnType;
        }

        public static T GetProperty<T>(ISymbol symbol, string propertyName) where T : class
        {
            var typeProperty = symbol.GetType().GetProperty(propertyName);

            return typeProperty?.GetValue(symbol) as T;
        }

        /// <summary>
        /// Parses the code to find external calls to mock and add them to a list to be processed later
        /// </summary>
        public static void ExtractExternalReferences(SyntaxNode node, ClassMapper clazz, IDictionary<SyntaxNode, MockMethodMapper> externalReferences, bool avoidOverMocking, BaseMethodDeclarationSyntax methodOwner)
        {
            var symbolInfo = GetSymbolInfoFromSyntaxNode(node, clazz.SemanticModel, methodOwner, clazz.Declaration, node);
            if (symbolInfo != null && symbolInfo.Symbol != null && ShouldMock(symbolInfo))
            {
                ExtractExternalReferencesFromSymbol(node, clazz, externalReferences, avoidOverMocking, symbolInfo);
            }
        }

        public static bool ShouldMock(SymbolInfoWrapper symbolInfo)
        {
            if (symbolInfo.Symbol is IFieldSymbol fieldSymbol && fieldSymbol.HasConstantValue)
            {
                return false;
            }
            return (symbolInfo.Symbol.ContainingType == null || (symbolInfo.Symbol.ContainingType != null && !ClassesToNotShim.Contains(symbolInfo.Symbol.ContainingType.Name)));
        }

        public static void ExtractExternalReferencesFromSymbol(SyntaxNode node, ClassMapper clazz, IDictionary<SyntaxNode, MockMethodMapper> externalReferences, bool avoidOverMocking, SymbolInfoWrapper symbolInfo)
        {
            // avoid mocking properties by default -> only when mocking selected code
            if (avoidOverMocking && !(symbolInfo.Symbol is IMethodSymbol))
            {
                return;
            }

            var mockMap = externalReferences.Values.FirstOrDefault(o => o.Symbol.Equals(symbolInfo.Symbol));
            if (mockMap == null)
            {
                mockMap = new MockMethodMapper(symbolInfo.Symbol, clazz);
                mockMap.IsInternalCall = symbolInfo.IsInternal;
                if (!mockMap.ReturnsVoid && mockMap.MockType is null)
                {
                    //TODO: need to check this case
                    return;
                }
                externalReferences.Add(node, mockMap);
            }

            if (node is MemberAccessExpressionSyntax)
            {
                if (node.Parent is InvocationExpressionSyntax invokeNode)
                {
                    var invokeValues = new Dictionary<string, ArgumentSyntax>();
                    var idx = 0;

                    //TODO: check why it's null
                    if (invokeNode.ArgumentList != null)
                    {
                        foreach (var arg in invokeNode.ArgumentList.Arguments)
                        {
                            //TODO: check why sometimes it has less parameters than expected
                            if (mockMap.Parameters.Count() > idx)
                            {
                                invokeValues.Add(mockMap.Parameters[idx].Name, arg);
                                idx++;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    mockMap.UsedParameterValues.Add(invokeValues);
                }
            }
            else if (node is ObjectCreationExpressionSyntax objectCreationExpressionSyntax)
            {
                var invokeValues = new Dictionary<string, ArgumentSyntax>();
                var idx = 0;

                //TODO: check why it's null
                if (objectCreationExpressionSyntax.ArgumentList != null)
                {
                    foreach (var arg in objectCreationExpressionSyntax.ArgumentList.Arguments)
                    {
                        //TODO: check why sometimes it has less parameters than expected
                        if (mockMap.Parameters.Count() > idx)
                        {
                            invokeValues.Add(mockMap.Parameters[idx].Name, arg);
                            idx++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                mockMap.UsedParameterValues.Add(invokeValues);
            }
            else if (node is InvocationExpressionSyntax invocationExpressionSyntax)
            {
                var invokeValues = new Dictionary<string, ArgumentSyntax>();
                var idx = 0;

                if (invocationExpressionSyntax.ArgumentList != null)
                {
                    foreach (var arg in invocationExpressionSyntax.ArgumentList.Arguments)
                    {
                        //TODO: check why sometimes it has less parameters than expected
                        if (mockMap.Parameters.Count() > idx)
                        {
                            invokeValues.Add(mockMap.Parameters[idx].Name, arg);
                            idx++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                mockMap.UsedParameterValues.Add(invokeValues);
            }
            else if (node is VariableDeclaratorSyntax variableDeclaratorSyntax 
                     && variableDeclaratorSyntax.Initializer != null
                     && variableDeclaratorSyntax.Initializer.Value != null)
            {
                if (variableDeclaratorSyntax.Initializer.Value is BinaryExpressionSyntax binaryExpressionSyntax
                    && binaryExpressionSyntax.Left is ElementAccessExpressionSyntax)
                {
                    ExtractExternalReferencesFromSymbol(binaryExpressionSyntax.Left, clazz, externalReferences, avoidOverMocking, symbolInfo);
                }

            }
            else if (node is ElementAccessExpressionSyntax elementAccessExpressionSyntax)
            {
                var invokeValues = new Dictionary<string, ArgumentSyntax>();
                var idx = 0;
                if (elementAccessExpressionSyntax.ArgumentList != null)
                {
                    foreach (var arg in elementAccessExpressionSyntax.ArgumentList.Arguments)
                    {
                        //TODO: check why sometimes it has less parameters than expected
                        if (mockMap.Parameters.Count() > idx)
                        {
                            invokeValues.Add(mockMap.Parameters[idx].Name, arg);
                            idx++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                mockMap.UsedParameterValues.Add(invokeValues);
            }
        }

        /// <summary>
        /// Parses the code to find external calls to mock and add them to a list to be processed later
        /// </summary>
        public static SymbolInfoWrapper GetSymbolInfoFromSyntaxNode(SyntaxNode node, SemanticModel model, BaseMethodDeclarationSyntax methodOwner, ClassDeclarationSyntax classOwner, SyntaxNode originalNode)
        {
            SymbolInfoWrapper symbolInfoWrapper = null;
            if (node is BinaryExpressionSyntax)
            {
                node = ((BinaryExpressionSyntax) node).Left;
            }
            if (node is CastExpressionSyntax)
            {
                node = ((CastExpressionSyntax)node).Expression;
            }
            if (node is MemberAccessExpressionSyntax ||
                node is ObjectCreationExpressionSyntax ||
                node is InvocationExpressionSyntax ||
                node is PropertyDeclarationSyntax ||
                node is IdentifierNameSyntax ||
                node is ElementAccessExpressionSyntax)
            {
                var symbolInfo = model.GetSymbolInfo(node);
                ISymbol symbol = symbolInfo.Symbol;

                // Special cases: ToLower() and ToUpper()
                if (node is InvocationExpressionSyntax invocationExpression
                    && symbol != null && (symbol.Name.Equals("ToLower")
                    || symbol.Name.Equals("ToUpper")))
                {
                    symbol = symbol.ContainingSymbol;
                }
                if (symbol == null && symbolInfo.CandidateSymbols.Length >= 1 && (symbolInfo.CandidateReason == CandidateReason.Ambiguous || symbolInfo.CandidateReason == CandidateReason.OverloadResolutionFailure))
                {
                    symbolInfoWrapper = ResolveSymbolWhenMismatch(node, symbolInfo);
                }
                else if (symbol != null)
                {
                    if (symbol is IPropertySymbol propertySymbol && propertySymbol.Name.Equals("this[]") && propertySymbol.GetMethod != null)
                    {
                        symbol = propertySymbol.GetMethod;
                    }
                    symbolInfoWrapper = new SymbolInfoWrapper(symbol, false);
                }
                else
                {
                    // symbol is not found -> can be a lot of reasons
                    // CASE 1: local variable -> search within the method
                    // CASE 2: class property -> search within the class
                    // CASE 3: class field -> search within the class
                    TypeSyntax nodeType = null;
                    if (node is MemberAccessExpressionSyntax memberAccessExpressionSyntax)
                    {
                        if (memberAccessExpressionSyntax.Expression is MemberAccessExpressionSyntax)
                        {
                            return GetSymbolInfoFromSyntaxNode(memberAccessExpressionSyntax.Expression, model, methodOwner, classOwner, originalNode);
                        }
                        else if (memberAccessExpressionSyntax.Expression is IdentifierNameSyntax identifierNameSyntax)
                        {
                            //handle case 1: local variable
                            if (methodOwner != null)
                            {
                                var localVariableDeclaration = methodOwner.DescendantNodes()
                                    .OfType<LocalDeclarationStatementSyntax>()
                                    .FirstOrDefault(o => o.Declaration.Variables.Count > 0 && o.Declaration.Variables[0].Identifier.ToString().Equals(identifierNameSyntax.Identifier.ToString()));
                                if (localVariableDeclaration != null
                                    && localVariableDeclaration.Declaration != null)
                                {
                                    nodeType = localVariableDeclaration.Declaration.Type;
                                }
                            }

                            //handle case 2: class property
                            if (nodeType == null && classOwner != null)
                            {
                                var localVariableDeclaration = classOwner.DescendantNodes()
                                    .OfType<PropertyDeclarationSyntax>()
                                    .FirstOrDefault(o => o.Identifier.ToString().Equals(identifierNameSyntax.Identifier.ToString()));
                                if (localVariableDeclaration != null
                                    && localVariableDeclaration != null)
                                {
                                    nodeType = localVariableDeclaration.Type;
                                }
                            }

                            //handle case 3: class field
                            if (nodeType == null && classOwner != null)
                            {
                                var localFieldDeclaration = classOwner.DescendantNodes()
                                    .OfType<FieldDeclarationSyntax>()
                                    .FirstOrDefault(o => o.Declaration.Variables.Count > 0 && o.Declaration.Variables[0].Identifier.ToString().Equals(identifierNameSyntax.Identifier.ToString()));

                                if (localFieldDeclaration != null
                                    && localFieldDeclaration.Declaration != null)
                                {
                                    nodeType = localFieldDeclaration.Declaration.Type;
                                }
                            }

                            //if type is found, locate the property within it
                            if (nodeType != null)
                            {
                                symbolInfo = model.GetSymbolInfo(nodeType);
                                symbol = symbolInfo.Symbol;
                                if (symbol != null && symbol is INamedTypeSymbol namedSymbol)
                                {
                                    while (symbolInfoWrapper == null && namedSymbol != null)
                                    {
                                        var memberSymbol = namedSymbol.GetMembers().FirstOrDefault(o => o.Name.Equals(memberAccessExpressionSyntax.Name.ToString()));
                                        if (memberSymbol != null)
                                        {
                                            symbolInfoWrapper = new SymbolInfoWrapper(memberSymbol, true); // replace it to make sure mock code will be generated
                                        }

                                        namedSymbol = namedSymbol.BaseType;
                                    }
                                }
                            }
                        }
                    }
                    else if (node is ElementAccessExpressionSyntax elementAccessExpressionSyntax)
                    {
                        return GetSymbolInfoFromSyntaxNode(elementAccessExpressionSyntax.Expression, model, methodOwner, classOwner, originalNode);
                    }
                    else if (node is InvocationExpressionSyntax invocationExpressionSyntax)
                    {
                        if (invocationExpressionSyntax.Expression is MemberAccessExpressionSyntax)
                        {
                            return GetSymbolInfoFromSyntaxNode(invocationExpressionSyntax.Expression, model, methodOwner, classOwner, originalNode);
                        }
                    }
                }
            }

            if (node != originalNode && symbolInfoWrapper != null && symbolInfoWrapper.Symbol != null)
            {
                //TODO
            }
            return symbolInfoWrapper;
        }

        internal static SymbolInfoWrapper ResolveSymbolWhenMismatch(SyntaxNode node, SymbolInfo symbolInfo)
        {
            SymbolInfoWrapper symbolInfoWrapper = new SymbolInfoWrapper(symbolInfo.Symbol, false);
            var symbol = symbolInfo.Symbol;
            if (symbol == null && symbolInfo.CandidateSymbols.Length >= 1 &&
                (symbolInfo.CandidateReason == CandidateReason.Ambiguous || symbolInfo.CandidateReason == CandidateReason.OverloadResolutionFailure))
            {
                // This case happens when we have a call to methods defined within the same class we are targeting
                symbol = symbolInfo.CandidateSymbols.First();
                symbolInfoWrapper = new SymbolInfoWrapper(symbol, true);
                if (symbolInfo.CandidateReason == CandidateReason.OverloadResolutionFailure)
                {
                    if (node is InvocationExpressionSyntax invocationNode)
                    {
                        foreach (var candidateSymbol in symbolInfo.CandidateSymbols)
                        {
                            if (candidateSymbol is IMethodSymbol methodSymbol)
                            {
                                if (invocationNode.ArgumentList.Arguments.Count == methodSymbol.Parameters.Count())
                                {
                                    //TODO: when OverloadResolutionFailure we need to check arguments that are being passed
                                    symbol = candidateSymbol;
                                    symbolInfoWrapper = new SymbolInfoWrapper(symbol, true);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        return symbolInfoWrapper;
                    }
                }
            }

            return symbolInfoWrapper;
        }

        //TODO: improve this: we should not rely on names to exclude shims -> also it's C# specific, doesn't belomg to this class
        internal static IEnumerable<string> ClassesToNotShim =>
            new[]
            {
                "String",
                "StringBuilder",
                "TimeSpan",
                "DateTime",
                "StackTrace",
                "List",
                "Collection",
                "Exception",
                "Object",
                "Dictionary",
                "Random",
                "Convert",
                "Int64",
                "Int32",
                "Int16",
                "Double",
                "Type",
                "SortedList",
                "Guid",
                "Array",
                "ICollection",
                "Regex",
                "Enum",
                "Decimal",
                "Boolean",
                "Single",
                "EventArgs",
                "Nullable",
                "Math",
                "HashSet",
                "Unit",
                "IList",
                "KeyValuePair",
                "CollectionBase",
                "DateTimeFormatInfo",
                "Enumerable",
                "IEnumerator",
                "ConcurrentDictionary",
                "Hashtable",
                "Encoding",
                "byte",
                "Byte",
                "IEnumerable",
                "Color",
                "Image",
                "KeyedCollection",
                "CultureInfo",
                "Char",
                "MatchCollection",
                "Literal",
                "Assembly",
                "Activator",
                "TextInfo",
                "XElement",
                "XAttribute",
                "XmlDocument",
                "XmlNode",
                "XContainer",
                "GroupCollection",
                "ArrayList",
                "InvalidOperationException",
                "IEnumerator",
                "Tuple",
                "Trace",
                "Func"
            };

        internal static string ResolveTypeName(ISymbol type)
        {
            var typeName = type.Name;
            if (typeName.Equals("Nullable") 
                || (type is INamedTypeSymbol namedTypeSymbol && namedTypeSymbol.IsGenericType)
                || typeName.Equals(""))
            {
                typeName = type.ToDisplayString();
            }
            if (type is INamedTypeSymbol namedTypeKind && namedTypeKind.TypeKind == TypeKind.Struct)
            {
                typeName = type.ToDisplayString();
            }
            if (typeName.Contains("anonymous") && type is INamedTypeSymbol namedType && namedType.IsGenericType)
            {
                typeName = "";
                foreach (var typeParam in namedType.TypeParameters)
                {
                    if (!string.IsNullOrWhiteSpace(typeName))
                    {
                        typeName += ", ";
                    }
                    typeName += typeParam.ToDisplayString();
                }

                typeName = $"{type.Name}<{typeName}>";
            }
            return typeName;
        }

        internal static bool ShouldUseIsNullFlag(ISymbol symbol, SemanticModel model)
        {
            ITypeSymbol type = GetTypeFromSymbol(symbol, model);
            if (type.Name.Equals("string", StringComparison.InvariantCultureIgnoreCase))
            {
                return false;
            }
            if (type is INamedTypeSymbol namedTypeSymbol)
            {
                if (namedTypeSymbol.TypeKind == TypeKind.Class)
                {
                    
                    ITypeSymbol classThatImplements;
                    if (!ImplementsInterfaceOrBaseClass(namedTypeSymbol, typeof(IList), out classThatImplements))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        internal static IDictionary<SyntaxNode, MockMethodMapper> GetUniqueExternalReferences(IDictionary<SyntaxNode, MockMethodMapper> original)
        {
            var result = new Dictionary<SyntaxNode, MockMethodMapper>();
            var groups = original.GroupBy(o => o.Value.MockMethodName);
            foreach (var group in groups)
            {
                var mockUsedAsTestParam = group.FirstOrDefault(o => !String.IsNullOrWhiteSpace(o.Value.ReturnExpressionString));
                if (mockUsedAsTestParam.Key != null)
                {
                    result.Add(mockUsedAsTestParam.Key, mockUsedAsTestParam.Value);
                }
                else
                {
                    result.Add(group.First().Key, group.First().Value);
                }
                
            }
            return result;
        }
    }
}
