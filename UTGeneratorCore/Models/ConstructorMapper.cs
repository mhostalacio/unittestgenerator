﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace UTGeneratorCore.Models
{
    public class ConstructorMapper : IMapper
    {
        public IDictionary<string, ParameterSymbol> ParametersTypes { get; }
        public int LinesCount { get; internal set; }
        public ClassMapper Class { get; internal set; }
        public IDictionary<SyntaxNode, MockMethodMapper> ExternalReferences { get; }
        public bool IsNonPublic { get; }
        public bool IsStatic { get; }
        public string UniqueName { get; set; }
        public string Name => "Constructor";

        public bool IsVoid
        {
            get { return true; }
        }

        public IDictionary<SwitchStatementSyntax, SymbolInfoWrapper> SwitchStatements { get; }
        public IDictionary<IfStatementSyntax, IList<SymbolInfoWrapper>> IfStatements { get; }
        public IDictionary<CatchClauseSyntax, SyntaxNode> CatchClauses { get; }

        public BaseMethodDeclarationSyntax MethodDeclaration { get; }

        public ConstructorMapper(BaseMethodDeclarationSyntax constructorDeclaration, ClassMapper classMapper, SemanticModel model)
        {
            ExternalReferences = new Dictionary<SyntaxNode, MockMethodMapper>();
            Class = classMapper;
            ParametersTypes = new Dictionary<string, ParameterSymbol>();
            IsStatic = constructorDeclaration.Modifiers.Any(o => o.Kind() == SyntaxKind.StaticKeyword);
            IsNonPublic = !constructorDeclaration.Modifiers.Any(o => o.IsKind(SyntaxKind.PublicKeyword));
            SwitchStatements = new Dictionary<SwitchStatementSyntax, SymbolInfoWrapper>();
            IfStatements = new Dictionary<IfStatementSyntax, IList<SymbolInfoWrapper>>();
            CatchClauses = new Dictionary<CatchClauseSyntax, SyntaxNode>();
            MethodDeclaration = constructorDeclaration;
            ExternalReferences = new Dictionary<SyntaxNode, MockMethodMapper>();

            var previousNextTokenLine = -1;
            foreach (var trivia in constructorDeclaration.DescendantTrivia())
            {
                var nextTokenLine = trivia.Token.GetLocation().GetMappedLineSpan().StartLinePosition.Line;

                if (nextTokenLine == previousNextTokenLine)
                {
                    continue;
                }

                LinesCount++;
                previousNextTokenLine = nextTokenLine;
            }

            var enumerator = constructorDeclaration.ParameterList.Parameters.GetEnumerator();

            while (enumerator.MoveNext())
            {
                var currentParameter = enumerator.Current;
                var info = ModelExtensions.GetTypeInfo(model, currentParameter.Type);

                ParametersTypes.Add(ModelsHelper.ConvertToCamelCase($"{currentParameter.Identifier.Text}TestArg", true), new ParameterSymbol(info.Type)
                {
                    IsOutParameter = currentParameter.Modifiers.Any(o => o.Kind() == SyntaxKind.OutKeyword),
                    IsRefParameter = currentParameter.Modifiers.Any(o => o.Kind() == SyntaxKind.RefKeyword),
                });
            }

            if (constructorDeclaration.ChildNodes().Any())
            {
                MethodMapper.IterateNodesForExternalCalls(this, constructorDeclaration.ChildNodes());
            }
        }
    }
}
