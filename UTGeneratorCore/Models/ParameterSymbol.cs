﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;

namespace UTGeneratorCore.Models
{
    public class ParameterSymbol
    {
        public ISymbol Symbol { get; }
        public string RenamedTo { get; set; }
        public bool UsedAsTestCaseParameter { get; set; }
        public bool IsOutParameter { get; set; }
        public bool IsRefParameter { get; set; }

        public string Name
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(RenamedTo))
                    return RenamedTo;
                else
                    return Symbol.Name;
            }
        }
        public ParameterSymbol(ISymbol symbol)
        {
            Symbol = symbol;
        }
    }
}
