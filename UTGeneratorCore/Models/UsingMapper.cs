﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Classification;

namespace UTGeneratorCore.Models
{
    public class UsingMapper
    {
        public string ClassName { get; }
        public Dictionary<string, string> NickNames { get; }

        public UsingMapper(string className, string nameSpace, string nickName)
        {
            ClassName = className;
            NickNames = new Dictionary<string, string>();
            NickNames.Add(nameSpace, nickName);
        }
    }
}
