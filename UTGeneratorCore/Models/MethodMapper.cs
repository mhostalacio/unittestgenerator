﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

namespace UTGeneratorCore.Models
{
    public class MethodMapper : IMapper
    {
        public ClassMapper Class { get; internal set; }
        public bool ContainsSwitchCase { get; internal set; }
        public IDictionary<SyntaxNode, MockMethodMapper> ExternalReferences { get; private set; }
        public bool IsGeneric { get; }
        public bool IsNonPublic { get; internal set; }
        public bool IsStatic { get; internal set; }
        public bool IsVoid { get; internal set; }
        public int LinesCount { get; }
        public BaseMethodDeclarationSyntax MethodDeclaration { get; }
        public string Name { get; }
        public IDictionary<string, ParameterSymbol> ParametersTypes { get; }
        public int StatementsCount { get; }
        public IDictionary<SwitchStatementSyntax, SymbolInfoWrapper> SwitchStatements { get; }
        public IDictionary<IfStatementSyntax, IList<SymbolInfoWrapper>> IfStatements { get; }
        public IDictionary<CatchClauseSyntax, SyntaxNode> CatchClauses { get; }
        public string UniqueName { get; set; }

        public MethodMapper(MethodDeclarationSyntax methodDeclaration, SemanticModel model, ClassMapper classMapper)
        {
            MethodDeclaration = methodDeclaration;

            LinesCount = methodDeclaration
                .Body != null ? methodDeclaration
                .Body
                .GetText()
                .Lines
                .Count(line => !string.IsNullOrWhiteSpace(line.ToString())) : 0;

            ParametersTypes = new Dictionary<string, ParameterSymbol>();
            SwitchStatements = new Dictionary<SwitchStatementSyntax, SymbolInfoWrapper>();
            IfStatements = new Dictionary<IfStatementSyntax, IList<SymbolInfoWrapper>>();
            CatchClauses = new Dictionary<CatchClauseSyntax, SyntaxNode>();
            Class = classMapper;
            Name = Convert.ToString(methodDeclaration.Identifier);
            UniqueName = ModelsHelper.NormalizePropertyName(Name);

            if (Class.Methods.FirstOrDefault(o => o.Name == Name) != null)
            {
                UniqueName += Class.Methods.Count(o => o.Name == Name);
            }

            StatementsCount = (methodDeclaration.Body?.Statements.Count).GetValueOrDefault();

            switch (methodDeclaration.ReturnType)
            {
                case GenericNameSyntax _:
                    IsGeneric = true;
                    break;
                case PredefinedTypeSyntax predefinedTypeSyntax when predefinedTypeSyntax.Keyword.Kind() == SyntaxKind.VoidKeyword:
                    IsVoid = true;
                    break;
            }

            IsStatic = methodDeclaration.Modifiers.Any(o => o.Kind() == SyntaxKind.StaticKeyword);
            IsNonPublic = methodDeclaration.Modifiers.FirstOrDefault(o => o.Kind() == SyntaxKind.PublicKeyword).Value == null;
            var enumerator = methodDeclaration.ParameterList.Parameters.GetEnumerator();

            while (enumerator.MoveNext())
            {
                var currentParameter = enumerator.Current;
                var infoType = ModelExtensions.GetTypeInfo(model, currentParameter.Type).Type;
                if (infoType.Kind == SymbolKind.ErrorType && infoType is IErrorTypeSymbol errorType && errorType.CandidateReason == CandidateReason.Ambiguous)
                {
                    infoType = errorType.CandidateSymbols[0] as ITypeSymbol;
                }
                var varName = ModelsHelper.ConvertToCamelCase($"{currentParameter.Identifier.Text}TestArg", true);
                ParametersTypes.Add(varName, new ParameterSymbol(infoType)
                {
                    IsOutParameter = currentParameter.Modifiers.Any(o => o.Kind() == SyntaxKind.OutKeyword),
                    IsRefParameter = currentParameter.Modifiers.Any(o => o.Kind() == SyntaxKind.RefKeyword),
                });
            }

            ExternalReferences = new Dictionary<SyntaxNode, MockMethodMapper>();

            if (methodDeclaration.ChildNodes().Any())
            {
                IterateNodesForExternalCalls(this, methodDeclaration.ChildNodes());
            }
        }

        /// <summary>
        /// iterates through the code to get what needs to be mocked and variables to pass as arguments to test cases
        /// </summary>
        internal static void IterateNodesForExternalCalls(IMapper methodMapper, IEnumerable<SyntaxNode> nodes)
        {
            var nodeList = nodes.ToList();
            MockMethodMapper mockMethodAdded = null;
            if (!nodeList.Any())
            {
                return;
            }

            foreach (var node in nodeList)
            {
                if (node is SwitchStatementSyntax switchStatementSyntax)
                {
                    var symbolInfo = ModelsHelper.GetSymbolInfoFromSyntaxNode(switchStatementSyntax.Expression, methodMapper.Class.SemanticModel, methodMapper.MethodDeclaration, methodMapper.Class.Declaration, switchStatementSyntax.Expression);
                    if (symbolInfo != null && symbolInfo.Symbol != null)
                    {
                        var symbolName = ModelsHelper.ConvertToCamelCase(switchStatementSyntax.Expression.ToString(), true) + "TestArg";
                        symbolInfo.IsInternal = true;
                        methodMapper.SwitchStatements.Add(switchStatementSyntax, symbolInfo);
                        HandleSymbolInfo(methodMapper, symbolInfo, symbolName, switchStatementSyntax.Expression, out mockMethodAdded);
                    }
                }
                if (node is IfStatementSyntax ifStatementSyntax)
                {
                    HandleIfStatements(methodMapper, ifStatementSyntax);
                }
                else if (node is CatchClauseSyntax catchClauseSyntax)
                {
                    HandleCatchClause(methodMapper, catchClauseSyntax);
                }
                else if (node is ForEachStatementSyntax forEachStatementSyntax)
                {
                    var symbolInfo = ModelsHelper.GetSymbolInfoFromSyntaxNode(forEachStatementSyntax.Expression, methodMapper.Class.SemanticModel, methodMapper.MethodDeclaration, methodMapper.Class.Declaration, forEachStatementSyntax.Expression);
                    if (symbolInfo != null && symbolInfo.Symbol != null)
                    {
                        symbolInfo.IsInternal = true;
                        HandleSymbolInfo(methodMapper, symbolInfo, null, forEachStatementSyntax.Expression, out mockMethodAdded);
                        ITypeSymbol classThatImplements;
                        if (ModelsHelper.GetTypeFromSymbol(symbolInfo.Symbol, methodMapper.Class.SemanticModel) is INamedTypeSymbol namedTypeSymbol
                            && !ModelsHelper.ClassesToNotShim.Contains(namedTypeSymbol.Name)
                            && ModelsHelper.ImplementsInterfaceOrBaseClass(namedTypeSymbol, typeof(IEnumerable), out classThatImplements))
                        {
                            if (methodMapper.ExternalReferences.ContainsKey(forEachStatementSyntax.Expression))
                            {
                                methodMapper.ExternalReferences[forEachStatementSyntax.Expression].EnumeratorTypeSymbol = classThatImplements;
                                methodMapper.ExternalReferences[forEachStatementSyntax.Expression].EnumeratorItemTypeSymbol = methodMapper.Class.SemanticModel.GetSymbolInfo(forEachStatementSyntax.Type).Symbol;
                            }
                        }
                    }
                }
                else
                {
                    //TODO: select only we really need to mock
                    ModelsHelper.ExtractExternalReferences(node, methodMapper.Class, methodMapper.ExternalReferences, true, methodMapper.MethodDeclaration);
                }

                IterateNodesForExternalCalls(methodMapper, node.ChildNodes());
            }
        }

        private static void HandleCatchClause(IMapper methodMapper, CatchClauseSyntax catchClauseSyntax)
        {
            // we need to force some exception mocking anything found in 'try block'
            var nodes = ((TryStatementSyntax) catchClauseSyntax.Parent).Block.DescendantNodes();
            bool foundSomethingToMock = false;
            foreach (var node in nodes)
            {
                var temp = new Dictionary<SyntaxNode, MockMethodMapper>();
                ModelsHelper.ExtractExternalReferences(node, methodMapper.Class, temp, true, methodMapper.MethodDeclaration);
                if (temp.Count > 0)
                {
                    methodMapper.CatchClauses.Add(catchClauseSyntax, node);
                    foundSomethingToMock = true;
                    break;
                }
            }

            if (!foundSomethingToMock)
            {
                //try again but without avoiding over mock
                foreach (var node in nodes)
                {
                    var temp = new Dictionary<SyntaxNode, MockMethodMapper>();
                    ModelsHelper.ExtractExternalReferences(node, methodMapper.Class, temp, false, methodMapper.MethodDeclaration);
                    if (temp.Count > 0)
                    {
                        methodMapper.CatchClauses.Add(catchClauseSyntax, node);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Handle if statements found in code
        /// </summary>
        private static void HandleIfStatements(IMapper methodMapper, IfStatementSyntax ifStatementSyntax)
        {
            var expressionList = GetExpressionListFromIfStatement(methodMapper, ifStatementSyntax.Condition, null);
            MockMethodMapper mockMethodAdded = null;
            if (expressionList.Count > 0)
            {
                methodMapper.IfStatements.Add(ifStatementSyntax, new List<SymbolInfoWrapper>());

                foreach (var expr in expressionList)
                {
                    var symbolInfo = ModelsHelper.GetSymbolInfoFromSyntaxNode(expr, methodMapper.Class.SemanticModel, methodMapper.MethodDeclaration, methodMapper.Class.Declaration, expr);
                    if (symbolInfo != null && symbolInfo.Symbol != null)
                    {
                        var symbolName = ModelsHelper.ConvertToCamelCase(expr.ToString(), true) + "TestArg";
                        symbolInfo.IsInternal = true;
                        if (ModelsHelper.ShouldMock(symbolInfo))
                        {
                            //TODO: check this -> if not able to resolve symbol, avoid unused test parameters, but the real problem is why Roslyn is not resolving the symbol
                            if (HandleSymbolInfo(methodMapper, symbolInfo, symbolName, expr, out mockMethodAdded))
                            {
                                methodMapper.IfStatements[ifStatementSyntax].Add(symbolInfo);
                            }
                        }
                        else
                        {
                            //TODO : handle special cases

                            if (symbolInfo.Symbol.Name.Equals("Count")
                                && symbolInfo.Symbol.ContainingType != null
                                && symbolInfo.Symbol.ContainingType.Name.Equals("List")
                                && expr is MemberAccessExpressionSyntax memberAccessExpressionSyntax)
                            {
                                symbolInfo = ModelsHelper.GetSymbolInfoFromSyntaxNode(memberAccessExpressionSyntax.Expression, methodMapper.Class.SemanticModel, methodMapper.MethodDeclaration, methodMapper.Class.Declaration, memberAccessExpressionSyntax.Expression);
                                symbolName = ModelsHelper.ConvertToCamelCase(memberAccessExpressionSyntax.Expression.ToString(), true) + "TestArg";
                                if (symbolInfo != null && symbolInfo.Symbol != null)
                                {
                                    //TODO: check this -> if not able to resolve symbol, avoid unused test parameters, but the real problem is why Roslyn is not resolving the symbol
                                    if (HandleSymbolInfo(methodMapper, symbolInfo, symbolName, expr, out mockMethodAdded))
                                    {
                                        methodMapper.IfStatements[ifStatementSyntax].Add(symbolInfo);
                                    }
                                }
                            }
                            else if(symbolInfo.Symbol.Name.Equals("HasValue")
                                    && expr is MemberAccessExpressionSyntax memberAccessExpressionSyntax2 
                                    && memberAccessExpressionSyntax2.Expression is MemberAccessExpressionSyntax memberAccessExpressionSyntax3)
                            {
                                symbolInfo = ModelsHelper.GetSymbolInfoFromSyntaxNode(memberAccessExpressionSyntax3, methodMapper.Class.SemanticModel, methodMapper.MethodDeclaration, methodMapper.Class.Declaration, memberAccessExpressionSyntax3);
                                symbolName = ModelsHelper.ConvertToCamelCase(memberAccessExpressionSyntax3.ToString(), true) + "TestArg";
                                if (symbolInfo != null && symbolInfo.Symbol != null)
                                {
                                    //TODO: check this -> if not able to resolve symbol, avoid unused test parameters, but the real problem is why Roslyn is not resolving the symbol
                                    if (HandleSymbolInfo(methodMapper, symbolInfo, symbolName, expr, out mockMethodAdded))
                                    {
                                        methodMapper.IfStatements[ifStatementSyntax].Add(symbolInfo);
                                    }
                                }
                            }
                            else if (symbolInfo.Symbol.Name.Equals("Equals")
                                     && expr is InvocationExpressionSyntax invocationExpressionSyntax
                                     && invocationExpressionSyntax.Expression is MemberAccessExpressionSyntax memberAccessExpressionSyntax4
                                     && memberAccessExpressionSyntax4.Expression is IdentifierNameSyntax identifierNameSyntax)
                            {
                                symbolInfo = ModelsHelper.GetSymbolInfoFromSyntaxNode(identifierNameSyntax, methodMapper.Class.SemanticModel, methodMapper.MethodDeclaration, methodMapper.Class.Declaration, identifierNameSyntax);
                                symbolName = ModelsHelper.ConvertToCamelCase(identifierNameSyntax.ToString(), true) + "TestArg";
                                if (symbolInfo != null && symbolInfo.Symbol != null)
                                {
                                    //TODO: check this -> if not able to resolve symbol, avoid unused test parameters, but the real problem is why Roslyn is not resolving the symbol
                                    if (HandleSymbolInfo(methodMapper, symbolInfo, symbolName, expr, out mockMethodAdded))
                                    {
                                        methodMapper.IfStatements[ifStatementSyntax].Add(symbolInfo);
                                    }
                                    if (mockMethodAdded != null)
                                    {
                                        //TODO: add used values
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static bool HandleSymbolInfo(IMapper methodMapper, SymbolInfoWrapper symbolInfo, string symbolName, SyntaxNode expr, out MockMethodMapper mockMethod)
        {
            var success = false;
            symbolInfo.RenamedTo = symbolName;
            mockMethod = null;
            //Add to ExternalReferences everything we need to mock for test parameters
            var temp = new Dictionary<SyntaxNode, MockMethodMapper>();
            ModelsHelper.ExtractExternalReferencesFromSymbol(expr, methodMapper.Class, temp, false, symbolInfo);
            if (temp.Count > 0)
            {
                success = true;
                // Make the mock internal to always be placed inside test cases not in setup method
                temp.First().Value.IsInternalCall = true;
                temp.First().Value.ReturnExpressionString = symbolName;
                //For complex classes we use a flag instead of the actual object as test parameter
                ApplyNullFlag(methodMapper, symbolInfo, symbolName, temp);
                if (!methodMapper.ExternalReferences.ContainsKey(temp.First().Key))
                {
                    methodMapper.ExternalReferences.Add(temp.First());
                    mockMethod = temp.First().Value;
                }
                else
                {
                    methodMapper.ExternalReferences[temp.First().Key] = temp.First().Value;
                    mockMethod = temp.First().Value;
                }
            }
            else if (symbolInfo.Symbol is IParameterSymbol parameterSymbol)
            {
                // In this case we need to replace the parameter of the tested method by the test argument value
                var paramType = methodMapper.ParametersTypes.FirstOrDefault(o => o.Key == parameterSymbol.Name);
                if (!string.IsNullOrWhiteSpace(paramType.Key))
                {
                    success = true;
                    paramType.Value.UsedAsTestCaseParameter = true;
                    paramType.Value.RenamedTo = symbolName;
                }
            }
            else if (symbolInfo.Symbol is ILocalSymbol localSymbol)
            {
                var initNode = methodMapper.MethodDeclaration.DescendantNodes().OfType<VariableDeclaratorSyntax>()
                    .FirstOrDefault(o => o.Identifier.ToString().Equals(localSymbol.Name));
                if (initNode != null && initNode.Initializer != null && initNode.Initializer.Value != null)
                {
                    var newsymbolInfo = ModelsHelper.GetSymbolInfoFromSyntaxNode(initNode.Initializer.Value, methodMapper.Class.SemanticModel, methodMapper.MethodDeclaration, methodMapper.Class.Declaration, initNode.Initializer.Value);
                    if (newsymbolInfo != null && newsymbolInfo.Symbol != null && (newsymbolInfo.Symbol.ContainingType  != null && !ModelsHelper.ClassesToNotShim.Contains(newsymbolInfo.Symbol.ContainingType.Name)))
                    {
                        symbolInfo.IsInternal = true;
                        symbolInfo.Symbol = newsymbolInfo.Symbol;
                        ModelsHelper.ExtractExternalReferencesFromSymbol(initNode, methodMapper.Class, temp, false, symbolInfo);
                        if (temp.Count > 0)
                        {
                            success = true;
                            // Make the mock internal to always be placed inside test cases not in setup method
                            temp.First().Value.IsInternalCall = true;
                            temp.First().Value.ReturnExpressionString = symbolName;
                            ApplyNullFlag(methodMapper, symbolInfo, symbolName, temp);
                            if (!methodMapper.ExternalReferences.ContainsKey(temp.First().Key))
                            {
                                methodMapper.ExternalReferences.Add(temp.First());
                                mockMethod = temp.First().Value;
                            }
                            else
                            {
                                methodMapper.ExternalReferences[temp.First().Key] = temp.First().Value;
                                mockMethod = temp.First().Value;
                            }
                        }
                    }
                }
                else
                {
                    // try to find where the variable is assigned
                    var assignmentNode = methodMapper.MethodDeclaration.DescendantNodes().OfType<AssignmentExpressionSyntax>()
                        .FirstOrDefault(o => o.Left is IdentifierNameSyntax identifier && identifier.Identifier.ToString().Equals(localSymbol.Name));
                    if (assignmentNode != null)
                    {
                        var newsymbolInfo = ModelsHelper.GetSymbolInfoFromSyntaxNode(assignmentNode.Right, methodMapper.Class.SemanticModel, methodMapper.MethodDeclaration, methodMapper.Class.Declaration, assignmentNode.Right);
                        if (newsymbolInfo != null && newsymbolInfo.Symbol != null && !ModelsHelper.ClassesToNotShim.Contains(newsymbolInfo.Symbol.ContainingType.Name))
                        {
                            symbolInfo.IsInternal = true;
                            ModelsHelper.ExtractExternalReferencesFromSymbol(assignmentNode, methodMapper.Class, temp, false, symbolInfo);
                            if (temp.Count > 0)
                            {
                                success = true;
                                // Make the mock internal to always be placed inside test cases not in setup method
                                temp.First().Value.IsInternalCall = true;
                                temp.First().Value.ReturnExpressionString = symbolName;
                                ApplyNullFlag(methodMapper, symbolInfo, symbolName, temp);
                                if (!methodMapper.ExternalReferences.ContainsKey(temp.First().Key))
                                {
                                    methodMapper.ExternalReferences.Add(temp.First());
                                    mockMethod = temp.First().Value;
                                }
                                else
                                {
                                    methodMapper.ExternalReferences[temp.First().Key] = temp.First().Value;
                                    mockMethod = temp.First().Value;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                // TODO: check this case
            }

            return success;
        }

        private static void ApplyNullFlag(IMapper methodMapper, SymbolInfoWrapper symbolInfo, string symbolName, Dictionary<SyntaxNode, MockMethodMapper> temp)
        {
            if (ModelsHelper.ShouldUseIsNullFlag(symbolInfo.Symbol, methodMapper.Class.SemanticModel))
            {
                temp.First().Value.AddIsNullFlag = true;
                temp.First().Value.ReturnExpressionString = $"{symbolName}IsNull";
                symbolInfo.RenamedTo = $"{symbolName}IsNull";
                symbolInfo.AddIsNullFlag = true;
            }
        }

        /// <summary>
        /// Extracts expressions from if statement needed for parameters construction
        /// </summary>
        private static List<SyntaxNode> GetExpressionListFromIfStatement(IMapper methodMapper, ExpressionSyntax exprSyntax, List<SyntaxNode> expressionList)
        {
            if (expressionList == null)
            {
                expressionList = new List<SyntaxNode>();
            }
            if (exprSyntax is BinaryExpressionSyntax binaryExpressionSyntax)
            {
                //can be simple case such as (A.Property == Something) or multiple conditions
                if (!AvoidAddingParameterForExpression(binaryExpressionSyntax.Left, methodMapper))
                {
                    //example: if (A.Property == Something) -> one condition
                    expressionList.Add(binaryExpressionSyntax.Left);
                }
                else
                {
                    GetExpressionListFromIfStatement(methodMapper, binaryExpressionSyntax.Left, expressionList);
                }

                if (!AvoidAddingParameterForExpression(binaryExpressionSyntax.Right, methodMapper))
                {
                    //example: if (A.Property == Something) -> one condition
                    expressionList.Add(binaryExpressionSyntax.Right);
                }
                else
                {
                    GetExpressionListFromIfStatement(methodMapper, binaryExpressionSyntax.Right, expressionList);
                }

            }
            else if (exprSyntax is PrefixUnaryExpressionSyntax prefixUnaryExpressionSyntax)
            {
                var symbolInfo = ModelsHelper.GetSymbolInfoFromSyntaxNode(prefixUnaryExpressionSyntax.Operand, methodMapper.Class.SemanticModel, methodMapper.MethodDeclaration, methodMapper.Class.Declaration, prefixUnaryExpressionSyntax.Operand);
                if (symbolInfo != null && symbolInfo.Symbol != null)
                {
                    expressionList.Add(prefixUnaryExpressionSyntax.Operand);
                }
                else if (prefixUnaryExpressionSyntax.Operand is InvocationExpressionSyntax invocationExpressionSyntax)
                {
                    // try to work with the arguments
                    foreach (var arg in invocationExpressionSyntax.ArgumentList.Arguments)
                    {
                        symbolInfo = ModelsHelper.GetSymbolInfoFromSyntaxNode(arg.Expression, methodMapper.Class.SemanticModel, methodMapper.MethodDeclaration, methodMapper.Class.Declaration, arg.Expression);
                        if (symbolInfo != null && symbolInfo.Symbol != null)
                        {
                            expressionList.Add(prefixUnaryExpressionSyntax.Operand);
                        }
                    }
                }
            }
            else
            {
                if (!AvoidAddingParameterForExpression(exprSyntax, methodMapper))
                    expressionList.Add(exprSyntax);
            }

            return expressionList;
        }

        private static bool AvoidAddingParameterForExpression(ExpressionSyntax expr, IMapper methodMapper)
        {
            if (expr is MemberAccessExpressionSyntax memberAccessExpressionSyntax)
            {
                //example: if (shape.LineWidth != HSSFShape.LINEWIDTH_DEFAULT)
                var symbolInfo = methodMapper.Class.SemanticModel.GetSymbolInfo(memberAccessExpressionSyntax);
                var symbolInfoWrapper = ModelsHelper.ResolveSymbolWhenMismatch(memberAccessExpressionSyntax, symbolInfo);
                if (symbolInfoWrapper.Symbol != null && symbolInfoWrapper.Symbol is IFieldSymbol fieldSymbol && 
                    (fieldSymbol.IsConst || fieldSymbol.IsStatic))
                {
                    // when referencing a constant or static field we do not want to use as test parameter
                    return true;
                }
            }

            return expr is PrefixUnaryExpressionSyntax
                   || expr is BinaryExpressionSyntax // multiple conditions
                   || (expr.Parent != null && expr.Parent.Kind() == SyntaxKind.IsExpression); //example: if (something is Object) -> do not try to mock right side
        }
    }
}
