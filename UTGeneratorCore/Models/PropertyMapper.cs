﻿using System;
using System.Linq;
using System.Web.UI;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace UTGeneratorCore.Models
{
    public class PropertyMapper
    {
        public bool ExtendsControl { get; }
        public bool ImplementsIDisposable { get; }
        public bool IsPrivate { get; internal set; }
        public string Name { get; internal set; }
        public ClassMapper Owner { get; }
        public SemanticModel SemanticModel { get; }
        public IPropertySymbol PropertySymbol { get; }
        public bool HasGetter { get; }
        public bool HasPublicGetter { get; }
        public bool HasSetter { get; }
        public bool HasPublicSetter { get; }
        public bool IsStatic { get; }
        public PropertyDeclarationSyntax Declaration { get; }
        public PropertyMapper(PropertyDeclarationSyntax member, SemanticModel model, ClassMapper classMapper)
        {
            SemanticModel = model;
            Owner = classMapper;
            IsPrivate = member.Modifiers.Any(o => o.Kind() == SyntaxKind.PrivateKeyword);
            Declaration = member;

            var symbol = SemanticModel.GetDeclaredSymbol(member);

            if (symbol == null)
            {
                return;
            }

            PropertySymbol = symbol as IPropertySymbol;

            if (PropertySymbol == null)
            {
                return;
            }

            HasGetter = PropertySymbol.GetMethod != null;

            HasSetter = PropertySymbol.SetMethod != null;

            IsStatic = PropertySymbol.IsStatic;

            HasPublicGetter = HasGetter && PropertySymbol.GetMethod.DeclaredAccessibility == Accessibility.Public;

            HasPublicSetter = HasSetter && PropertySymbol.SetMethod.DeclaredAccessibility == Accessibility.Public;

            var typeSymbol = PropertySymbol.Type as INamedTypeSymbol;
            ITypeSymbol classThatImplements;
            ImplementsIDisposable = ModelsHelper.ImplementsInterfaceOrBaseClass(typeSymbol, typeof(IDisposable), out classThatImplements);

            ExtendsControl = ModelsHelper.ImplementsInterfaceOrBaseClass(typeSymbol, typeof(Control), out classThatImplements) ||
                             ModelsHelper.ImplementsInterfaceOrBaseClass(typeSymbol, typeof(System.Windows.Forms.Control), out classThatImplements);

            Name = PropertySymbol.Name;
        }
    }
}
