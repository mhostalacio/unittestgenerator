﻿using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace UTGeneratorCore.Models
{
    public interface IMapper
    {
        IDictionary<string, ParameterSymbol> ParametersTypes { get; }

        ClassMapper Class { get; }

        IDictionary<SyntaxNode, MockMethodMapper> ExternalReferences { get; }

        bool IsStatic { get; }

        string UniqueName { get; }

        string Name { get; }
        bool IsVoid { get; }
        bool IsNonPublic { get; }

        IDictionary<SwitchStatementSyntax, SymbolInfoWrapper> SwitchStatements { get; }
        IDictionary<IfStatementSyntax, IList<SymbolInfoWrapper>> IfStatements { get; }
        IDictionary<CatchClauseSyntax, SyntaxNode> CatchClauses { get; }
        BaseMethodDeclarationSyntax MethodDeclaration { get; }
    }
}
