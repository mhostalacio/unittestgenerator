﻿using System;
using System.Linq;
using System.Web.UI;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace UTGeneratorCore.Models
{
    public class FieldMapper
    {
        public bool ExtendsControl { get; }
        public bool ImplementsIDisposable { get; }
        public bool IsPrivate { get; internal set; }
        public bool IsPublic { get; internal set; }
        public bool IsStatic { get; internal set; }
        public bool IsConstant { get; internal set; }
        public string Name { get; internal set; }
        public ClassMapper Owner { get; }
        public SemanticModel SemanticModel { get; }
        public IFieldSymbol FieldSymbol { get; }
        public bool HasDefaultValue { get; internal set; }
        public BaseFieldDeclarationSyntax Declaration { get; internal set; }
        public FieldMapper(BaseFieldDeclarationSyntax member, SemanticModel model, ClassMapper classMapper)
        {
            SemanticModel = model;
            Owner = classMapper;
            IsPrivate = member.Modifiers.Any(o => o.Kind() == SyntaxKind.PrivateKeyword);
            IsPublic = member.Modifiers.Any(o => o.Kind() == SyntaxKind.PublicKeyword);
            IsStatic = member.Modifiers.Any(o => o.Kind() == SyntaxKind.StaticKeyword);
            IsConstant = member.Modifiers.Any(o => o.Kind() == SyntaxKind.ConstKeyword);
            HasDefaultValue = member.Declaration.Variables[0].Initializer != null;
            Declaration = member;
            if (member.Declaration.Variables.Count <= 0)
            {
                return;
            }

            var symbol = SemanticModel.GetDeclaredSymbol(member.Declaration.Variables[0]);

            if (symbol == null)
            {
                return;
            }

            FieldSymbol = symbol as IFieldSymbol;

            if (FieldSymbol == null)
            {
                return;
            }

            var typeSymbol = FieldSymbol.Type as INamedTypeSymbol;
            ITypeSymbol classThatImplements;
            ImplementsIDisposable = ModelsHelper.ImplementsInterfaceOrBaseClass(typeSymbol, typeof(IDisposable), out classThatImplements);

            ExtendsControl = ModelsHelper.ImplementsInterfaceOrBaseClass(typeSymbol, typeof(Control), out classThatImplements) || 
                             ModelsHelper.ImplementsInterfaceOrBaseClass(typeSymbol, typeof(System.Windows.Forms.Control), out classThatImplements);

            Name = FieldSymbol.Name;
        }

    }
}
