﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;

namespace UTGeneratorCore.Models
{
    public class SymbolInfoWrapper
    {
        public ISymbol Symbol { get; set; }

        public bool IsInternal { get; set; }
        public string RenamedTo { get; set; }
        public bool AddIsNullFlag { get; set; }

        public string Name
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(RenamedTo))
                {
                    return RenamedTo;
                }
                else
                {
                    return Symbol.Name;
                }
            }
        }

        public SymbolInfoWrapper(ISymbol symbol, bool isInternal)
        {
            Symbol = symbol;
            IsInternal = isInternal;
        }
    }
}
