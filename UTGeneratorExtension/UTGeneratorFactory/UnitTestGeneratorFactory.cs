﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.VisualBasic;
using UTGeneratorCore.Factory.CSharp;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;

namespace UTGeneratorExtension
{
    // UnitTestGeneratorFactory should be replaced with DI or ServiceLocator
    public static class UnitTestGeneratorFactory
    {
        public static IUnitTestGenerator CreateGenerator()
        {
            return new UnitTestGenerator();
        }
    }

    public interface IUnitTestGenerator
    {
        string Generate(string filePath, TestFramework testFramework, MockFramework mockFramework, bool addMocksToSetupMethod, IList<string> trustedAssembliesPaths);

        void MockHighlitedCode(string filePath, TextViewSelection selectedCode, MockFramework mockFramework, IList<string> trustedAssembliesPaths);

        void GenerateTestProject(string folderPathAndProjectName, TestFramework testFramework);
    }

    class UnitTestGenerator : IUnitTestGenerator
    {
        public string Generate(string filePath, TestFramework testFramework, MockFramework mockFramework, bool addMocksToSetupMethod, IList<string> trustedAssembliesPaths)
        {
            try
            {
                var gen = new CSharpTestGenerator<TestFramework, MockFramework>(filePath, testFramework, mockFramework, trustedAssembliesPaths);
                gen.PlaceMocksInSetupMethod = addMocksToSetupMethod;
                var code = gen.GenerateFullTestClass();
                // workaround to fix format
                var result = gen.FixFormatting(code.ToString());
                return result;
            }
            catch (Exception e)
            {
                Clipboard.SetText(e.Message + e.StackTrace);
                Console.WriteLine(e);
                throw;
            }
        }

        public void GenerateTestProject(string folderPathAndProjectName, TestFramework testFramework)
        {
            var gen = new CSharpTestGenerator<TestFramework, MockFramework>(testFramework);
            gen.GenerateTestProject(folderPathAndProjectName);
        }

        public void MockHighlitedCode(string filePath, TextViewSelection selectedCode, MockFramework mockFramework, IList<string> trustedAssembliesPaths)
        {
            // test framework doesn't make any difference
            var gen = new CSharpTestGenerator<TestFramework, MockFramework>(filePath, NUnitTestFramework.Instance, mockFramework, trustedAssembliesPaths);
            var code = gen.MockSelectedCode(selectedCode);
            var codeStr = string.Join("\n", code.Select(o => o.ToString()));
            // workaround to fix format
            if (String.IsNullOrWhiteSpace(codeStr))
            {
                codeStr = "// Unable to generate mock code.";
            }
            var result = gen.FixFormatting(codeStr);
            Clipboard.SetText(result);
        }
    }
}
