﻿using EnvDTE;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace UTGeneratorExtension
{
    public class GeneratorViewModel : BaseViewModel
    {
        private string _generatedCode;
        private string _testFileName;
        private string _sourceFilePath;
        private string _testClassName;
        private string _sourceFileName;
        private string _testFramework;
        private bool _preserveFoldersStructure;
        private Project _selectedTestProject;
        private bool _createTestProject;
        private string _newProjectName;
        private bool _addMocksToSetupMehtod;

        public SolutionListenerModel ListenerModel { get; private set; }

        public string GeneratedCode
        {
            get => _generatedCode;
            set
            {
                _generatedCode = value;
                NotifyPropertyChanged(nameof(GeneratedCode));
            }
        }

        public string NewProjectName
        {
            get => _newProjectName;
            set
            {
                _newProjectName = value;
                NotifyPropertyChanged(nameof(NewProjectName));
            }
        }

        public string TestFileName
        {
            get => _testFileName;
            set
            {
                _testFileName = value;
                NotifyPropertyChanged(nameof(TestFileName));
            }
        }

        public string SourceFilePath
        {
            get => _sourceFilePath;
            set
            {
                _sourceFilePath = value;
                NotifyPropertyChanged(nameof(SourceFilePath));
            }
        }

        public string TestClassName
        {
            get => _testClassName;
            set
            {
                _testClassName = value;
                NotifyPropertyChanged(nameof(TestClassName));
            }
        }

        public string SourceFileName
        {
            get => _sourceFileName;
            set
            {
                _sourceFileName = value;
                NotifyPropertyChanged(nameof(SourceFileName));
            }
        }

        public string TestFramework
        {
            get => _testFramework;
            set
            {
                _testFramework = value;
                NotifyPropertyChanged(nameof(TestFramework));
            }
        }

        public bool PreserveFoldersStructure
        {
            get => _preserveFoldersStructure;
            set
            {
                _preserveFoldersStructure = value;
                NotifyPropertyChanged(nameof(PreserveFoldersStructure));
            }
        }

        public bool AddMocksToSetupMehtod
        {
            get => _addMocksToSetupMehtod;
            set
            {
                _addMocksToSetupMehtod = value;
                NotifyPropertyChanged(nameof(AddMocksToSetupMehtod));
            }
        }

        public bool CreateTestProject
        {
            get => _createTestProject;
            set
            {
                _createTestProject = value;
                NotifyPropertyChanged(nameof(CreateTestProject));
            }
        }

        public Project SelectedTestProject
        {
            get => _selectedTestProject;
            set
            {
                _selectedTestProject = value;
                NotifyPropertyChanged(nameof(SelectedTestProject));
            }
        }

        public Project CurrentSourceCodeProject { get; set; }

        public GeneratorViewModel(SolutionListenerModel listenerModel)
        {
            ListenerModel = listenerModel;
            Reset();
        }

        public void Reset()
        {
            GeneratedCode = "Drop C# class";
            TestFileName = string.Empty;
            SourceFilePath = string.Empty;
            TestClassName = string.Empty;
            SourceFileName = string.Empty;
            if (String.IsNullOrEmpty(TestFramework))
                TestFramework = ListenerModel.Frameworks.First().Key;
            _preserveFoldersStructure = true;
            _createTestProject = false;
            _newProjectName = string.Empty;
            if (ListenerModel.Projects.Count > 0)
            {
                var commonPath = ListenerModel.Projects.Select(s => s.FullName.Split('\\'))
                    .Aggregate(
                         ListenerModel.Projects[0].FullName.Split('\\').AsEnumerable(), // init accum with words from first string
                         (a, words) => a.Intersect(words),       // intersect with next set of words
                         a => a);
                _newProjectName = string.Join("\\", commonPath) + "\\UnitTests\\{ProjectName}.Tests";
                if (commonPath.Count() == 0)
                    _newProjectName = "{Root Folder}" + _newProjectName;
            }
            _addMocksToSetupMehtod = false;
        }

        public bool Validate()
        {
            throw new NotImplementedException();
        }
    }
}
