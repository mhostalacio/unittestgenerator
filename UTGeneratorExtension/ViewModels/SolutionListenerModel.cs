﻿using EnvDTE;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UTGeneratorExtension
{
    public class SolutionListenerModel : BaseViewModel
    {
        private bool _solutionLoaded;

        public ObservableCollection<Project> Projects { get; private set; }
        public ObservableCollection<KeyValuePair<string,string>> Frameworks { get; private set; }

        public SolutionListenerModel()
        {
            Projects = new ObservableCollection<Project>();
            Frameworks = new ObservableCollection<KeyValuePair<string, string>>
            {
                new KeyValuePair<string,string>("NUnit", "NUnit"),
                new KeyValuePair<string,string>("MSTests", "MS Tests"),
            };
        }

        public bool SolutionLoaded
        {
            get => _solutionLoaded;
            set
            {
                _solutionLoaded = value;
                NotifyPropertyChanged(nameof(SolutionLoaded));
            }
        }
    }
}
