﻿using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UTGeneratorExtension
{
    class SolutionListener : ISolutionListener, IVsSolutionEvents
    {
        private IVsSolution _solution;
        private uint _eventsCookie = VSConstants.VSCOOKIE_NIL;
        private bool _isDisposed;
        public Solution2 DteSolution { get; private set; }

        public SolutionListenerModel Model { get; private set; }

        public SolutionListener(IVsSolution solution, Solution2 dteSolution)
        {
            _solution = solution;
            DteSolution = dteSolution;
            Model = new SolutionListenerModel();
            ThreadHelper.ThrowIfNotOnUIThread();
            ErrorHandler.ThrowOnFailure(_solution.AdviseSolutionEvents(this, out _eventsCookie));
            ReloadModelInternal();
        }

        #region Private Methods

        private void ReloadModelInternal()
        {
            Model.SolutionLoaded = GetSolutionPropertyValue<bool>(__VSPROPID.VSPROPID_IsSolutionOpen);
            Model.Projects.Clear();
            GetProjects().ToList().ForEach(p => Model.Projects.Add(p));
        }

        private IEnumerable<Project> GetProjects()
        {
            foreach (IVsHierarchy h in GetProjectsInSolution(_solution))
            {
                Project project = GetDTETestProject(h);
                if (project != null) yield return project;
            }
        }

        private IEnumerable<IVsHierarchy> GetProjectsInSolution(IVsSolution solution)
        {
            IEnumHierarchies enumHierarchies;
            Guid guid = Guid.Empty;
            solution.GetProjectEnum((uint)__VSENUMPROJFLAGS.EPF_LOADEDINSOLUTION, ref guid, out enumHierarchies);
            if (enumHierarchies == null) yield break;
            IVsHierarchy[] hierarchy = new IVsHierarchy[1];
            uint fetched;
            while (enumHierarchies.Next(1, hierarchy, out fetched) == VSConstants.S_OK && fetched == 1)
            {
                if (hierarchy.Length > 0 && hierarchy[0] != null)
                    yield return hierarchy[0];
            }
        }

        private Project GetDTETestProject(IVsHierarchy hierarchy)
        {
            if (hierarchy == null) throw new ArgumentNullException("hierarchy");
            object obj;
            hierarchy.GetProperty(VSConstants.VSITEMID_ROOT, (int)__VSHPROPID.VSHPROPID_ExtObject, out obj);
            var project = obj as Project;
            if (project != null && project.FullName.EndsWith(".csproj"))
            {
                return project;
            }
            return null;
        }

        private T GetSolutionPropertyValue<T>(__VSPROPID solutionProperty)
        {
            object value = null;
            T result = default(T);
            if (_solution.GetProperty((int)solutionProperty, out value) == VSConstants.S_OK)
            {
                result = (T)value;
            }
            return result;
        }

        #endregion

        #region ISolutionListener

        public void ReloadModel()
        {
            ReloadModelInternal();
        }

        #endregion

        #region IVsSolutionEvents

        public int OnAfterOpenProject(IVsHierarchy pHierarchy, int fAdded)
        {
            var addedProject = GetDTETestProject(pHierarchy);
            if (addedProject != null)
            {
                Model.Projects.Add(addedProject);
            }
            return VSConstants.S_OK;
        }
        public int OnQueryCloseProject(IVsHierarchy pHierarchy, int fRemoving, ref int pfCancel) => VSConstants.S_OK;
        public int OnBeforeCloseProject(IVsHierarchy pHierarchy, int fRemoved)
        {
            var removedProject = GetDTETestProject(pHierarchy);
            if (removedProject != null)
            {
                Model.Projects.Remove(removedProject);
            }
            return VSConstants.S_OK;
        }
        public int OnAfterLoadProject(IVsHierarchy pStubHierarchy, IVsHierarchy pRealHierarchy) => VSConstants.S_OK;
        public int OnQueryUnloadProject(IVsHierarchy pRealHierarchy, ref int pfCancel) => VSConstants.S_OK;
        public int OnBeforeUnloadProject(IVsHierarchy pRealHierarchy, IVsHierarchy pStubHierarchy) => VSConstants.S_OK;
        public int OnAfterOpenSolution(object pUnkReserved, int fNewSolution)
        {
            Model.SolutionLoaded = true;
            return VSConstants.S_OK;
        }
        public int OnQueryCloseSolution(object pUnkReserved, ref int pfCancel) => VSConstants.S_OK;
        public int OnBeforeCloseSolution(object pUnkReserved) => VSConstants.S_OK;
        public int OnAfterCloseSolution(object pUnkReserved)
        {
            Model.SolutionLoaded = false;
            return VSConstants.S_OK;
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            if (!_isDisposed)
            {
                if (disposing && _solution != null && _eventsCookie != VSConstants.VSCOOKIE_NIL)
                {
                    ErrorHandler.ThrowOnFailure(_solution.UnadviseSolutionEvents(_eventsCookie));
                    _eventsCookie = VSConstants.VSCOOKIE_NIL;
                }
                _isDisposed = true;
            }
        }

        #endregion
    }
}
