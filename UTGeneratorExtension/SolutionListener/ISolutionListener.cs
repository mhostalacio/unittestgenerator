﻿using EnvDTE80;
using System;

namespace UTGeneratorExtension
{
    public interface ISolutionListener : IDisposable
    {
        SolutionListenerModel Model { get; }
        Solution2 DteSolution { get; }
        void ReloadModel();
    }
}
