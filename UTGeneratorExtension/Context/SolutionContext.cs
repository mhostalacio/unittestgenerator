﻿using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using IAsyncServiceProvider = Microsoft.VisualStudio.Shell.IAsyncServiceProvider;
using Task = System.Threading.Tasks.Task;

namespace UTGeneratorExtension
{
    public class SolutionContext
    {
        private static ISolutionListener _listener;

        public static ISolutionListener Listener => _listener ?? throw new Exception("SolutionContext is not initialized!");

        public static async Task InitializeAsync(IAsyncServiceProvider serviceProvider)
        {
            if (_listener == null)
            {
                await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
                var solution = await serviceProvider.GetServiceAsync(typeof(SVsSolution)) as IVsSolution;
                var dteSolution = ((await serviceProvider.GetServiceAsync(typeof(EnvDTE.DTE))) as EnvDTE80.DTE2).Solution as EnvDTE80.Solution2;
                if (solution == null || dteSolution==null)
                {
                    throw new InvalidOperationException("Could not retrieve solution service");
                }
                _listener = new SolutionListener(solution, dteSolution);
            }
        }
    }
}
