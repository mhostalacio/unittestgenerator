﻿using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TextManager.Interop;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Globalization;
using System.Linq;
using UTGeneratorCore.Factory.Frameworks;
using UTGeneratorCore.Models;
using Task = System.Threading.Tasks.Task;

namespace UTGeneratorExtension
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class MockHighlightedCode
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 256;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("38c45d38-483f-4bdb-943e-c37e6b1f3011");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly AsyncPackage package;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockHighlightedCode"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        /// <param name="commandService">Command service to add command to, not null.</param>
        private MockHighlightedCode(AsyncPackage package, OleMenuCommandService commandService)
        {
            this.package = package ?? throw new ArgumentNullException(nameof(package));
            commandService = commandService ?? throw new ArgumentNullException(nameof(commandService));

            var menuCommandID = new CommandID(CommandSet, CommandId);
            var menuItem = new MenuCommand(Execute, menuCommandID);
            commandService.AddCommand(menuItem);
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static MockHighlightedCode Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private Microsoft.VisualStudio.Shell.IAsyncServiceProvider ServiceProvider
        {
            get
            {
                return package;
            }
        }

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static async Task InitializeAsync(AsyncPackage package)
        {
            // Switch to the main thread - the call to AddCommand in MockHighlightedCode's constructor requires
            // the UI thread.
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync(package.DisposalToken);

            OleMenuCommandService commandService = await package.GetServiceAsync((typeof(IMenuCommandService))) as OleMenuCommandService;
            Instance = new MockHighlightedCode(package, commandService);
        }

        /// <summary>
        /// This function is the callback used to execute the command when the menu item is clicked.
        /// See the constructor to see how the menu item is associated with this function using
        /// OleMenuCommandService service and MenuCommand class.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        private void Execute(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();
            TextViewSelection selection = GetSelection((IServiceProvider)ServiceProvider);
            string activeDocumentPath = GetActiveFilePath((IServiceProvider)ServiceProvider);
            var referencedAssembliesList = ReferencedAssembliesList(activeDocumentPath);
            UnitTestGeneratorFactory.CreateGenerator()
                .MockHighlitedCode(activeDocumentPath, selection, MsFakesMockFramework.Instance, referencedAssembliesList);
            string message = "Mock code was placed into your clipboard. Please paste it wherever it suits you.";
          
            // Show a message box to prove we were here
            VsShellUtilities.ShowMessageBox(
                package,
                message,
                "Unit Test Generator Tool",
                OLEMSGICON.OLEMSGICON_INFO,
                OLEMSGBUTTON.OLEMSGBUTTON_OK,
                OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
        }

        private List<string> ReferencedAssembliesList(string filePath)
        {
            var referencedAssembliesList = new List<string>();
            var projectItem = DteHelper.FindItemByFilePath(SolutionContext.Listener.Model.Projects.ToList(), filePath);
            var proj = projectItem != null
                ? projectItem.ContainingProject
                : null;
            if (proj != null)
            {
                var vsproject = proj.Object as VSLangProj.VSProject;
                if (vsproject != null)
                {
                    foreach (var reference in vsproject.References)
                    {
                        if (reference is VSLangProj.Reference vsReference)
                        {
                            referencedAssembliesList.Add(vsReference.Path);
                        }
                    }
                }

                var vswebsite = proj.Object as VsWebSite.VSWebSite;
                if (vswebsite != null)
                {
                    foreach (var reference in vswebsite.References)
                    {
                        if (reference is VSLangProj.Reference vsReference)
                        {
                            referencedAssembliesList.Add(vsReference.Path);
                        }
                    }
                }
            }

            return referencedAssembliesList;
        }

        private TextViewSelection GetSelection(IServiceProvider serviceProvider)
        {
            var service = serviceProvider.GetService(typeof(SVsTextManager));
            var textManager = service as IVsTextManager2;
            IVsTextView view;
            int result = textManager.GetActiveView2(1, null, (uint)_VIEWFRAMETYPE.vftCodeWindow, out view);

            view.GetSelection(out int startLine, out int startColumn, out int endLine, out int endColumn);//end could be before beginning
            var start = new TextViewPosition(startLine, startColumn);
            var end = new TextViewPosition(endLine, endColumn);

            view.GetSelectedText(out string selectedText);

            var selection = new TextViewSelection(start, end, selectedText);
            return selection;
        }

        private string GetActiveFilePath(IServiceProvider serviceProvider)
        {
            EnvDTE80.DTE2 applicationObject = serviceProvider.GetService(typeof(DTE)) as EnvDTE80.DTE2;
            return applicationObject.ActiveDocument.FullName;
        }
    }

}
