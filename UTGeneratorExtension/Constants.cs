﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTGeneratorExtension
{
    public class Constants
    {
        public const string ThirdPartyFolder = "ThirdParty";
        public const string NUnitTemplateFolder = "NUnitTemplate";
        public const string NUnitCompressedTemplate = "NUnit.Tests.Template.zip";
        public const string NUnitTemplateName = "MyTemplate.vstemplate";
        public const string MSTestTemplateFolder = "MSTestTemplate";
        public const string MSTestCompressedTemplate = "MSTests.Tests.Template.zip";
        public const string MSTestTemplateName = "MyTemplate.vstemplate";
    }
}
