﻿using EnvDTE;
using System;
using System.Collections.Generic;
using static Microsoft.VisualStudio.VSConstants;

namespace UTGeneratorExtension
{
    public class DteHelper
    {
        public static ProjectItem FindItemByFilePath(List<Project> projects, string filePath)
        {
            foreach (var project in projects)
            {
                var projectItem = FindItemByFilePath(project, filePath);
                if (projectItem != null)
                {
                    return projectItem;
                }
            }
            return null;
        }

        public static ProjectItem FindItemByFilePath(Project project, string filePath)
        {
            if (project.ProjectItems.Count > 0)
            {
                foreach (ProjectItem item in project.ProjectItems)
                {
                    var projectItem = FindItemByFilePath(item, filePath);
                    if (projectItem != null)
                    {
                        return projectItem;
                    }
                }
            }
            return null;
        }

        public static ProjectItem FindItemByFilePath(ProjectItem projectItem, string filePath)
        {
            if (projectItem.FileCount > 0 && projectItem.FileNames[0] == filePath)
            {
                return projectItem;
            }
            foreach (ProjectItem item in projectItem.ProjectItems)
            {
                var nestedItem = FindItemByFilePath(item, filePath);
                if (nestedItem != null)
                {
                    return nestedItem;
                }
            }
            return null;
        }

        /// <summary>
        /// Create folder tree to test project and return last folder item
        /// </summary>
        /// <param name="project"></param>
        /// <param name="folderTree"></param>
        /// <returns></returns>
        public static ProjectItem CreateFolderTree(Project project, string[] folderTree)
        {
            if (folderTree == null || folderTree.Length == 0)
            {
                throw new ArgumentException(nameof(folderTree));
            }
            var projectItems = project.ProjectItems;
            var currentIndex = 0;
            ProjectItem foundFolderItem = null;
            while (currentIndex < folderTree.Length)
            {
                ProjectItem currentFolderItem = null;
                var projectItemsEnumerator = projectItems.GetEnumerator();
                while (projectItemsEnumerator.MoveNext())
                {
                    var currentItem = (ProjectItem)projectItemsEnumerator.Current;
                    if (currentItem.Name == folderTree[currentIndex] && currentItem.Kind == ItemTypeGuid.PhysicalFolder_string)
                    {
                        currentFolderItem = currentItem;
                        foundFolderItem = currentItem;
                        break;
                    }
                }
                if (currentFolderItem == null)
                {
                    break;
                }
                currentIndex++;
                projectItems = currentFolderItem.ProjectItems;
            }
            var targetProjectItems = projectItems;
            ProjectItem newFolderItem = foundFolderItem;
            for (var i = currentIndex; i < folderTree.Length; i++)
            {
                newFolderItem = targetProjectItems.AddFolder(folderTree[i]);
                targetProjectItems = newFolderItem.ProjectItems;
            }
            if (newFolderItem == null)
            {
                throw new Exception("Could not generate test class folders");
            }
            return newFolderItem;
        }
    }
}
