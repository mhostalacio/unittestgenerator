﻿using System;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using UTGeneratorCore.Factory.Frameworks;

namespace UTGeneratorExtension
{
    public class TemplateHelper
    {
        private static string _thirdPartyFolder;

        public static string ExtractTemplate(TestFramework framework)
        {
            var templatePath = GetTemplateFilePath(framework);
            if (!File.Exists(templatePath))
            {
                var templateZipPath = GetCompressedTemplatePath(framework);
                ZipFile.ExtractToDirectory(templateZipPath, templateZipPath.Substring(0, templateZipPath.LastIndexOf("\\")));
                return templatePath;
            }
            return templatePath;
        }

        private static string GetThirdPartyFolder()
        {
            return !string.IsNullOrEmpty(_thirdPartyFolder)
                ? _thirdPartyFolder
                : _thirdPartyFolder = Path.Combine(
                    Directory.GetParent(Assembly.GetExecutingAssembly().Location).FullName,
                    Constants.ThirdPartyFolder);
        }

        private static string GetTemplateFolderPath(TestFramework framework)
        {
            var templateFolderName = string.Empty;
            // using switch case instead of if/else will be easier to add more new frameworks
            switch (framework)
            {
                case NUnitTestFramework x: templateFolderName = Constants.NUnitTemplateFolder; break;
                case MsTestsTestFramework x: templateFolderName = Constants.MSTestTemplateFolder; break;
                default: throw new NotSupportedException();
            }
            return Path.Combine(GetThirdPartyFolder(), templateFolderName);
        }

        private static string GetTemplateFilePath(TestFramework framework)
        {
            var templateFileName = string.Empty;
            switch (framework)
            {
                case NUnitTestFramework x: templateFileName = Constants.NUnitTemplateName; break;
                case MsTestsTestFramework x: templateFileName = Constants.MSTestTemplateName; break;
                default: throw new NotSupportedException();
            }
            return Path.Combine(GetTemplateFolderPath(framework), templateFileName);
        }

        private static string GetCompressedTemplatePath(TestFramework framework)
        {
            var templateName = string.Empty;
            switch (framework)
            {
                case NUnitTestFramework x: templateName = Constants.NUnitCompressedTemplate; break;
                case MsTestsTestFramework x: templateName = Constants.MSTestCompressedTemplate; break;
                default: throw new NotSupportedException();
            }
            return Path.Combine(GetTemplateFolderPath(framework), templateName);
        }
    }
}
