using EnvDTE;
using EnvDTE100;
using EnvDTE80;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using UTGeneratorCore.Factory.Frameworks;
using static Microsoft.VisualStudio.VSConstants;

namespace UTGeneratorExtension
{
    public partial class UTGeneratorWindowControl : UserControl
    {
        private GeneratorViewModel _generatorViewModel;
        private string[] _validExtensions = { ".cs", ".aspx", ".ascx", ".asax", ".xaml", ".asmx", ".svc" };

        public UTGeneratorWindowControl()
        {
            this.InitializeComponent();
            _generatorViewModel = new GeneratorViewModel(SolutionContext.Listener.Model);
            DataContext = _generatorViewModel;
        }

        private void TxtBoxSource_PreviewDrop(object sender, DragEventArgs e)
        {
            e.Handled = true;
            if (e.Data != null && e.Data.GetDataPresent(DataFormats.Text))
            {
                try
                {
                    var filePath = e.Data.GetData(DataFormats.Text).ToString();
                    if (!_validExtensions.Any(x => filePath.EndsWith(x)))
                    {
                        throw new InvalidOperationException($"generator supports only the following file extensions {string.Join(",", _validExtensions)}");
                    }
                    filePath = NormalizeCSharpFileExtension(filePath);
                    if (!File.Exists(filePath))
                    {
                        throw new FileNotFoundException($"Could not find {filePath}. if this is a nested file please select the base file (ex: *.aspx instead of *.aspx.cs)");
                    }
                    TestFramework testFramework;
                    MsFakesMockFramework mockFramework;
                    GetFrameworksSelected(out testFramework, out mockFramework);
                    var sourceFileName = filePath.Split(@"\/".ToCharArray()).Last();
                    _generatorViewModel.SourceFileName = sourceFileName;
                    _generatorViewModel.TestFileName = NormalizeCSharpFileExtension($"{sourceFileName.Split(".".ToCharArray()).First()}Tests");
                    _generatorViewModel.SourceFilePath = filePath;
                    var referencedAssembliesList = ReferencedAssembliesList(filePath);
                    _generatorViewModel.GeneratedCode = UnitTestGeneratorFactory.CreateGenerator().Generate(filePath, testFramework, mockFramework, _generatorViewModel.AddMocksToSetupMehtod, referencedAssembliesList);
                }
                //TODO: implement error status bar
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private List<string> ReferencedAssembliesList(string filePath)
        {
            var referencedAssembliesList = new List<string>();
            var projectItem = DteHelper.FindItemByFilePath(_generatorViewModel.ListenerModel.Projects.ToList(), filePath);
            _generatorViewModel.CurrentSourceCodeProject = projectItem != null
                ? projectItem.ContainingProject
                : null;
            if (_generatorViewModel.CurrentSourceCodeProject != null)
            {
                var vsproject = _generatorViewModel.CurrentSourceCodeProject.Object as VSLangProj.VSProject;
                if (vsproject != null)
                {
                    foreach (var reference in vsproject.References)
                    {
                        if (reference is VSLangProj.Reference vsReference)
                        {
                            referencedAssembliesList.Add(vsReference.Path);
                        }
                    }
                }

                var vswebsite = _generatorViewModel.CurrentSourceCodeProject.Object as VsWebSite.VSWebSite;
                if (vswebsite != null)
                {
                    foreach (var reference in vswebsite.References)
                    {
                        if (reference is VSLangProj.Reference vsReference)
                        {
                            referencedAssembliesList.Add(vsReference.Path);
                        }
                    }
                }
            }

            return referencedAssembliesList;
        }

        private void GetFrameworksSelected(out TestFramework testFramework, out MsFakesMockFramework mockFramework)
        {
            testFramework = (_generatorViewModel.TestFramework != null && _generatorViewModel.TestFramework.Equals("NUnit")) ? NUnitTestFramework.Instance : (TestFramework)MsTestsTestFramework.Instance;
            mockFramework = MsFakesMockFramework.Instance;
        }

        private string NormalizeCSharpFileExtension(string filePath)
        {
            return filePath.EndsWith(".cs") ? filePath : $"{filePath}.cs";
        }

        private void BtnReload_Click(object sender, RoutedEventArgs e)
        {
            SolutionContext.Listener.ReloadModel();
        }

        private void BtnGenerate_Click(object sender, RoutedEventArgs e)
        {
            //TODO: replace with view model Validate method
            if (comboBoxProjects.SelectedItem == null)
            {
                MessageBox.Show("Please select project");
                return;
            }
            try
            {
                string[] sourceFileFoldersTree = null;
                var selectedProjected = comboBoxProjects.SelectedItem as Project;
                var solution = SolutionContext.Listener.DteSolution;
                var targetProjectItems = selectedProjected.ProjectItems;
                var templatePath = solution.GetProjectItemTemplate("Class.zip", "CSharp");
                if (_generatorViewModel.PreserveFoldersStructure)
                {
                    sourceFileFoldersTree = ScanSourceClassFolders();
                    if (sourceFileFoldersTree.Length > 0)
                    {
                        targetProjectItems = DteHelper.CreateFolderTree(selectedProjected, sourceFileFoldersTree).ProjectItems;
                    }
                }
                targetProjectItems.AddFromTemplate(templatePath, _generatorViewModel.TestFileName);
                var addedFile = targetProjectItems.Item(_generatorViewModel.TestFileName);
                var textDoc = (TextDocument)addedFile.Document.Object("TextDocument");
                EditPoint editPoint = textDoc.StartPoint.CreateEditPoint();
                EditPoint endPoint = textDoc.EndPoint.CreateEditPoint();
                editPoint.Delete(endPoint);
                editPoint.Insert(_generatorViewModel.GeneratedCode);
                var baseNamespace = selectedProjected.Properties.Item("DefaultNamespace").Value.ToString();
                ModifyClassNamespace(baseNamespace, addedFile, sourceFileFoldersTree);
                selectedProjected.Save();
                ResetAll();
            }
            //TODO: implement error status bar
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ModifyClassNamespace(string baseNamespace, ProjectItem classItem, string[] sourceFileFoldersTree)
        {
            // changed code from relying on FileCodeModel to TextDocument interface as FileCodeModel is null if project is not built successfully
            var textDoc = (TextDocument)classItem.Document.Object("TextDocument");
            EditPoint startPoint = textDoc.StartPoint.CreateEditPoint();
            EditPoint movingPoint  = textDoc.StartPoint.CreateEditPoint();
            movingPoint.LineDown();
            TextPoint endPoint = textDoc.EndPoint;
            var treeBasedNamespace = string.Join(".", sourceFileFoldersTree ?? Array.Empty<string>());
            var modifiedNamespace = string.IsNullOrEmpty(treeBasedNamespace)
                ? baseNamespace
                : string.Join(".", baseNamespace, treeBasedNamespace);
            while (startPoint.LessThan(endPoint))
            {
                if (startPoint.ReplacePattern(movingPoint, "^namespace *", "*", (int)vsFindOptions.vsFindOptionsRegularExpression))
                {
                    startPoint.Delete(startPoint.LineLength);
                    startPoint.Insert($"namespace {modifiedNamespace}");
                    break;
                }
                startPoint.LineDown();
                movingPoint.LineDown();
            };
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            ResetAll();
        }

        private void ResetAll()
        {
            comboBoxProjects.SelectedItem = null;
            _generatorViewModel.Reset();
        }

        //TODO: add to helpers
        private string[] ScanSourceClassFolders()
        {
            if (string.IsNullOrEmpty(_generatorViewModel.SourceFilePath))
            {
                throw new InvalidOperationException("source file path is empty");
            }
            /*
             get ProjectItem and its containing project with this aggressive iteration instead of relying on paths similarity
             between both of them to avoid failure in specific rare cases.
             */
            var sourceFilePath = _generatorViewModel.SourceFilePath;
            var sourceClassItem = DteHelper.FindItemByFilePath(_generatorViewModel.ListenerModel.Projects.ToList(), sourceFilePath);
            if (sourceClassItem == null)
            {
                throw new Exception("could not retrieve source class");
            }
            if (sourceClassItem.Kind != ItemTypeGuid.PhysicalFile_string)
            {
                // this should never happen!
                throw new Exception("retrieving source class failed");
            }
            var containingProject = sourceClassItem.ContainingProject;
            var basePath = containingProject.FileName.Replace($"{containingProject.Name}.csproj", string.Empty);
            var sourceFileRelativePath = sourceFilePath
                .Replace(basePath, string.Empty)
                .Replace(_generatorViewModel.SourceFileName, string.Empty);
            var sourceFileTree = sourceFileRelativePath.Split(@"\/".ToCharArray());
            return sourceFileTree.Take(sourceFileTree.Length - 1).ToArray();
        }

        private void ComboBoxProjects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _generatorViewModel.SelectedTestProject = ((ComboBox)sender).SelectedItem as Project;
        }

        private void CreateTestProject_Checked(object sender, RoutedEventArgs e)
        {
            _generatorViewModel.CreateTestProject = ((CheckBox)sender).IsChecked.HasValue && ((CheckBox)sender).IsChecked.Value;
            if (_generatorViewModel.CreateTestProject)
            {
                txtNewProjectName.Visibility = Visibility.Visible;
                btnCreate.Visibility = Visibility.Visible;
            }
            else
            {
                txtNewProjectName.Visibility = Visibility.Hidden;
                btnCreate.Visibility = Visibility.Hidden;
            }
        }

        private void BtnCreate_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtNewProjectName.Text))
            {
                //TODO
            }
            else
            {
                var templatePath = string.Empty;
                TestFramework testFramework;
                MsFakesMockFramework mockFramework;
                GetFrameworksSelected(out testFramework, out mockFramework);
                try
                {
                    templatePath = TemplateHelper.ExtractTemplate(testFramework);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error while extracting project template : {ex.Message}");
                }
                var folderPathAndProjectName = txtNewProjectName.Text.Trim();
                string projectName = folderPathAndProjectName.Substring(folderPathAndProjectName.LastIndexOf("\\") + 1);
                var dteSolution = SolutionContext.Listener.DteSolution;
                var solutionBuild = dteSolution.SolutionBuild;
                try
                {
                    dteSolution.AddFromTemplate(templatePath, folderPathAndProjectName, projectName, false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error while adding new project : {ex.Message}");
                }
                // UnitTestGeneratorFactory.CreateGenerator().GenerateTestProject(txtNewProjectName.Text.Trim(),testFramework);
            }
        }

        private void ComboBoxFrameworks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _generatorViewModel.TestFramework = ((KeyValuePair<string, string>)((ComboBox)sender).SelectedItem).Key;
        }

        private void AddMocksToSetupMehtod_Click(object sender, RoutedEventArgs e)
        {
            _generatorViewModel.AddMocksToSetupMehtod = ((CheckBox)sender).IsChecked.HasValue && ((CheckBox)sender).IsChecked.Value;
        }
    }
}
