﻿namespace UTGeneratorExtension
{
    using System;
    using System.Runtime.InteropServices;
    using Microsoft.VisualStudio.Shell;

    /// <summary>
    /// This class implements the tool window exposed by this package and hosts a user control.
    /// </summary>
    /// <remarks>
    /// In Visual Studio tool windows are composed of a frame (implemented by the shell) and a pane,
    /// usually implemented by the package implementer.
    /// <para>
    /// This class derives from the ToolWindowPane class provided from the MPF in order to use its
    /// implementation of the IVsUIElementPane interface.
    /// </para>
    /// </remarks>
    [Guid("25a78900-aca8-4b8a-b7df-a4af6beb7aed")]
    public class UTGeneratorWindow : ToolWindowPane
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UTGeneratorWindow"/> class.
        /// </summary>
        public UTGeneratorWindow() : base(null)
        {
            this.Caption = "Unit Test Generator Tool";

            // This is the user control hosted by the tool window; Note that, even if this class implements IDisposable,
            // we are not calling Dispose on this object. This is because ToolWindowPane calls Dispose on
            // the object returned by the Content property.
            this.Content = new UTGeneratorWindowControl();
        }
    }
}
